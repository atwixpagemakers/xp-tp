<?php

require_once '../inc/global.php';

			$res = $db->query('SELECT DISTINCT c.tp_courses_id, c.tp_courses_name 
					 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' pc, ' . $CONFP['TABLE_TP_COURSES'] . ' c, ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' pc2s 
                        WHERE pc.tp_courses_id = c.tp_courses_id
                          AND pc2s.tp_planned_course_id = pc.tp_planned_course_id 
                          AND pc.tp_planned_course_completed = 1
                          AND pc.tp_planned_course_id = 196
                     ORDER BY c.tp_courses_name asc');

			$courses_data = array();
			while ($row = $res->fetchRow()) {
				$courses_data[$row['tp_courses_id']] = $row;
	
				$res_planned = $db->query('SELECT DISTINCT pc.tp_planned_course_id, pc.tp_planned_course_date, pc.tp_planned_course_notes, pc.tp_planned_course_trainer
					 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' pc, ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' pc2s 
                        WHERE pc2s.tp_planned_course_id = pc.tp_planned_course_id 
                          AND pc.tp_courses_id = ' . $row['tp_courses_id'] . '
                          AND pc.tp_planned_course_id = 196
                          AND pc.tp_planned_course_completed = 1
                     ORDER BY pc.tp_planned_course_date asc');
				while ($row_planned = $res_planned->fetchRow()) {                 
				    $courses_data[$row['tp_courses_id']]['planned'][$row_planned['tp_planned_course_id']] = $row_planned;

					$res_user = $db->query('SELECT DISTINCT pc2s.no_staff, pc2s.sabre_passed_status, pc2s.sabre_none_passed_reason, u.firstname, u.surname, CONCAT(u.surname, ", ", u.firstname) as user_name, sq.sabre_code,  
					                                        pc2s.sabre_passed_status,
															if ( pc2s.sabre_passed_status IN (3, 5, 6) , 0 , 1) as sabre_passed_status_order  
					 FROM users u, ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' pc2s  
					 LEFT JOIN ' . $CONFP['TABLE_TP_SABRE_QUALIF'] . ' sq on sq.sabre_id = pc2s.sabre_id
                        WHERE pc2s.tp_planned_course_id = ' . $row_planned['tp_planned_course_id'] . '
                          AND u.no_staff = pc2s.no_staff
                     ORDER BY sabre_passed_status_order asc, user_name asc, sq.sabre_code asc');
                     
					while ($row_user = $res_user->fetchRow()) {
						$courses_data[$row['tp_courses_id']]['planned'][$row_planned['tp_planned_course_id']]['users'][] = $row_user;
					}
				}
			}

			// BOF save course name & sabres codes
			$course_query = $db->query('select c.tp_courses_name 
			                              from tp_courses c, tp_planned_courses pc
										 where pc.tp_courses_id = c.tp_courses_id
										   and pc.tp_planned_course_id = "196"');
			$course_user = $course_query->fetchRow();
						 
			
			$sabre_data = sabreQualifGet(NULL, true);
			$sabres_query = $db->query('select distinct pc2s.sabre_id 
			                              from tp_planned_courses_to_sabres pc2s
										 where pc2s.tp_planned_course_id = "196"');
			// EOF save course name & sabres codes

			if ( count($courses_data) ) {
				foreach ( $courses_data as $_tp_courses_id => $course_data ) {
					foreach ( $course_data['planned'] as $_tp_planned_course_id => $planned_data ) {
						$email_content .= '<p>&nbsp;</p>';
						$email_content .= 'Course Name: ' . '<b>' . $course_data['tp_courses_name'] .'</b>' . '<br>';
						$email_content .= 'Course Date: ' . '<b>' . date('d/m/y', $planned_data['tp_planned_course_date']) . '</b><br>';
						$email_content .= 'Course Instructor: ' . '<b>' . strip_tags($planned_data['tp_planned_course_trainer']) . '</b><br>';

						$email_content .= '<table class="regular thickbox botmar4" cellspacing="1" cellpadding="0" border="0" width="100%">' . "\n";
						$email_content .= '<tr>' . "\n";
						$email_content .= '<th width="120">Staff Number</th>' . "\n";
						$email_content .= '<th width="210">Username</th>' . "\n";
						$email_content .= '<th width="150">Sabre Qualification</th>' . "\n";
						$email_content .= '<th width="100" style="text-align:center">Status</th>' . "\n";
						$email_content .= '<th>Reason</th>' . "\n";
						$email_content .= '</tr>' . "\n";
						$flag_use_odd = false;
						foreach ( $planned_data['users'] as $_tp_planned_course_id => $user_data ) {
							$email_content .= '<tr ' . ( $flag_use_odd ? 'class="odd"' : '' ) . '>' . "\n";
							$email_content .= '<td align="center">' . $user_data['no_staff'] . '</td>' . "\n";
							$email_content .= '<td>' . ( $user_data['sabre_passed_status_order'] == 0 ? '<font color="red">' : '' ) . $user_data['user_name'] . ( $user_data['sabre_passed_status_order'] == 0 ? '</font>' : '' ) . '</td>' . "\n";
							$email_content .= '<td align="center">' . $user_data['sabre_code'] . '</td>' . "\n";
							$email_content .= '<td align="center">' . "\n" . 
										     ( $user_data['sabre_passed_status'] == 1 ? 'Pass' : '' ) . "\n" .
											 ( $user_data['sabre_passed_status'] == 2 ? 'Pass-Retake' : '' ) . "\n" .
											 ( $user_data['sabre_passed_status'] == 3 ? 'Fail' : '' ) . "\n" .
											 ( $user_data['sabre_passed_status'] == 4 ? 'Complete' : '' ) . "\n" .
										     ( $user_data['sabre_passed_status'] == 5 ? 'Not Complete' : '' ) . "\n" .
										     ( $user_data['sabre_passed_status'] == 6 ? 'Not-Attended' : '' ) . "\n" .
										     '</td>'. "\n";
							$email_content .= '<td>' . strip_tags($user_data['sabre_none_passed_reason']) . '&nbsp;</td>' . "\n";
							$email_content .= '</tr>' . "\n";

							if ( $flag_use_odd ) {
								$flag_use_odd = false;
							} else {
								$flag_use_odd = true;
							}
						}
						$email_content .= '</table>' . "\n";
					}
				}

				$style_content = file_get_contents(ROOT_PROJ . 'css/style.css');

	
				$admin_email_array = explode(',', 'admin@tcxplorer.com,firster@crehelp.com');	
		
				if ( count($admin_email_array) ) {
					foreach ( $admin_email_array as $key => $admin_email )	{
						$admin_email = trim($admin_email);
						if ( tep_validate_email($admin_email) ) {
							$correct_admin_email[] = $admin_email;
							mailProj(
								array (
									'to' => $admin_email,
									'subject' => 'Training Planner Notification. REPEATEDLY (correct email)',
									'body' => 'Dear Admin,<br><br>' . $email_content,
							),
							array (
								'{HEAD_ADDITIONAL}' => '<style type="text/css">' . $style_content . '</style>',
							));
						}
					}
				}
			} 
			// EOF send email section

echo 'ok';
?>
