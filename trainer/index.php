<?php
ini_set('memory_limit', '128M');
//ini_set('max_input_vars', '10000');

require_once '../inc/global.php';

trainerAccessCheck();

pageTitle('Planned Courses');

$planned_courses = getAllPlannedCourses(true); // where tp_planned_course_completed = 0
$planned_courses_dates = array();
foreach ( $planned_courses as $_tp_courses_id => $courses_data ) {
	$planned_courses_dates[$_tp_courses_id] = getPlannedCourseDates($_tp_courses_id, true);
}

if ( (!isset($_GET['tp_planned_course_id'])) && (!isset($_GET['tp_courses_id'])) ) {
	// get first course
	reset($planned_courses);
	$tmp_buffer = current($planned_courses);
	$current_tp_courses_id = $tmp_buffer['tp_courses_id'];
	
	reset($planned_courses_dates[$current_tp_courses_id]);
	$tmp_buffer = current($planned_courses_dates[$current_tp_courses_id]);
	$current_tp_planned_course_id = $tmp_buffer['tp_planned_course_id'];
} elseif ( isset($_GET['tp_planned_course_id']) ) {
	$current_tp_planned_course_id = (int)$_GET['tp_planned_course_id'];
	
	$current_tp_courses_id = 0;
	foreach ( $planned_courses_dates as $_tp_courses_id => $courses_data ) {
		foreach ( $courses_data as $_tp_planned_course_id => $planned_course_data ) {
			if ( $_tp_planned_course_id == $current_tp_planned_course_id ) {
				$current_tp_courses_id = $_tp_courses_id;
				break;
			}
		}
		if ( $current_tp_courses_id > 0 ) break;
	}
} elseif ( isset($_GET['tp_courses_id']) ) {
	$current_tp_courses_id = (int)$_GET['tp_courses_id'];

	reset($planned_courses_dates[$current_tp_courses_id]);
	$tmp_buffer = current($planned_courses_dates[$current_tp_courses_id]);
	$current_tp_planned_course_id = $tmp_buffer['tp_planned_course_id'];
}

$default_day = date('j');
$default_month = date('n');
$default_year = date('Y');

// BOF get SABRES in the current COURSE
$courses_sabres_table = array();
$courses_sabres_table = getSabresByCourse($current_tp_courses_id);
$another_courses_sabres_table = getAnotherSabresFromPlannedCourse($current_tp_planned_course_id);

foreach ( $another_courses_sabres_table as $key => $sabre_item ) {
	$courses_sabres_table[] = array_merge($sabre_item, array('color' => 'red') );
}
// EOF get SABRES in the current COURSE

switch ( $_GET['action'] )  {
	case 'print_course':
		define('_MPDF_PATH', ROOT_PROJ . '/inc/MPDF/');
		define('ROOT_PAS', realpath(ROOT_PROJ . '../pas') . '/');
	
		require_once _MPDF_PATH . 'mpdf.php';
		require_once ROOT_PAS . 'inc/class.ns.http.php';
	break;

	case 'course_save_marks':
		if ( count($_POST['sabre_status']) ) {
			foreach ( $_POST['sabre_status'] as $_no_staff => $sabre_data ) {
				$db->query('delete from ' . CONFP('TABLE_TP_PLANNED_COURSES_TO_SABRES') . ' where sabre_id = "0" and sabre_added_status = "0" and no_staff = "' . $_no_staff . '"');
				
				//echo $_no_staff . '<br>';
				foreach ( $sabre_data as $_sabre_id => $_sabre_added_status ) {
					//echo $_sabre_id . '=>' . $_sabre_added_status . '<br>';
					if ( $_sabre_added_status > 0 ) {
						if ( ! checkSabreExistsForUserInCourse($current_tp_planned_course_id, $_sabre_id, $_no_staff) ) {
							$sql = 'insert into ' . CONFP('TABLE_TP_PLANNED_COURSES_TO_SABRES') . ' (
											tp_planned_course_id,
											sabre_id,
											no_staff,
											sabre_added_status,
											date_added) values (
											"' . $current_tp_planned_course_id . '", 
											"' . $_sabre_id . '", 
											"' . addslashes($_no_staff) . '", 
											"' . $_sabre_added_status . '", 
											' . TIME . ')';
							$db->query($sql);					
						}
					}
				}
			}
		}

		if ( count($_POST['sabre_passed_status']) ) {
			foreach ( $_POST['sabre_passed_status'] as $_tp_planned_courses_to_sabres_id => $_sabre_passed_status ) {
				if ( $_POST['passed_date_day'][$_tp_planned_courses_to_sabres_id] > 0 && $_POST['passed_date_month'][$_tp_planned_courses_to_sabres_id]==0 || $_POST['passed_date_year'][$_tp_planned_courses_to_sabres_id] > 0 )  {
					$passed_date = mktime(0, 0, 0, $_POST['passed_date_month'][$_tp_planned_courses_to_sabres_id], $_POST['passed_date_day'][$_tp_planned_courses_to_sabres_id], $_POST['passed_date_year'][$_tp_planned_courses_to_sabres_id]);
					
					if ( $passed_date > TIME ) $passed_date = 0;
				} else {
					$passed_date = 0;
				}
				
				if ( $_sabre_passed_status > 0 && $passed_date > 0  ) {
					// BOF detect EXP date
					
					// 1. get all info about current planned sabre to course
					$planned_sabre_info = getPlannedUserSabreDataByID($_tp_planned_courses_to_sabres_id);
					// 2. get info about SABRE
					$sabre_data = sabreQualifGet($planned_sabre_info['sabre_id'], true);
					// 3. seach for prev module
					
					if ( $sabre_data['sabre_exp'] == 0 ) {
						// $_exp_date = TIME;
						$_exp_date = $passed_date;
					} else { 
						$_exp_date = add_month_to_date($passed_date, (int)$sabre_data['sabre_exp']);

						if ( (int)$sabre_data['sabre_eom'] == 1 ) {
							$how_many_days_in_month = cal_days_in_month(CAL_GREGORIAN, strftime("%m", $_exp_date), strftime("%G",$_exp_date));

							$_exp_date = mktime(0, 0, 0, date('n', $_exp_date), $how_many_days_in_month, date('Y', $_exp_date));
						} 
					}

					if ( $_sabre_passed_status==$CONFP['TRAINER_MARKS']['fail'] || $_sabre_passed_status==$CONFP['TRAINER_MARKS']['not-complete']  || $_sabre_passed_status==$CONFP['TRAINER_MARKS']['not-attended'] ) {
						$_exp_date = 0;
					}

					// EOF detect EXP date
					$flag_execute_sql = true;
					
					if ( ( $_sabre_passed_status==3 || $_sabre_passed_status==5 ) && trim($_POST['sabre_none_passed_reason'][$_tp_planned_courses_to_sabres_id])=='' ) {
						$flag_execute_sql = false;
					}

					if ( $flag_execute_sql ) {
						// check if exists
						$res = $db->query('SELECT * 
						                     FROM ' . CONFP('TABLE_TP_PLANNED_COURSES_TO_SABRES') . ' 
											WHERE sabre_passed_status = "' . $_sabre_passed_status . '"
											  AND passed_date = ' . $passed_date . '
											  AND tp_planned_courses_to_sabres_id = ' . $_tp_planned_courses_to_sabres_id);

						if ( $res->numRows() == 0 ) {
							$sql = 'update ' . CONFP('TABLE_TP_PLANNED_COURSES_TO_SABRES') . ' 
								       set sabre_passed_status = "' . $_sabre_passed_status . '",
								           sabre_none_passed_reason = "' . addslashes(trim($_POST['sabre_none_passed_reason'][$_tp_planned_courses_to_sabres_id])) . '",
								           exp_date = ' . $_exp_date . ',
								           passed_date = ' . $passed_date . ',
								           passed_by = "' . userStaffNo() . '",
								           date_modified = ' . TIME . ' 
								     where tp_planned_courses_to_sabres_id = ' . $_tp_planned_courses_to_sabres_id;
							$db->query($sql);		
						} 
					}
					
				}
			}
		}

		header('Location: ' . $_SERVER['PHP_SELF'] . '?success=saved_course&tp_planned_course_id=' . (int)$_GET['tp_planned_course_id']);
	break;	

	case 'complete_course':
		$res = $db->query('select * from ' . CONFP('TABLE_TP_PLANNED_COURSES_TO_SABRES') . ' WHERE tp_planned_course_id = ' . (int)$_GET['tp_planned_course_id'] . ' and sabre_passed_status = 0');
		if ( $res->numRows() > 0 ) {
			errorToPrint('All modules must be updated.');
		} else {
			$db->query('UPDATE ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' 
		               SET tp_planned_course_completed = "1", 
		                   tp_planned_course_complete_date = ' . TIME . ',
						   tp_planned_course_completed_by = "' . userStaffNo() . '"
		             WHERE tp_planned_course_id = ' . (int)$_GET['tp_planned_course_id']);
	
			// BOF send email section
			// 1 - pass (green)
			// 2 - pass-retake (yellow)
			// 3 - fail (red)
			// 4 - complete (green)
			// 5 - not complete (red)
			// 6 - not-attended (purple)

			$res = $db->query('SELECT DISTINCT c.tp_courses_id, c.tp_courses_name 
					 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' pc, ' . $CONFP['TABLE_TP_COURSES'] . ' c, ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' pc2s 
                        WHERE pc.tp_courses_id = c.tp_courses_id
                          AND pc2s.tp_planned_course_id = pc.tp_planned_course_id 
                          AND pc.tp_planned_course_completed = 1
                          AND pc.tp_planned_course_id = ' . (int)$_GET['tp_planned_course_id'] . '
                     ORDER BY c.tp_courses_name asc');
			$courses_data = array();
			while ($row = $res->fetchRow()) {
				$courses_data[$row['tp_courses_id']] = $row;
	
				$res_planned = $db->query('SELECT DISTINCT pc.tp_planned_course_id, pc.tp_planned_course_date, pc.tp_planned_course_notes, pc.tp_planned_course_trainer
					 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' pc, ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' pc2s 
                        WHERE pc2s.tp_planned_course_id = pc.tp_planned_course_id 
                          AND pc.tp_courses_id = ' . $row['tp_courses_id'] . '
                          AND pc.tp_planned_course_id = ' . (int)$_GET['tp_planned_course_id'] . '
                          AND pc.tp_planned_course_completed = 1
                     ORDER BY pc.tp_planned_course_date asc');
				while ($row_planned = $res_planned->fetchRow()) {                 
				    $courses_data[$row['tp_courses_id']]['planned'][$row_planned['tp_planned_course_id']] = $row_planned;

					$res_user = $db->query('SELECT DISTINCT pc2s.no_staff, pc2s.sabre_passed_status, pc2s.sabre_none_passed_reason, u.firstname, u.surname, CONCAT(u.surname, ", ", u.firstname) as user_name, sq.sabre_code,  
					                                        pc2s.sabre_passed_status,
															if ( pc2s.sabre_passed_status IN (3, 5, 6) , 0 , 1) as sabre_passed_status_order  
					 FROM users u, ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' pc2s  
					 LEFT JOIN ' . $CONFP['TABLE_TP_SABRE_QUALIF'] . ' sq on sq.sabre_id = pc2s.sabre_id
                        WHERE pc2s.tp_planned_course_id = ' . $row_planned['tp_planned_course_id'] . '
                          AND u.no_staff = pc2s.no_staff
                     ORDER BY sabre_passed_status_order asc, user_name asc, sq.sabre_code asc');
                     
					while ($row_user = $res_user->fetchRow()) {
						$courses_data[$row['tp_courses_id']]['planned'][$row_planned['tp_planned_course_id']]['users'][] = $row_user;
					}
				}
			}

			// BOF save course name & sabres codes
			$course_query = $db->query('select c.tp_courses_name 
			                              from tp_courses c, tp_planned_courses pc
										 where pc.tp_courses_id = c.tp_courses_id
										   and pc.tp_planned_course_id = "' . (int)$_GET['tp_planned_course_id'] . '"');
			$course_user = $course_query->fetchRow();
			$db->query('update tp_planned_courses 
			               set tp_courses_name = "' . $course_user['tp_courses_name'] . '"
						 where tp_planned_course_id = "' . (int)$_GET['tp_planned_course_id'] . '"');
						 
			
			$sabre_data = sabreQualifGet(NULL, true);
			$sabres_query = $db->query('select distinct pc2s.sabre_id 
			                              from tp_planned_courses_to_sabres pc2s
										 where pc2s.tp_planned_course_id = "' . (int)$_GET['tp_planned_course_id'] . '"');
			while ( $sabres_item = $sabres_query->fetchRow() ) {	
				$db->query('update tp_planned_courses_to_sabres 
			               set sabre_code = "' . $sabre_data[$sabres_item['sabre_id']]['sabre_code'] . '",
			                   sabre_subject = "' . $sabre_data[$sabres_item['sabre_id']]['sabre_subject'] . '"
						 where tp_planned_course_id = "' . (int)$_GET['tp_planned_course_id'] . '" 
						   and sabre_id = ' . $sabres_item['sabre_id']);
			}
			// EOF save course name & sabres codes

			if ( count($courses_data) ) {
				foreach ( $courses_data as $_tp_courses_id => $course_data ) {
					foreach ( $course_data['planned'] as $_tp_planned_course_id => $planned_data ) {
						$email_content .= '<p>&nbsp;</p>' . "\n";
						$email_content .= 'Course Name: ' . '<b>' . $course_data['tp_courses_name'] .'</b>' . '<br>' . "\n";
						$email_content .= 'Course Date: ' . '<b>' . date('d/m/y', $planned_data['tp_planned_course_date']) . '</b><br>' . "\n";
						$email_content .= 'Course Instructor: ' . '<b>' . strip_tags($planned_data['tp_planned_course_trainer']) . '</b><br>' . "\n";

						$email_content .= '<table class="regular thickbox botmar4" cellspacing="1" cellpadding="0" border="0" width="100%">' . "\n";
						$email_content .= '<tr>' . "\n";
						$email_content .= '<th width="120">Staff Number</td>' . "\n";
						$email_content .= '<th width="210">Username</td>' . "\n";
						$email_content .= '<th width="150">Sabre Qualification</td>' . "\n";
						$email_content .= '<th width="100" style="text-align:center">Status</td>' . "\n";
						$email_content .= '<th>Reason</td>' . "\n";
						$email_content .= '</tr>' . "\n";
						$flag_use_odd = false;
						foreach ( $planned_data['users'] as $_tp_planned_course_id => $user_data ) {
							$email_content .= '<tr ' . ( $flag_use_odd ? 'class="odd"' : '' ) . '>' . "\n";
							$email_content .= '<td align="center">' . $user_data['no_staff'] . '</td>' . "\n";
							$email_content .= '<td>' . ( $user_data['sabre_passed_status_order'] == 0 ? '<font color="red">' : '' ) . $user_data['user_name'] . ( $user_data['sabre_passed_status_order'] == 0 ? '</font>' : '' ) . '</td>' . "\n";
							$email_content .= '<td align="center">' . $user_data['sabre_code'] . '</td>' . "\n";
							$email_content .= '<td align="center">' . 
										     ( $user_data['sabre_passed_status'] == 1 ? 'Pass' : '' ) . 
											 ( $user_data['sabre_passed_status'] == 2 ? 'Pass-Retake' : '' ) . 
											 ( $user_data['sabre_passed_status'] == 3 ? 'Fail' : '' ) . 
											 ( $user_data['sabre_passed_status'] == 4 ? 'Complete' : '' ) . 
										     ( $user_data['sabre_passed_status'] == 5 ? 'Not Complete' : '' ) . 
										     ( $user_data['sabre_passed_status'] == 6 ? 'Not-Attended' : '' ) . 
										     '</td>' . "\n";
							$email_content .= '<td>' . strip_tags($user_data['sabre_none_passed_reason']) . '&nbsp;</td>' . "\n";
							$email_content .= '</tr>' . "\n";

							if ( $flag_use_odd ) {
								$flag_use_odd = false;
							} else {
								$flag_use_odd = true;
							}
						}
						$email_content .= '</table>' . "\n";
					}
				}

				$style_content = file_get_contents(ROOT_PROJ . 'css/style.css');

				$tp_admin_emails_query = $db->query('select conf_value from tp_config where conf_name="TP_ADMIN_EMAILS"');
				$tp_admin_emails_item = $tp_admin_emails_query->fetchRow();
	
				$admin_email_array = explode(',', $tp_admin_emails_item['conf_value']);	
		
				if ( count($admin_email_array) ) {
					foreach ( $admin_email_array as $key => $admin_email )	{
						$admin_email = trim($admin_email);
						if ( tep_validate_email($admin_email) ) {
							$correct_admin_email[] = $admin_email;
							mailProj(
								array (
									'to' => $admin_email,
									'subject' => 'Training Planner Notification',
									'body' => 'Dear Admin,<br><br>' . $email_content,
							),
							array (
								'{HEAD_ADDITIONAL}' => '<style type="text/css">' . $style_content . '</style>',
							));
						}
					}
				}
			} 
			// EOF send email section
	
			header('Location: ' . $_SERVER['PHP_SELF'] . '?success=course_completed'); exit;		
		}
	break;

	case 'add_new_user_to_existing_course':
	  $new_staff_no = trim($_POST['new_staff_no']);
	  if ( $new_staff_no != '' ) {
		$flag_error_new_user = false;

		$buf = explode(',', $new_staff_no);
		
		$buf[0] = trim($buf[0]); // staff_no
		$buf[1] = trim($buf[1]); // crew code
		
		/*
		$res = $db->query('SELECT DISTINCT no_staff, CONCAT(surname, ", ", firstname) as user_name, surname, firstname, rank     
						 		   FROM users 
                                   WHERE no_staff = "' . $buf[0] . '"');
		*/
		
		$res = $db->query('SELECT DISTINCT u.no_staff, CONCAT(u.surname, ", ", u.firstname) as user_name, u.surname, u.firstname, r.id_rank as rank        
						 		   FROM users u, users_ranks ur, ranks r  
                                   WHERE no_staff = "' . $buf[0] . '"
								     AND ur.id_rank = r.id_rank
								     AND ur.id_user = u.id_user');

		if ( $res->numRows() ) {
			$row = $res->fetchRow();
			$_rank = $row['rank'];
			
			$check_user_query = $db->query('SELECT * FROM tp_planned_courses_to_sabres WHERE no_staff = "' . $buf[0] . '" and tp_planned_course_id = ' . (int)$_GET['tp_planned_course_id']);
			if ( $check_user_query->numRows() ) {
				$detected_user = userNameByStaffNo($buf[0]);

				errorToPrint($detected_user['surname'] . ', ' . $detected_user['firstname'] . ' already added!');
			
				$flag_error_new_user = true;
			}
		} else {
			errorToPrint('User not detected!');
			
			$flag_error_new_user = true;
		}

/*
		if ( $flag_error_new_user == false ) {
			$db->query('insert into tp_planned_courses_to_sabres(tp_planned_course_id, no_staff, date_added) values("' . (int)$_GET['tp_planned_course_id'] . '", "' . $buf[0] . '", ' . TIME . ')');
			
			successToPrint('User successfully added!');
		}
*/
		if ( $flag_error_new_user == false ) {
			$get_sabres_from_course_query = $db->query('SELECT distinct sq.sabre_id, sq.sabre_rank
						FROM ' . $CONFP['TABLE_TP_SABRE_QUALIF'] . ' sq, ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' pc, ' . $CONFP['TABLE_TP_SABRES_TO_COURSES'] . ' s2c
						WHERE s2c.sabre_id = sq.sabre_id
						  and s2c.tp_courses_id = pc.tp_courses_id
						  and pc.tp_planned_course_id = ' . (int)$_GET['tp_planned_course_id'] . ' 
					 order by sq.sabre_id asc');

			$flag_user_inserted = false;
			while ( $sabre_item = $get_sabres_from_course_query->fetchRow() ) {
				$flag_use_logic = false;

				if ( $sabre_item['sabre_rank'] == 0 ) {
					$flag_use_logic = true;
				} elseif ( ( $sabre_item['sabre_rank'] == 1 ) && ( in_array($_rank, $CONFP['RANKS_PILOTS']) ) ) {
					$flag_use_logic = true;
				} elseif ( ( $sabre_item['sabre_rank'] == 2 ) && ( in_array($_rank, $CONFP['RANKS_CREWS'])  ) ) {
					$flag_use_logic = true;
				}

				if ( $flag_use_logic && crewCodeAssignedToSabre($buf[1], $sabre_item['sabre_id']) ) {
					$username_info = userNameByStaffNo($buf[0]);
					
					if ( ! checkSabreExistsForUserInCourse((int)$_GET['tp_planned_course_id'], $sabre_item['sabre_id'], $buf[0]) ) {
						$db->query('insert into tp_planned_courses_to_sabres(
									tp_planned_course_id, 
									sabre_id,
									no_staff,
									firstname, 
									surname, 
									sabre_added_status,
									date_added,
									manually_added_by_trainer
									) values(
									"' . (int)$_GET['tp_planned_course_id'] . '", 
									' . $sabre_item['sabre_id'] . ',
									"' . $buf[0] . '", 
									"' . $username_info['firstname'] . '",
									"' . $username_info['surname'] . '",
									1,
									' . TIME . ',
									1)');

						$flag_user_inserted = true;
					}
				} 
				
				if ( $flag_user_inserted == false ) {
					$db->query('insert into tp_planned_courses_to_sabres(tp_planned_course_id, no_staff, date_added) values("' . (int)$_GET['tp_planned_course_id'] . '", "' . $buf[0] . '", ' . TIME . ')');
				}
			}	
			successToPrint($row['surname'] . ', ' . $row['firstname'] . ' successfully added!');	
		}
	  }
	break;
}

// BOF get user's data
$courses_users_table = get_courses_users_table($current_tp_planned_course_id, true);
// EOF get user's data

$days_array = array();
//$days_array[] = array( 'id' => 0, 'text' => 'DD' );
for ($i = 1, $N = 31; $i <= $N; $i++ ) {
	$days_array[] = array( 'id' => $i, 'text' => $i );
}

$months_array = array();
//$months_array[] = array( 'id' => 0, 'text' => 'MM' );
for ($i = 1, $N = 12; $i <= $N; $i++ ) {
	$months_array[] = array( 'id' => $i, 'text' => date("M", mktime(0, 0, 0, $i, 1, 2013)) );
}

// BOF year section
$min_passed_date = get_min_passed_date_of_course($current_tp_planned_course_id);
$max_passed_date = get_max_passed_date_of_course($current_tp_planned_course_id);

// BOF changed by Alex CREHelp
//$year_range_start = date('Y');
//$year_range_start = 2012;
//$year_range_start = date('Y') - 10;
$year_range_start = 1988;
// EOF changed by Alex CREHelp

$year_range_finish = date('Y') + 1;

if ( $min_passed_date > 0 ) {
	$min_passed_date = date('Y', $min_passed_date);

	if ( $min_passed_date < $year_range_start )	{
		$year_range_start = $min_passed_date;
	} 
}

if ( $max_passed_date > 0 ) {
	$max_passed_date = date('Y', $max_passed_date);

	if ( $max_passed_date > $year_range_finish )	{
		$year_range_finish = $max_passed_date;
	} 
}

$years_array = array();
//$years_array[] = array( 'id' => 0, 'text' => 'YY' );
for ( $i = $year_range_start, $N = $year_range_finish; $i <= $N; $i++ ) {
	$years_array[] = array( 'id' => $i, 'text' => $i );
}
// EOF year section

switch ( @$_GET['success'] ) {
	case 'saved_course': successToPrint('Successfully saved'); break;
	case 'course_completed': successToPrint('Course successfully completed'); break;
}

includeJS();

pageAutoAssignVars('courses_users_table', 
                   'planned_courses', 
				   'planned_courses_dates', 
				   'current_tp_courses_id', 
				   'current_tp_planned_course_id', 
				   'default_day',
				   'default_month',
				   'default_year',
				   'mpdf', 
				   'days_array',
				   'months_array',
				   'years_array',
				   'courses_sabres_table');

if ( isset($_GET['action']) && $_GET['action'] == 'print_course' ) {
	pageDisplay( 'trainer/reports/index');
} else {
	pageDisplay();
}
?>

