$(document).ready(function()
{
	if ( 'undefined' != typeof($.tablesorter) )
	{
		$.tablesorter.addParser(
		{
			id:	'myField',
			is:	function(s)	{ return false; },
			format:	function(s, table, cell)
			{
				var s1 = $.trim($(cell).attr('sortValue'));
				return s1 ? s1 : s;
			},
			type: 'text'
		});
	}
});
