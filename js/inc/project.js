IN_FRAME = window.frameElement && window.parent;

function frameOnLoad()
{
	if ( IN_FRAME && 'undefined' != typeof(window.parent.frameOnLoad) )
	{
		window.parent.frameOnLoad();
	}
}

function frameMinHeight(h)
{
	if ( ! IN_FRAME ) return;

	var $main = $('#main');
	if ( $main.length > 0 && $main.height() < h ) $main.height(h + 10); 

	frameOnLoad();
}

// ---------------------------------------------

$(document).ready(function()
{
	if ( ie6 ) $('body').addClass('ie6');

	if ( errors ) alertError(errors);
	else if ( messages ) alertSuccess(messages);
	else if ( info_messages ) alertSuccess(info_messages, 0);

	$('#nav_menu').change(function()
	{
		var href = $(this).val();
		if ( this.tp && this.tp.beforeChange && $.isFunction(this.tp.beforeChange) )
		{
			if ( false === this.tp.beforeChange(this, function(){ if ( href ) window.location = href; }) )
			{
				this.selectedIndex = 0;
				return false;
			}
		}
		
		this.selectedIndex = 0;
		if ( href ) window.location = href;
	});

	$('select.autogoto').change(function()
	{
		var href = $(this).val();
		if ( href ) window.location = href;
	});

	$('.goprint').click(function()
	{
		$(this).blur();
		window.print();
	});

	$('input[popwin]').click(function()
	{
		$(this).blur();
		popup($(this).attr('popwin'), 877, 600, 'popwin', 'location=no,menubar=no,resizable=no,scrollbars=yes,status=yes,toolbar=no');
		return false;
	});
});

(function($)
{
	$.fn.extend(
	{
		redrawOddRows: function()
		{
			return this.each(function()
			{
				$(this).find('tbody').children('tr').not(':odd').removeClass('odd').end().filter(':odd').addClass('odd');
			});
		},
		redrawOddRowsOnSort: function()
		{
			return this.each(function()
			{
				$(this).bind('sortEnd', function()
				{
					$(this).redrawOddRows();
				});
			});
		}
	});
})(jQuery);
