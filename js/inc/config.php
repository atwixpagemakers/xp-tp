<?php

$NO_LOGIN_NEED = true;

require_once '../../inc/global.php';

header("Content-type: text/javascript; charset=utf-8");
header("Last-Modified: " . gmdate('D, d M Y H:i:s T', getlastmod()));


foreach (array
(
	'HTTP_PATH',
)
as $k)
{
	if ( ! array_key_exists($k, $CONFP[$k]) ) continue;

	echo $k . ' = ' . json_encode($CONFP[$k]) . ';' . "\n";
}

exit; # !!!

?>
KEEP_ALIVE_TIME = <?=intval($C['config']['logout_time']) * 20?>;
