errors = 'undefined' != typeof(errors) ? errors : '';
messages = 'undefined' != typeof(messages) ? messages : '';
info_messages = 'undefined' != typeof(info_messages) ? info_messages : '';

ie6 = ($.browser.msie && parseInt($.browser.version) == 6);
ie7 = ($.browser.msie && parseInt($.browser.version) == 7);

function alertError(message, autoClose)
{
	$.facebox('<div class="alert alertError"><p>' + message + '</p></div>');
	autoClose = autoClose || 0;
	if ( autoClose ) setTimeout('$.facebox.close()', autoClose);
}

var alertSucessAutoClose = 2500;
function alertSuccess(message, autoClose)
{
	$.facebox('<div class="alert alertSuccess"><p>' + message + '</p></div>');
	
	autoClose = 'undefined' == typeof(autoClose) ? (alertSucessAutoClose || 0) : 0;
	if ( autoClose ) setTimeout('$.facebox.close()', autoClose);
}

function popup(url, width, height, name, parms)
{
	if (width == '') width = ie7 ? document.body.clientWidth : window.innerWidth;
	if (height == '') height = ie7 ? document.body.clientHeight : height = window.innerHeight;

	var left = Math.floor( (screen.width - width) / 2);
	var top = Math.floor( (screen.height - height) / 2);
	var winParms = "top=" + top + ",left=" + left + ",height=" + height + ",width=" + width;
	if (parms) { winParms += "," + parms; }
	var win = window.open(url, name, winParms);
	if (win && (parseInt(navigator.appVersion) >= 4)) { 
		win.window.focus(); 
	}
	return win;
}

function preg_quote(str)
{
    // http://kevin.vanzonneveld.net
    // +   original by: booeyOH
    // +   improved by: Ates Goral (http://magnetiq.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Onno Marsman
    // *     example 1: preg_quote("$40");
    // *     returns 1: '\$40'
    // *     example 2: preg_quote("*RRRING* Hello?");
    // *     returns 2: '\*RRRING\* Hello\?'
    // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
    // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'
 
    return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!<>\|\:])/g, "\\$1");
}

function isDate(s)
{
	var r = new RegExp("^((19|20)\\d\\d)-(0{0,1}[1-9]|1[012])-(0{0,1}[1-9]|[12][0-9]|3[01])$");

	var arr = r.exec(s);
	if ( arr )
	{
		var mon = arr[3], day = arr[4], yr = arr[1];
		if (day == 31 && (mon == 4 || mon == 6 || mon == 9 || mon == 11)) return false;
		if (day >= 30 && mon == 2) return false;
		if (mon == 2 && day == 29 && !(yr % 4 == 0 && (yr % 100 != 0 || yr % 400 == 0))) return false;
		return yr + '-' + (mon.length<2?'0':'') + mon + '-' + (day.length<2?'0':'') + day;
	}

	return false;
}

function dateToYmd(d)
{
	if ( 'function' != typeof(d.getMonth) ) return '';
	var y = d.getFullYear();
	var m = d.getMonth() + 1;
	var d = d.getDate();
	return y + '-' + (m < 10 ? '0' : '') + m + '-' + (d < 10 ? '0' : '') + d;
}

function ymdToDate(ymd)
{
	var ymd = isDate(ymd);
	if ( ! ymd ) return false;
	var arr = ymd.split('-');
	var year = arr[0], mon = parseInt(arr[1].replace(/^0*/, '')), day = parseInt(arr[2].replace(/^0*/, ''));
	return new Date(year, mon-1, day, 12, 1, 0);
}

function monthName(o, shrt)
{
	var m = 0;
	m = 'function' == typeof(o.getMonth) ? o.getMonth() : parseInt(o.replace(/^0*/, '')) - 1;
	var mnths = shrt ? ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'] : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	return mnths[m] ? mnths[m] : '';
}

Number.prototype.toOrdinal = String.prototype.toOrdinal = function()
{
	var n = parseInt(this.replace(/^0*/, '')) % 100;
	var suff = ["th", "st", "nd", "rd", "th"];
	var ord = n < 21 ? (n < 4 ? suff[n] : suff[0]) : (n%10 > 4 ? suff[0] : suff[n%10]);
	return this + ord;
}

function refreshOpener()
{
	// add_url format: [[sUrl1, re1], [sUrl2, re2]]
	// sUrl adding to the end of opener query string
	// if re (RegExp) is specified - sUrl will be adding only when opener query string in not match the pattern

	if ( window.opener )
	{
		var url = window.opener.location.href;
		var ai, i, add, re;

		for (ai=0; ai<arguments.length; ai++)
		{
			if ( ! arguments[ai] ) continue;

			add = arguments[ai][0] || '';
			re = arguments[ai][1] || '';

			if ( add && (! re || ! re.test(url)) )
			{
				url += url.match(/\?/) ? '&' : '?';
				url += add;
			}
		}

		window.opener.location = url;
	}
}

// ---------------------------------------------

// -------------------------
// Placeholders

(function($)
{
	$.fn.extend(
	{
		placeholder: function()
		{
			return this.each(function()
			{
				$(this)
				.focus(function()
				{
					$(this).removeClass('inpInact');
					if ( $(this).val() == $(this).attr('placeholder') ) $(this).val('');
				})
				.blur(function()
				{
					if ( !$.trim($(this).val()) || $(this).val() == $(this).attr('placeholder') )
					{
						$(this).addClass('inpInact');
						$(this).val($(this).attr('placeholder'));
					}
					else $(this).removeClass('inpInact');
				})
				.keydown(function()
				{
					$(this).removeClass('inpInact');
				})
				.trigger('blur');
			});
		}
	});
})(jQuery);

// Placeholders
// -------------------------

$(document).ready(function()
{
	$(':text[placeholder]').placeholder();
});
