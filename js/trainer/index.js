$(document).ready(function() {
	var flag_new_changes = false;
	var flag_horizontal_check_boxchecked = false;
	var flag_vertical_check_boxchecked = false;
	var selected_marks = 0;
	var selected_reason = "";
	var selected_sabre_id = 0;
	var selected_input_code = 0;
	var selected_sabre_pf = 0;
	var selected_passed_date_day = 0;
	var selected_passed_date_month =  0;
	var selected_passed_date_year =  0;

	$("#user_course").change(function () {
		$("#form_user_course").submit();
	});			
	
	$("#user_course_modules").change(function () {
		$('#form_user_course_modules').submit();
	});			
	
	$(".tp_sabre").click(function (e) {	
		hide_all_popups();
		
		selected_sabre_id = $(this).attr("sabre_id");

		flag_continue = true;
		if ( flag_vertical_check_boxchecked ) {
			flag_continue = false;
			$("input:checkbox[class=sabre_checkbox]").each( function() {
				if ( $(this).attr("checked")==true && selected_sabre_id == $(this).attr("sabre_id") ) {
					flag_continue = true;
				}
			});
		}
		if ( flag_continue == false ) {
			$("input:checkbox[class=sabre_checkbox]").each( function() {
				if ( $(this).attr("checked")==true ) {
					$(this).attr("checked", "");
				}
			});

			clean_selected_var_params();
		}

		if ( ( flag_horizontal_check_boxchecked || flag_vertical_check_boxchecked ) && selected_sabre_pf != $(this).attr("sabre_pf") ) {
			selected_marks = 0;
		}

		if ( selected_marks == 0 ) {
			selected_input_code = $(this).attr("input_code");
			selected_sabre_pf = $(this).attr("sabre_pf");
					
			pos_left = e.pageX + 2;
			if ( pos_left > 660 ) {
				pos_left = e.pageX - 215;
			}
	
			$( '#popup_module_marks' + $(this).attr("input_code") ).css({
    			left:		pos_left,
				top:		e.pageY - 2
			});

			$( '#popup_module_marks' + $(this).attr("input_code") ).show();
		}
	});	
	
	$("a.tp_close_popup").bind('click touchend', function(e) {
		hide_all_popups();
	});	
	
	$("li.popup_module_marks_item").hover( function () {
		$(this).css("background", "#efefef");
	}, function () {
		$(this).css("background", "#ffffff");
	});	

	$("li.popup_module_marks_item a").click( function () {
		var tp_planned_courses_to_sabres_id = $(this).attr("input_code");
		var sabre_passed_status = $(this).attr("sabre_passed_status");

		if ( check_mark_date(tp_planned_courses_to_sabres_id)==false ) {
			return false;
		} else {
			selected_passed_date_day = $("select[name='passed_date_day[" + tp_planned_courses_to_sabres_id + "]']").val();
			selected_passed_date_month = $("select[name='passed_date_month[" + tp_planned_courses_to_sabres_id + "]']").val();
			selected_passed_date_year = $("select[name='passed_date_year[" + tp_planned_courses_to_sabres_id + "]']").val();
		}
		
		set_mark(tp_planned_courses_to_sabres_id, sabre_passed_status, true);

		if ( flag_horizontal_check_boxchecked ) {
			$("input:checkbox[class=user_checkbox]:checked").each( function() {
				tmp_staff_no = $(this).attr("staff_no");

				$("img[class=tp_sabre][staff_no=" + tmp_staff_no + "]").each( function() {
					input_code = $(this).attr("input_code");
					sabre_pf = $(this).attr("sabre_pf");
			
					if ( tp_planned_courses_to_sabres_id != input_code && selected_sabre_pf == sabre_pf ) {
						set_mark(input_code, sabre_passed_status, false);
					}
				});
			});			
		}
		
		if ( flag_vertical_check_boxchecked ) {
			//$("input:checkbox[class=sabre_checkbox]:checked").each( function() {
			$("input:checkbox.sabre_checkbox:checked").each( function() {
				//$("img[class=tp_sabre][sabre_id=" + selected_sabre_id + "]").each( function() {
				$("img.tp_sabre[sabre_id=" + selected_sabre_id + "]").each( function() {
					tmp_staff_no = $(this).attr("staff_no");
					input_code = $(this).attr("input_code");
					sabre_pf = $(this).attr("sabre_pf");
					
					if ( tp_planned_courses_to_sabres_id != input_code && selected_sabre_pf == sabre_pf ) {
						set_mark(input_code, sabre_passed_status, false);
					}
				});
			});
		}
	});	
	
	$("input.main_checkbox").click( function () {
		if ( $(this).attr("checked")==true ) {
			user_checkbox_status = "checked";

			flag_horizontal_check_boxchecked = true;
			
			flag_vertical_check_boxchecked = false;
			$("input:checkbox[class=sabre_checkbox]").each( function() {
				if ( $(this).attr("checked")==true ) {
					$(this).attr("checked", "");
				}
			});
			
//			clean_selected_var_params();
		} else {
			user_checkbox_status = "";
			
			flag_horizontal_check_boxchecked = false;
//			selected_marks = 0;
//			selected_reason = "";
//			selected_sabre_id = 0;
//			selected_input_code = 0;
//			selected_sabre_pf = 0;
		}

		clean_selected_var_params();

		$("input:checkbox[class=user_checkbox]").each( function() {
			$(this).attr("checked", user_checkbox_status);
		});
	});	

	function clean_selected_var_params() {
		selected_marks = 0;
		selected_reason = "";
		selected_sabre_id = 0;
		selected_input_code = 0;
		selected_sabre_pf = 0;
	}
	
	$("input:checkbox[class=user_checkbox]").click( function () {	
		flag_horizontal_check_boxchecked = false;
		$("input:checkbox[class=user_checkbox]").each( function() {
			if ( $(this).attr("checked")==true ) {
				flag_horizontal_check_boxchecked = true;
			}
		});

		if (!flag_horizontal_check_boxchecked) {
//			clean_selected_var_params();
		} else {
			flag_vertical_check_boxchecked = false;
			$("input:checkbox[class=sabre_checkbox]").each( function() {
				if ( $(this).attr("checked")==true ) {
					$(this).attr("checked", "");
				}
			});
			
//			clean_selected_var_params();
		}
		clean_selected_var_params();
	});

	$("input:checkbox[class=sabre_checkbox]").click( function () {	
		flag_vertical_check_boxchecked = false;

		$("input:checkbox[class=sabre_checkbox]").each( function() {
			if ( $(this).attr("checked")==true ) {
				flag_vertical_check_boxchecked = true;
			}
		});

		if ( !flag_vertical_check_boxchecked ) {
//			clean_selected_var_params();
		} else {
			flag_horizontal_check_boxchecked = false;
			
			$("input:checkbox[class=user_checkbox]").each( function() {
				if ( $(this).attr("checked")==true ) {
					$(this).attr("checked", "");
				}
			});

			$("input.main_checkbox").attr("checked", "");
//			clean_selected_var_params();
		}
		
		clean_selected_var_params();
	});
	
	function set_mark(tp_planned_courses_to_sabres_id, sabre_passed_status, apply_new_status) {
		flag_empty_text_error = false;
		reason_text = " ";
		
		if ( ((sabre_passed_status == 3) || (sabre_passed_status == 5)) && (selected_marks==0) ) {
			reason_text = prompt( "Enter a reason : ", $( "#sabre_none_passed_reason" + tp_planned_courses_to_sabres_id ).attr("value") );
			reason_text = jQuery.trim(reason_text);
			
			if ( reason_text == "") {
				alert("You should enter a reason!");
				flag_empty_text_error = true;
			}
		} 	

		if ( ( flag_empty_text_error == false ) && ( $( "#sabre_passed_status" + tp_planned_courses_to_sabres_id ).attr("value") == 0 || apply_new_status ) ) {
			switch ( sabre_passed_status ) {
				case '1':
					$( "img[class=tp_sabre][input_code=" + tp_planned_courses_to_sabres_id + "]" ).attr("src", "images/circle_green.png");
				break;
				case '2':
					$( "img[class=tp_sabre][input_code=" + tp_planned_courses_to_sabres_id + "]" ).attr("src", "images/circle_yellow.png");		
				break;
				case '3':
					$( "img[class=tp_sabre][input_code=" + tp_planned_courses_to_sabres_id + "]" ).attr("src", "images/circle_red.png");		
				break;
				case '4':
					$( "img[class=tp_sabre][input_code=" + tp_planned_courses_to_sabres_id + "]" ).attr("src", "images/circle_green.png");		
				break;
				case '5':
					$( "img[class=tp_sabre][input_code=" + tp_planned_courses_to_sabres_id + "]" ).attr("src", "images/circle_red.png");		
				break;
				case '6':
					$( "img[class=tp_sabre][input_code=" + tp_planned_courses_to_sabres_id + "]" ).attr("src", "images/circle_purple.png");		
				break;
			}
		
			$( "#sabre_passed_status" + tp_planned_courses_to_sabres_id ).attr("value", sabre_passed_status);
			$( "#popup_module_marks" + tp_planned_courses_to_sabres_id ).hide();		

			$("select[name='passed_date_day[" + tp_planned_courses_to_sabres_id + "]'] option[value='" + selected_passed_date_day + "']").attr("selected", "selected");
			$("select[name='passed_date_month[" + tp_planned_courses_to_sabres_id + "]'] option[value='" + selected_passed_date_month + "']").attr("selected", "selected");
			$("select[name='passed_date_year[" + tp_planned_courses_to_sabres_id + "]'] option[value='" + selected_passed_date_year + "']").attr("selected", "selected");

			if ( flag_horizontal_check_boxchecked || flag_vertical_check_boxchecked ) {
				if ( selected_marks==0 ) {
					selected_marks = sabre_passed_status;
					selected_reason = reason_text;
				} else {
					reason_text = selected_reason;
				}
			}


			$( "#sabre_none_passed_reason" + tp_planned_courses_to_sabres_id ).attr("value", reason_text);		
		}
	}
	
	function hide_all_popups() {
		$(".popup_module_marks").each( function() {
			$(this).hide();
		});
	}
	
	function check_mark_date(tp_planned_courses_to_sabres_id) {
		passed_date_day = $("select[name='passed_date_day[" + tp_planned_courses_to_sabres_id + "]']").val();
		passed_date_month = $("select[name='passed_date_month[" + tp_planned_courses_to_sabres_id + "]']").val();
		passed_date_year = $("select[name='passed_date_year[" + tp_planned_courses_to_sabres_id + "]']").val();
		
		flag_error = false;
		
		if ( passed_date_day == 0 || passed_date_month == 0 || passed_date_year ==0 ) {
			flag_error = true;
		} else {
			if ( passed_date_year > default_year ) {
				flag_error = true;
			}
			
			if ( (passed_date_month > default_month) && (passed_date_year == default_year) ) {
				flag_error = true;
			}

			if ( (passed_date_day > default_day) && (passed_date_month == default_month) && (passed_date_year == default_year) ) {
				flag_error = true;
			}
		}
		
		if ( flag_error==false ) {
			return true;		
		} else {
			alert('Please check date.');
			
			return false;
		}
	}
	
	
//	$("#trainer_courses_save").submit(function () {
//		$(".ch_mark_ddm").remove();
		
//		return true;
//	});			
	//**************************************************************************
	//                              BOF custom user
	//**************************************************************************
	$("input.img_add_new_user").click(function () {
		while (true) {
			new_staff_no = prompt( "Enter staff number. (EG: 12345,3M)", " " );

			if ( new_staff_no==' ' ) {
				alert('Please enter staff number!');
			} else {
				if ( new_staff_no.length > 0 ) {
					$("#new_staff_no").attr("value", new_staff_no);
					$("#course_custom_user").submit();
				}
			
				break;
			}
		}
	});			
	
	$(".tp_sabre_custom").click(function () {
		switch ( $("#i" + $(this).attr('input_code')).attr("value") ) {
				case '0':
					$("#i" + $(this).attr('input_code')).attr("value", "2");
					$("#img" + $(this).attr('input_code')).attr("src", "images/circle_red.png");
					$("#img" + $(this).attr('input_code')).attr("title", "Module is applicable to this user");
				break;
				case '2':
					$("#i" + $(this).attr('input_code')).attr("value", "0");
					$("#img" + $(this).attr('input_code')).attr("src", "images/circle_blank.png");
					$("#img" + $(this).attr('input_code')).attr("title", "Module not applicable to this crew member");
				break;
				
				default: 
					$("#i" + $(this).attr('input_code')).attr("value", "2");
					$("#img" + $(this).attr('input_code')).attr("src", "images/circle_red.png");
					$("#img" + $(this).attr('input_code')).attr("title", "Module is applicable to this user");
		}
			
		flag_new_changes = true;
	});
	
	//**************************************************************************
	//                              EOF custom user
	//**************************************************************************
});
