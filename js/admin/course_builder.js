$(document).ready(function()
{
	var $form = $('form[name=delete]');
	var $checks = $form.find(':checkbox');

	$form.submit(function()
	{
		if ( ! $checks.filter(':checked').length )
		{
			alertError('Please select items to delete', 1500);
			return false;
		}
		return confirm('Are you sure you want to delete selected items?');
	});
});
