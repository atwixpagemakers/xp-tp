$(document).ready(function() {
	$("#user_course").change(function () {
		$('#form_user_course').submit();
	});			

	$("#user_course_modules").change(function () {
		$('#form_user_course_modules').submit();
	});			

	/*
	
	
	$(".tp_sabre").click(function () {
		if ( confirm('Toggle?') ) {
			switch ( $("#i" + $(this).attr('input_code')).attr("value") ) {
				case '0':
					$("#i" + $(this).attr('input_code')).attr("value", "1");
					$("#img" + $(this).attr('input_code')).attr("src", "images/circle_black.png");
					$("#img" + $(this).attr('input_code')).attr("title", "Module is applicable to this user");
				break
				case '1':
					$("#i" + $(this).attr('input_code')).attr("value", "2");
					$("#img" + $(this).attr('input_code')).attr("src", "images/circle_red.png");
					$("#img" + $(this).attr('input_code')).attr("title", "Module is applicable to this user as additional");
				break
				case '2':
					$("#i" + $(this).attr('input_code')).attr("value", "0");
					$("#img" + $(this).attr('input_code')).attr("src", "images/circle_blank.png");
					$("#img" + $(this).attr('input_code')).attr("title", "Module not applicable to this crew member");
				break
			}
		}
	});
	
	$("#course_planner_save").submit(function () {
		if ( confirm('Save course?') ) {
			flag_dates_filled = true;
			$(".planned_course_date").each( function() {
				if ( $(this).attr('value')=='' ) {
					flag_dates_filled = false;
				}
			});
		
			if ( flag_dates_filled == false ) {
				alert('Please check if all date fields are set for available courses!');
				return false;
			}
		} else {
			return false;		
		}
	});
	
	*/
});
