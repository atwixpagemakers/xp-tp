function check_date_range() {
		error_message = "";
		flag_no_errors = true;
		
		tp_month_from = $("#tp_month_from").attr("value") * 1;
		tp_year_from = $("#tp_year_from").attr("value") * 1;
		tp_month_to = $("#tp_month_to").attr("value") * 1;
		tp_year_to = $("#tp_year_to").attr("value") * 1;
		
		if ( tp_month_from == 0 ) {
			error_message += "* 'From Month' field is empty!" + "\n";

			flag_no_errors = false;
		}

		if ( tp_year_from == 0 ) {
			error_message += "* 'From Year' field is empty!" + "\n";

			flag_no_errors = false;
		}

		if ( tp_month_to == 0 ) {
			error_message += "* 'To Month' field is empty!" + "\n";

			flag_no_errors = false;
		}

		if ( tp_year_to == 0 ) {
			error_message += "* 'To Year' field is empty!" + "\n";

			flag_no_errors = false;
		}

		if ( tp_year_to > 0 && tp_month_to > 0 && tp_month_from > 0 && tp_year_from > 0 ) {
			if ( tp_year_from > tp_year_to ) {
				error_message += "* 'From Year' field is more than 'To Year' field!" + "\n";

				flag_no_errors = false;
			}

			if ( tp_year_to == tp_year_from ) {
				if ( tp_month_from > tp_month_to ) {
					error_message += "* 'From Month' field is more than 'To Month' field!" + "\n";

					flag_no_errors = false;
				}
			}
		}

		if ( flag_no_errors == false ) {
			alert(error_message);
		}
	
		return flag_no_errors;
}

$(document).ready(function() {
/*
	$("#tp_report_type").change(function () {
		$("#tp_report_form").submit();
	});
*/
	
// *****************************************************************************
//                            BOF section for EXPIRY report and MODULE report
// *****************************************************************************
	$("#tp_report_expiry").submit(function () {
		return check_date_range();
		/*
		error_message = "";
		flag_error = false;
		
		tp_month_from = $("#tp_month_from").attr("value");
		tp_year_from = $("#tp_year_from").attr("value");
		tp_month_to = $("#tp_month_to").attr("value");
		tp_year_to = $("#tp_year_to").attr("value");
		
		if ( tp_month_from == 0 ) {
			error_message += "* 'From Month' field is empty!" + "\n";

			flag_error = true;
		}

		if ( tp_year_from == 0 ) {
			error_message += "* 'From Year' field is empty!" + "\n";

			flag_error = true;
		}

		if ( tp_month_to == 0 ) {
			error_message += "* 'To Month' field is empty!" + "\n";

			flag_error = true;
		}

		if ( tp_year_to == 0 ) {
			error_message += "* 'To Year' field is empty!" + "\n";

			flag_error = true;
		}

		if ( tp_year_to > 0 && tp_month_to > 0 && tp_month_from > 0 && tp_year_from > 0 ) {
			if ( tp_year_from > tp_year_to ) {
				error_message += "* 'From Year' field is more than 'To Year' field!" + "\n";

				flag_error = true;
			}

			if ( tp_year_to == tp_year_from ) {
				if ( tp_month_from > tp_month_to ) {
					error_message += "* 'From Month' field is more than 'To Month' field!" + "\n";

					flag_error = true;
				}
			}
		}

		if ( flag_error ) {
			alert(error_message);
			
			return false;
		}
		*/
	});
// *****************************************************************************
//                            EOF section for EXPIRY report
// *****************************************************************************

// *****************************************************************************
//                            BOF section for USER report
// *****************************************************************************
	$("#tp_report_user").submit(function () {
		error_message = "";
		flag_error = false;

//		$("#report_user_tp_planned_course_id").attr("value", "0");
		
		if ( $("#report_user_no_staff").attr("value")=="" ) {
			error_message = "User field is empty!";	
			
			flag_error = true;
		}
		
		if ( flag_error ) {
			alert(error_message);
			
			return false;
		}
	});	

	$("#tp_report_user_course").change(function () {
//		if ( $(this).attr("value") == 0 ) {
//			$("table[class='regular thickbox botmar4 report_user']").show();		
//		} else {
			$("table.report_user").hide();
		
			$( "#tp_planned_course_id_" + $(this).attr("value") ).show();
//		}

		frameOnLoad();
	});
	
	$("a.report_mod_sort_url").click(function () {
		current_href = $(this).attr("href");
		
		$(this).attr("href", current_href + "&tp_planned_course_id=" + $("#tp_report_user_course").val());
		
		return true;
	});

	$("#form_pdf_report_user").submit(function () {
		current_action = $(this).attr("action");
//alert(current_action + "&tp_planned_course_id=" + $("#tp_report_user_course").val());
		$(this).attr("action", current_action + "&tp_planned_course_id=" + $("#tp_report_user_course").val());
	});	
	

	jQuery.fn.Hint = function (o) {
		o = jQuery.extend({trigger:"mouseover"}, o);

	    function hint(e){
    	    $("<div class='hint_box' style='left:"+($(e).position().left - 100 + $(e).width())+"px; top:"+($(e).position().top + 3 + $(e).height())+"px; position:absolute;padding:10px;width:200px; background-color:#fff; border:1px solid red; z-index:20; font-size:12px; color: red;'>" + $(e).attr("hint")+"</div>").appendTo("body");
    	};
	
	    $(this).find('[hint]').each(function(){
        if(o.trigger=="mouseover"){
            $(this).mouseover(function(){
                $(this).css({"cursor":"pointer"});
                hint(this);
            }).mouseout(function(){
                $('.hint_box').remove();
            });
        }
   		});
	}

	$('body').Hint({
		trigger: "mouseover"
	});
// *****************************************************************************
//                            EOF section for USER report
// *****************************************************************************

// *****************************************************************************
//                            BOF section for COURSE report
// *****************************************************************************
	//$("#tp_report_course_id").change(function () {
	//	$("#tp_report_course_form").submit();
	//});

	$("#tp_report_course_form").submit(function () {
		if ( $("#tp_report_course_id").val()==0 ) {
			alert('Select course, please!');
			
			return false;
		} else {
			return check_date_range();
		}
	});	

	$("#form_pdf_report_course").submit(function () {
		$(".report_main_container").show();
		
		if ( typeof(pdf_global_time) != undefined ) {
			setInterval(hideProgress, (pdf_global_time * 1000));
		}

//		return false;
	});	
// *****************************************************************************
//                            EOF section for COURSE report
// *****************************************************************************


// *****************************************************************************
//                            BOF section for FAIL report
// *****************************************************************************
	$("input.include_deleted_users").change(function () {
		if ( $(this).is(":checked") ) {
			$("tr.block_base").hide();
		} else {
			$("tr.block_base").show();		
		}

		frameOnLoad();
	});
// *****************************************************************************
//                            EOF section for FAIL report
// *****************************************************************************
});

function hideProgress() {
	$(".report_main_container").hide();
}
