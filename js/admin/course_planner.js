$(document).ready(function() {
	var flag_new_changes = false;
	var ddm_current_values_array = new Array();
	var curr_user_course_value = $("#user_course option:selected").val();

	$("select[class=ddm_additional]").each( function() {
		ddm_current_values_array[$(this).attr("ddm_id")] = $(this).attr("value");
	});
	
	$("input.img_add_new_user").click(function () {
		while (true) {
			new_staff_no = prompt( "Enter staff number and current qualification. (EG: 12345,3M)", " " );
			//alert(new_staff_no);
			
			if ( new_staff_no==' ' ) {
				alert('Please enter staff number and current qualification!');
			} else {
				if ( new_staff_no.length > 0 ) {
					
					staff_array = new_staff_no.split(',');
					//if ( ($("#row_" + staff_array[0]).length) && ($( "#row_" + staff_array[0]).is(':visible')==false) ) {
					if ( ($("tr[user_id=row_" + staff_array[0] + "]").length) && ($( "tr[user_id=row_" + staff_array[0] + "]" ).is(':visible')==false) ) {
					//user_id
						data_array = {};
						
						staff_no = staff_array[0];
						
						data_array.action = 'add_user_to_corse';
						data_array.staff_no = staff_no;
				
						$.ajax({
		    	    		type: "POST",
	    			    	url: "/external/tp/admin/course_planner_ajax.php",
		    			    data: data_array,
				    		dataType: "json",
				    		timeout: 5000,
    		    			success: function(data) { 
								$( "input[name='delete_staff_no[" + staff_no + "]']").attr("value", "0");
					
								//$( "#row_" + staff_no ).show();
								$( "tr[user_id=row_" + staff_no + "]" ).show();
							}
    					}); 
					} else {
						$("#new_staff_no").attr("value", new_staff_no);
						$("#course_custom_user").submit();
					}
				}
				
				break;
			}
		}
	});			
	
	$("img.img_delete_user").click(function () {
		if ( confirm("Are you sure you want to delete?") ) {
			staff_no = $(this).attr("user_staff_no");
			
			data_array = {};
			
			if ( $(this).attr("operation_type")=="remove_row" ) {
				data_array.action = 'remove_user_from_session';
				data_array.staff_no = staff_no;
				
				$.ajax({
    	    		type: "POST",
	    	    	url: "/external/tp/admin/course_planner_ajax.php",
		    	    data: data_array,
		    		dataType: "json",
		    		timeout: 5000,
    		    	success: function(data) { 
						//$( "#row_" + staff_no ).remove();
						$( "tr[user_id=row_" + staff_no + "]" ).remove();
					}
    			}); 
			}
			
			if ( $(this).attr("operation_type")=="remove_user" ) {
				data_array.action = 'remove_user_from_corse';
				data_array.staff_no = staff_no;
				
				$.ajax({
    	    		type: "POST",
	    	    	url: "/external/tp/admin/course_planner_ajax.php",
		    	    data: data_array,
		    		dataType: "json",
		    		timeout: 5000,
    		    	success: function(data) { 
						$( "input[name='delete_staff_no[" + staff_no + "]']").attr("value", "1");
						
						//$( "#row_" + staff_no ).hide();
						$( "tr[user_id=row_" + staff_no + "]" ).hide();
					}
    			}); 
			}
		}
	});			
	
	$("#user_course").change(function () {
		flag_submit_form = true;

		if ( $(this).attr("value") > 0 ) {
			if ( flag_new_changes ) {
				if ( confirm('You selected another course. Your changes will be losted! Do you want change course?') ) {
					flag_submit_form = true;
				} else {
					flag_submit_form = false;
				}
			}
		}

		if ( flag_submit_form == true ) {
			$("#form_user_course").submit();
		}
	});			
	
	$(".tp_sabre").click(function () {
		switch ( $("#i" + $(this).attr('input_code')).attr("value") ) {
				case '0':
					$("#i" + $(this).attr('input_code')).attr("value", "1");
					$("#img" + $(this).attr('input_code')).attr("src", "images/circle_black.png");
					$("#img" + $(this).attr('input_code')).attr("title", "Module is applicable to this user");
				break;
				case '1':
					$("#i" + $(this).attr('input_code')).attr("value", "0");
					$("#img" + $(this).attr('input_code')).attr("src", "images/circle_blank.png");
					$("#img" + $(this).attr('input_code')).attr("title", "Module not applicable to this crew member");
				break;
				
				default: 
					$("#i" + $(this).attr('input_code')).attr("value", "1");
					$("#img" + $(this).attr('input_code')).attr("src", "images/circle_black.png");
					$("#img" + $(this).attr('input_code')).attr("title", "Module is applicable to this user");
		}
			
		flag_new_changes = true;
	});

	$(".tp_sabre_additional").click(function () {
		if ( $( "select[name='ddm_additional[" + $(this).attr('ddm_id') + "]']").attr("value") > 0 ) { // if SELECT has value ONLY THEN you can set module for user
			switch ( $("#i_" + $(this).attr('input_code')).attr("value") ) {
				case '0':
					$("#i_" + $(this).attr('input_code')).attr("value", "2");
					$("#img_" + $(this).attr('input_code')).attr("src", "images/circle_red.png");
					$("#img_" + $(this).attr('input_code')).attr("title", "Module is applicable to this user as additional");
				break;
				case '2':
					$("#i_" + $(this).attr('input_code')).attr("value", "0");
					$("#img_" + $(this).attr('input_code')).attr("src", "images/circle_blank.png");
					$("#img_" + $(this).attr('input_code')).attr("title", "Module not applicable to this crew member");
				break;
				
				default: 
					$("#i_" + $(this).attr('input_code')).attr("value", "2");
					$("#img_" + $(this).attr('input_code')).attr("src", "images/circle_red.png");
					$("#img_" + $(this).attr('input_code')).attr("title", "Module is applicable to this user as additional");
			}
			
			flag_new_changes = true;
		}
	});

	$("select[class=ddm_additional]").change(function () {
		current_ddm_id = $(this).attr("ddm_id");
		current_value = $(this).attr("value");
		prev_value = ddm_current_values_array[current_ddm_id];
		ddm_current_values_array[current_ddm_id] = current_value;
		
		if ( current_value == 0 ) {
			$("input[ddm_id=" + current_ddm_id + "]").each( function() {
				$(this).attr("value", "0");
			});

			$("img[ddm_id=" + current_ddm_id + "]").each( function() {
				$(this).attr("src", "images/circle_blank.png");
				$(this).attr("title", "");
			});
		
		}

		$("select[class=ddm_additional]").each( function() {
			if ( $(this).attr("ddm_id") != current_ddm_id ) {
				$("select[name=ddm_additional[" + $(this).attr("ddm_id") + "]] [value='" + prev_value + "']").attr("disabled", "");
				if ( current_value > 0 ) {
					$("select[name=ddm_additional[" + $(this).attr("ddm_id") + "]] [value='" + current_value + "']").attr("disabled", "disabled");
				}
			}
		});


	});
	
	$("#course_planner_save").submit(function () {
		if ( confirm('Save course?') ) {
			flag_dates_filled = true;
			$(".planned_course_date").each( function() {
				if ( $(this).attr('value')=='' ) {
					flag_dates_filled = false;
				}
			});
		
			if ( flag_dates_filled == false ) {
				alert('Please check if DATE field is set for available course!');
				return false;
			}
		} else {
			return false;		
		}
	});
	
	$("#edit_user_course").change(function () {
		flag_submit_form = false;
		
		if ( flag_new_changes ) {
			if ( confirm('You selected another course. Your changes will be losted! Please save changes before exit.') ) flag_submit_form = true;
		} else {
			flag_submit_form = true;
		}
		
		if ( flag_submit_form ) {
			curr_user_course_value = $("#user_course option:selected").val();

			$('#form_user_course').submit();
		} else {
			$("#user_course [value='" + curr_user_course_value + "']").attr("selected", "selected");
		}
	});			

	$("#edit_user_course_modules").change(function () {
		if ( flag_new_changes ) {
			if ( confirm('You selected another course. Your changes will be losted! Please save changes before exit.') ) $('#form_user_course_modules').submit();
		} else {
			$('#form_user_course_modules').submit();
		}
	});			
	
	$("#form_back_to_planned_courses").submit(function () {
		if ( flag_new_changes ) {
			if ( confirm('Do you really want to terminate course edit process?') ) return true;
		} else {
			return true;
		}	

		return false;
	});			
});
