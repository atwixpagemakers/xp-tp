$(document).ready(function()
{
	$("#sabre_qualif").submit(function () {
		if ( $("#sabre_exp").attr("value")=='' ) {
			alert('Please set an expiry period!');

			return false;
		}
		
		flag_cq_checked = false;
		$("input[class=cq_id]:checked").each( function() {
			flag_cq_checked = true;
		});

		if ( flag_cq_checked == false ) {
			alert('Please select at least 1 Crew Code.');
			return false;			
		}
	});
	
	var $form = $('form[name=delete]');
	var $checks = $form.find(':checkbox');

	$form.submit(function()
	{
		if ( ! $checks.filter(':checked').length )
		{
			alertError('Please select items to delete', 1500);
			return false;
		}
		return confirm('Are you sure you want to delete selected items?');
	});
});
