<?php
$course_info = getPlannedCourseInfo($current_tp_planned_course_id);
$course_trainer = ( !empty($course_info['tp_planned_course_trainer']) ? strip_tags($course_info['tp_planned_course_trainer']) : '<font color="red">' . 'none' . '</font>' );

?>
<div id="header">
<?php
	echo '<form method="get" action="' . CONFP('HTTP_PATH') . '"><input type="submit" value="Home"></form>&nbsp;';
	echo '<form action="/external/tp/index.php" method="get" id="form_user_course" name="form_user_course">';	
	
	echo '<select id="user_course" style="margin: 0 0 0 5px;" name="tp_courses_id">';
	foreach ( $planned_courses as $_tp_courses_id => $course_item_data ) {
		echo '<option ' . ( $current_tp_courses_id == $_tp_courses_id ? 'selected' : '' ) . ' value="' . $_tp_courses_id . '">' . $course_item_data['tp_courses_name'] . '</option>';
	}
	echo '</select>';
	echo '</form>';

	echo '<form action="/external/tp/index.php" method="get" id="form_user_course_modules" name="form_user_course_modules">';	
	echo '<select id="user_course_modules" style="margin: 0 10px 0 5px;" name="tp_planned_course_id">';
	$i = 1;
	foreach ( $planned_courses_dates[$current_tp_courses_id] as $_tp_planned_course_id => $planned_course_data ) {
		echo '<option ' . ( $current_tp_planned_course_id == $_tp_planned_course_id ? 'selected' : '' ) . ' value="' . $_tp_planned_course_id . '">' . $i . ':' . date('d/m/y', $planned_course_data['tp_planned_course_date']) . ( !empty($planned_course_data['tp_planned_course_notes']) ? ' ' . strip_tags($planned_course_data['tp_planned_course_notes']) : '' ) . '</option>';
		
		$i++;
	}
	echo '</select>';
	echo '</form>';

	echo '<b>' . 'Trainer:' . '</b>' . '&nbsp;' . $course_trainer;
?>
</div>
<?php
if ( count($courses_users_table) ) {
		echo '<form name="course_custom_user" id="course_custom_user" method="post" action="' . $_SERVER['PHP_SELF'] . '?action=add_new_user_to_existing_course&tp_planned_course_id=' . $current_tp_planned_course_id . '">' . 
		     '<input type="hidden" name="new_staff_no" id="new_staff_no" value="">' . 
			 '</form>'; 

		echo '<form name="trainer_courses" enctype="multipart/form-data" id="trainer_courses_save" method="post" action="' . $_SERVER['PHP_SELF'] . '?action=course_save_marks&tp_planned_course_id=' . $current_tp_planned_course_id . '" onSubmit="return confirm(\'Save changes?\');">'; 

		$i = 0;
		$CS = count($courses_sabres_table);
		while ( $i < $CS ) {
			echo '<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">';
			echo '<tr>';
			echo '<th align="right">' . '<input type="checkbox" value="1" class="main_checkbox" style="float: left;">' . '<span style="float: right;">' . 'REQUIREMENTS' . '</span>' . '</th>';

			$start = $i;
			$finish = $start + ($CONFP['COUNT_COLUMNS'] - 2);
			for ( $j = $start, $CR = $finish; $j < $CR; $j++ ) {
				echo '<td width="35" align="center">';
				if ( isset($courses_sabres_table[$j]) ) {
					$sabre_item = $courses_sabres_table[$j];
					
					echo '<a class="sabre_title" href="javascript: void(0);" title="' . $sabre_item['sabre_subject'] . '">' . ( isset($sabre_item['color']) && $sabre_item['color']=='red' ? '<font color="red">' : '' ) . $sabre_item['sabre_code'] . ( isset($sabre_item['color']) && $sabre_item['color']=='red' ? '</font>' : '' ) . '</a>' . 
						 ' ' .
					     '<input type="checkbox" value="1" sabre_id="' . $sabre_item['sabre_id'] . '" class="sabre_checkbox">';
				} else {
					echo '&nbsp;&nbsp;&nbsp;';
				}
				echo '</td>';
			}	
			echo '</tr>';

			$flag_use_odd = true;
			foreach ( $courses_users_table as $_no_staff => $user_sabres_data ) {
				$flag_check_custom_user = false;
				if ( is_user_custom($_no_staff) ) {
					$flag_check_custom_user = true;
				}

				echo '<tr ' . ( $flag_use_odd ? 'class="odd"' : '' ) . '>'; 

				$tmp_user_name = userNameByStaffNo($_no_staff);
				if ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']=='' ) {
					$_user_name = '<font color=red><b>User not detected</b></font>' . '<br>' . 'Staff No.' . $_no_staff;
				} elseif ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']!='' ) {
					$_user_name = $tmp_user_name['firstname'];
				} elseif ( $tmp_user_name['surname']!='' && $tmp_user_name['firstname']=='' ) {
					$_user_name = $tmp_user_name['surname'];
				} else {
					$_user_name = $tmp_user_name['surname'] . ', ' . $tmp_user_name['firstname'];
				}
				
				if ( strpos($_user_name, 'User not detected')===false ) {
					$_user_name_out = '<span style="float: right;">' . $_user_name . '</span>' . 
					                  '<br>' . 
									  '<span style="float: right;">' . $_no_staff . '</span>';
				} else {
					$_user_name_out = '<span style="float: right;">' . $_user_name . '</span>';
				}

				echo '<td>' . '<input type="checkbox" value="1" staff_no="' . $_no_staff . '" class="user_checkbox" style="float: left;">' . $_user_name_out . '</td>';

				//foreach ( $courses_sabres_table as $key => $sabre_item ) {
				for ( $j = $start, $CR = $finish; $j < $CR; $j++ ) {
					if ( isset($courses_sabres_table[$j]) ) {
						$sabre_item = $courses_sabres_table[$j];
						
						$planned_item = $user_sabres_data[$sabre_item['sabre_id']];
			
						$sabre_qualif_info = sabreQualifGet($sabre_item['sabre_id'], true);
			
						// 1 - pass (green)
						// 2 - pass-retake (yellow)
						// 3 - fail (red)
						// 4 - complete (green)
						// 5 - not complete (red)
						$image_filename = '';

						if ( $planned_item['sabre_passed_status'] > 0 ) {
							switch ($planned_item['sabre_passed_status']) {
								case $CONFP['TRAINER_MARKS']['pass']:
									$image_filename = 'circle_green.png';
								break;

								case $CONFP['TRAINER_MARKS']['pass-retake']:
									$image_filename = 'circle_yellow.png';
								break;
					
								case $CONFP['TRAINER_MARKS']['fail']:
									$image_filename = 'circle_red.png';
								break;
					
								case $CONFP['TRAINER_MARKS']['complete']:
									$image_filename = 'circle_green.png';
								break;
					
								case $CONFP['TRAINER_MARKS']['not-complete']:
									$image_filename = 'circle_red.png';
								break;

								case $CONFP['TRAINER_MARKS']['not-attended']:
									$image_filename = 'circle_purple.png';
								break;
							}
						} elseif ( $planned_item['sabre_added_status'] > 0 ) {
							$image_filename = 'circle_black.png';
						}
				
						$input_code = $_no_staff . '_' . $sabre_item['sabre_id'];
					
						echo '<td align="center" ' . ($flag_check_custom_user ? 'class="tp_sabre_custom" input_code="' . $input_code . '"' : '' ) . '>';

						if ( $flag_check_custom_user ) {
							echo '<img src="images/circle_blank.png" border="0" id="img' . $input_code . '">';
							echo '<input type="hidden" id="i' . $input_code . '" name="sabre_status[' . $_no_staff . '][' . $sabre_item['sabre_id'] . ']" value="0">';
						}
				
						if ( is_array($planned_item) ) {
							echo '<input type="hidden" id="sabre_passed_status' . $planned_item['tp_planned_courses_to_sabres_id'] . '" name="sabre_passed_status[' . $planned_item['tp_planned_courses_to_sabres_id'] . ']" value="' . $planned_item['sabre_passed_status'] . '">';

							echo '<input type="hidden" id="sabre_none_passed_reason' . $planned_item['tp_planned_courses_to_sabres_id'] . '" name="sabre_none_passed_reason[' . $planned_item['tp_planned_courses_to_sabres_id'] . ']" value="' . ( empty($planned_item['sabre_none_passed_reason']) ? ' ' : $planned_item['sabre_none_passed_reason'] ) . '">';

							echo '<img class="tp_sabre" staff_no="' . $_no_staff . '" sabre_id="' . $sabre_item['sabre_id'] . '" sabre_pf="' . $sabre_qualif_info['sabre_pf'] . '" input_code="' . $planned_item['tp_planned_courses_to_sabres_id'] . '" src="' . $CONFP['TP_SITE_URL'] . 'images/' . $image_filename . '" border="0" id="img' . $planned_item['tp_planned_courses_to_sabres_id'] . '">';

							echo '<div id="popup_module_marks' . $planned_item['tp_planned_courses_to_sabres_id'] . '" class="popup_module_marks" tp_planned_courses_to_sabres_id="' . $planned_item['tp_planned_courses_to_sabres_id'] . '">';
							echo '<div style="position: absolute; top: -10px; left: 209px;">' . '<a href="javascript: void(0);" class="tp_close_popup">' . '<img src="images/icon_close_popup.png" border="0">' . '</a>' . '</div>';
							echo '<ul>' . "\n" . 
								 '<li style="padding: 3px;" class="with_border"><nobr>' . '<b>' . 'Date:' . '</b>' . ' ' .
								 tep_draw_pull_down_menu('passed_date_day[' . $planned_item['tp_planned_courses_to_sabres_id'] . ']', $days_array, ( $planned_item['sabre_passed_status'] > 0 ? date('j', $planned_item['passed_date']) : $default_day ), 'class="ch_mark_ddm" input_code="' . $planned_item['tp_planned_courses_to_sabres_id'] . '"') . 
								 ' ' . 
								 tep_draw_pull_down_menu('passed_date_month[' . $planned_item['tp_planned_courses_to_sabres_id'] . ']', $months_array, ( $planned_item['sabre_passed_status'] > 0 ? date('n', $planned_item['passed_date']) : $default_month ), 'class="ch_mark_ddm" input_code="' . $planned_item['tp_planned_courses_to_sabres_id'] . '"') . 
								 ' ' . 
								 tep_draw_pull_down_menu('passed_date_year[' . $planned_item['tp_planned_courses_to_sabres_id'] . ']', $years_array, ( $planned_item['sabre_passed_status'] > 0 ? date('Y', $planned_item['passed_date']) : $default_year ), 'class="ch_mark_ddm" input_code="' . $planned_item['tp_planned_courses_to_sabres_id'] . '"') . 
						 		 '</nobr>' . 
						 		 '</li>';

							if ( $sabre_qualif_info['sabre_pf'] == 1  ) {
								// 1 - pass (green)
								// 2 - pass-retake (yellow)
								// 3 - fail (red)
								// 6 - Not Attended
								echo '<li class="popup_module_marks_item with_border"><a href="javascript: void(0);" sabre_passed_status="' . $CONFP['TRAINER_MARKS']['pass'] . '" input_code="' . $planned_item['tp_planned_courses_to_sabres_id'] . '"><img border="0" src="' . $CONFP['TP_SITE_URL'] . 'images/circle_green.png">Pass</a>' . '</li>' . "\n" . 
									 '<li class="popup_module_marks_item with_border"><a href="javascript: void(0);" sabre_passed_status="' . $CONFP['TRAINER_MARKS']['pass-retake'] . '" input_code="' . $planned_item['tp_planned_courses_to_sabres_id'] . '"><img border="0" src="' . $CONFP['TP_SITE_URL'] . 'images/circle_yellow.png">Pass (Re-take)</a>' . '</li>' . "\n" . 
									 '<li class="popup_module_marks_item with_border"><a href="javascript: void(0);" sabre_passed_status="' . $CONFP['TRAINER_MARKS']['fail'] . '" input_code="' . $planned_item['tp_planned_courses_to_sabres_id'] . '"><img border="0" src="' . $CONFP['TP_SITE_URL'] . 'images/circle_red.png">Fail</a>' . '</li>' . "\n" . 
									 '<li class="popup_module_marks_item"><a href="javascript: void(0);" sabre_passed_status="' . $CONFP['TRAINER_MARKS']['not-attended'] . '" input_code="' . $planned_item['tp_planned_courses_to_sabres_id'] . '"><img border="0" src="' . $CONFP['TP_SITE_URL'] . 'images/circle_purple.png">Not Attended</a>' . '</li>' . "\n";
							} else {
								// 4 - complete (green)
								// 5 - not complete (red)
								// 6 - Not Attended
								echo '<li class="popup_module_marks_item with_border"><a href="javascript: void(0);" sabre_passed_status="' . $CONFP['TRAINER_MARKS']['complete'] . '" input_code="' . $planned_item['tp_planned_courses_to_sabres_id'] . '"><img border="0" src="' . $CONFP['TP_SITE_URL'] . 'images/circle_green.png">Complete</a>' . '</li>' . "\n" . 
									 '<li class="popup_module_marks_item with_border"><a href="javascript: void(0);" sabre_passed_status="' . $CONFP['TRAINER_MARKS']['not-complete'] . '" input_code="' . $planned_item['tp_planned_courses_to_sabres_id'] . '"><img border="0" src="' . $CONFP['TP_SITE_URL'] . 'images/circle_red.png">Not Complete</a>' . '</li>' . "\n" . 
									 '<li class="popup_module_marks_item"><a href="javascript: void(0);" sabre_passed_status="' . $CONFP['TRAINER_MARKS']['not-attended'] . '" input_code="' . $planned_item['tp_planned_courses_to_sabres_id'] . '"><img border="0" src="' . $CONFP['TP_SITE_URL'] . 'images/circle_purple.png">Not Attended</a>' . '</li>' . "\n";
							}
							echo '</ul>';
							echo '</div>';
						} elseif ( $flag_check_custom_user == false ) {
							echo '&nbsp;';
						}
						echo '</td>';
					} else {
						echo '<td>' . '&nbsp;' . '</td>';
					}
				}
				echo '</tr>';		
		
				if ( $flag_use_odd ) {
					$flag_use_odd = false;
				} else {
					$flag_use_odd = true;
				}
			}	

			$i = $j;
			echo '</table>';				
		}
		

		echo '<table cellpadding="1" cellspacing="0" border="0" align="right">';
		echo '<tr>';	
		echo '<td valign="top"><input type="button" value="Add User" class="img_add_new_user"></td>';
		echo '<td valign="top"><input type="submit" value="Save"></td>';
		echo '</form>';	
		echo '<td valign="top">' . 
			 '<form name="form_complete" method="post" action="' . $_SERVER['PHP_SELF'] . '?action=complete_course&tp_planned_course_id=' . $current_tp_planned_course_id . '" onSubmit="return confirm(\'Are you sure you want to complete this course? Further editing will not be possible.\')">' . 
			 '<input type="submit" value="Complete">' . 
			 '</td>' .
			 '</form>';
		echo '<td valign="top">' . 
			 '<form name="form_print" method="post" action="' . $_SERVER['PHP_SELF'] . '?action=print_course&tp_planned_course_id=' . $current_tp_planned_course_id . '">' . 
			 '<input type="submit" value="Print">' . 
			 '</form>' . 
			 '</td>';
		echo '</tr>';	
		echo '</table>';	

/*
		echo '<table cellpadding="1" cellspacing="0" border="0" align="right">';
		echo '<tr>';	
		echo '<td valign="top"><input type="button" value="Add User" class="img_add_new_user"></td>';
		echo '<td valign="top"><input type="submit" value="Save"></td>';
		echo '</form>';	
		echo '<td valign="top">' . 
			 '<form name="form_complete" method="post" action="' . $_SERVER['PHP_SELF'] . '?action=complete_course&tp_planned_course_id=' . $current_tp_planned_course_id . '" onSubmit="return confirm(\'Are you sure you want to complete this course? Further editing will not be possible.\')">' . 
			 '<input type="submit" value="Complete">' . 
			 '</form>' . 
			 '</td>';
		echo '<td valign="top">' . 
			 '<form name="form_print" method="post" action="' . $_SERVER['PHP_SELF'] . '?action=print_course&tp_planned_course_id=' . $current_tp_planned_course_id . '">' . 
			 '<input type="submit" value="Print">' . 
			 '</form>' . 
			 '</td>';
		echo '</tr>';	
		echo '</table>';	
*/
}
		echo '<div style="height:100px;">&nbsp;</div>';
?>
