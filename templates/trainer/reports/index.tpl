<?php
$course_info = getPlannedCourseInfo($current_tp_planned_course_id);
$course_notes = strip_tags($course_info['tp_planned_course_notes']);
$course_trainer = ( !empty($course_info['tp_planned_course_trainer']) ? strip_tags($course_info['tp_planned_course_trainer']) : '<font color="red">' . 'none' . '</font>' );
$current_planned_course_date = $course_info['tp_planned_course_date'];

foreach ( $planned_courses as $_tp_courses_id => $course_item_data ) {
	if ( $current_tp_courses_id == $_tp_courses_id ) {
		$current_course_name = $course_item_data['tp_courses_name'];
		
		break;
	}
}

foreach ( $planned_courses_dates[$current_tp_courses_id] as $_tp_planned_course_id => $planned_course_data ) {
	if ( $current_tp_planned_course_id == $_tp_planned_course_id ) {
		$current_planned_course_number = $i;
		
		break;
	}
}


if ( count($courses_users_table) ) {

	$mpdf = new mPDF('win-1252', 'A4', 0, '', 15, 15, 16, 16, 9, 5);
	$mpdf->SetTitle($current_course_name . ' ' . $current_planned_course_number . ':' . date('d/m/y', $current_planned_course_date) );
	$mpdf->WriteHTML(utf8_encode(file_get_contents(ROOT_PROJ . 'css/style.css')), 1);   
	$mpdf->SetHTMLFooter($CONFP['PDF_FOOTER_TEXT']);

	$course_notices = '<table border="0" cellpadding="2" cellspacing="2" width="100%" style="font-size:14px;">' . 
			 '<tr>' . 
			 '<td width="120">Course Name:</td>' .
			 '<td><b>' . $current_course_name . '</b></td>' .
			 '<td rowspan="4" align="right">' . '<img src="' . $CONFP['TP_SITE_URL'] . 'images/thomas_cook.jpg">' . '</td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td>Course Notes:</td>' .
			 '<td><b>' . $course_notes . '</b></td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td>Trainer:</td>' .
			 '<td><b>' . $course_trainer . '</b></td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td>Course Date:</td>' .
			 '<td><b>' . date('d/m/y', $current_planned_course_date) . '</b></td>' .
			 '</tr>' . 
			 '</table><br>';

	$i = 0;
	$CS = count($courses_sabres_table);
	while ( $i < $CS ) {
			$content = '';

			$content .= $course_notices;

			$content .= '<div style="border: 5px solid #2263AD;">';
			$content .= '<table style="background: #2263AD;" border="0" cellpadding="0" cellspacing="1" width="100%">';

			$content_modules = '';
			$content_modules .= '<tr>';
			$content_modules .= '<td width="200" align="right" style="color: #ffffff; font-weight: bold; padding: 5px;">' . $CONFP['TEXT_CONSTANTS']['TABLE_HEADER_REQUIREMENTS'] . '</td>';

			$start = $i;
			$finish = $start + ($CONFP['COUNT_COLUMNS'] - 2);
			for ( $j = $start, $CR = $finish; $j < $CR; $j++ ) {
				$content_modules .= '<td align="center" style="background: #ffffff; padding: 5px;">';

				if ( isset($courses_sabres_table[$j]) ) {
					$sabre_item = $courses_sabres_table[$j];
					
					$content_modules .= ( isset($sabre_item['color']) && $sabre_item['color']=='red' ? '<font color="red">' : '' ) . $sabre_item['sabre_code'] . ( isset($sabre_item['color']) && $sabre_item['color']=='red' ? '</font>' : '' );
				} else {
					$content_modules .= '&nbsp;&nbsp;&nbsp;';
				}
				$content_modules .= '</td>';
			}	
			$content_modules .= '</tr>';

			$content .= $content_modules; 

			$flag_use_odd = true;		
			$flag_show_black_description = false;
			$flag_show_green_description = false;
			$flag_show_yellow_description = false;
			$flag_show_red_description = false;
			$flag_show_purple_description = false;
			$flag_show_m_explanation = false;

			$users_count = 1;
			foreach ( $courses_users_table as $_no_staff => $user_sabres_data ) {
				if ( $users_count > 12 ) {
					$users_count = 1;
				
					$content .= '</table>' . "\n";
					$content .= '</div>' . "\n";

					$mpdf->AddPage('L');
					$mpdf->WriteHTML(utf8_encode($content));		

					$content = '';
	
					$content .= $course_notices;

					$content .= '<div style="border: 5px solid #2263AD;">';
					$content .= '<table style="background: #2263AD;" border="0" cellpadding="0" cellspacing="1" width="100%">';
					$content .= $content_modules; 
				}

				$content .= '<tr ' . ( $flag_use_odd ? 'style="background-color: #D4E3F2;"' : 'style="background-color: #ffffff;"' ) . '>';		

				$tmp_user_name = userNameByStaffNo($_no_staff);

				if ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']=='' ) {
					$_user_name = '<font color=red><b>User not detected</b></font>' . '<br>' . 'Staff No.' . $_no_staff;
				} elseif ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']!='' ) {
					$_user_name = $tmp_user_name['firstname'];
				} elseif ( $tmp_user_name['surname']!='' && $tmp_user_name['firstname']=='' ) {
					$_user_name = $tmp_user_name['surname'];
				} else {
					$_user_name = $tmp_user_name['surname'] . ', ' . $tmp_user_name['firstname'];
				}

				if ( $courses_users_table[$_no_staff]['manually_added_by_trainer']==1 ) {
					$_user_name .= ' (M)'; 
					
					$flag_show_m_explanation = true;
				}

				if ( strpos($_user_name, 'User not detected')===false ) {
					$_user_name .= '<br>' . $_no_staff;
				}

				$content .= '<td align="right" style="padding: 5px;">' . $_user_name . '</td>';

				for ( $j = $start, $CR = $finish; $j < $CR; $j++ ) {
					if ( isset($courses_sabres_table[$j]) ) {
						$sabre_item = $courses_sabres_table[$j];
						
						$planned_item = $user_sabres_data[$sabre_item['sabre_id']];
						$sabre_qualif_info = sabreQualifGet($sabre_item['sabre_id'], true);
			
						// 1 - pass (green)
						// 2 - pass-retake (yellow)
						// 3 - fail (red)
						// 4 - complete (green)
						// 5 - not complete (red)
						// 6 - not-attended (purple)
						$image_filename = '';
						if ( $planned_item['sabre_passed_status'] > 0 ) {
							switch ($planned_item['sabre_passed_status']) {
								case 1:
									$image_filename = 'circle_green.png';
									$flag_show_green_description = true;
								break;

								case 2:
									$image_filename = 'circle_yellow.png';
									$flag_show_yellow_description = true;
								break;
					
								case 3:
									$image_filename = 'circle_red.png';
									$flag_show_red_description = true;
								break;
					
								case 4:
									$image_filename = 'circle_green.png';
									$flag_show_green_description = true;
								break;
					
								case 5:
									$image_filename = 'circle_red.png';
									$flag_show_red_description = true;
								break;

								case 6:
									$image_filename = 'circle_purple.png';
									$flag_show_purple_description = true;
								break;
							}
						} elseif ( $planned_item['sabre_added_status'] > 0 ) {
							$image_filename = 'circle_black.png';
							$flag_show_black_description = true;
						}

						$content .= '<td align="center" style="padding: 5px;">';
						if ( !empty($image_filename) ) {
							$content .= '<img src="' . $CONFP['TP_SITE_URL'] . 'images/' . $image_filename . '" border="0">';
						} else {
							$content .= '&nbsp;';
						}
						$content .= '</td>';
					} else {
						$content .= '<td>' . '&nbsp;' . '</td>';
					}
				}
				$content .= '</tr>';		
		
				if ( $flag_use_odd ) {
					$flag_use_odd = false;
				} else {
					$flag_use_odd = true;
				}
				
				$users_count++;				
			}	

			$i = $j;
			
			$content .= '</table>';
			$content .= '</div>';

			$mpdf->AddPage('L');
			$mpdf->WriteHTML(utf8_encode($content));		
	}

	if ( $flag_show_m_explanation || $flag_show_black_description || $flag_show_green_description || $flag_show_red_description || $flag_show_yellow_description || $flag_show_purple_description ) {
		$table_notices = '<br>' . 
			 '<table border="0" cellpadding="2" cellspacing="2" align="left">' .
			 '<tr><td colspan="2"><b>Description of the table:</b></td></tr>' .
			 '<tr>' .
			 ( $flag_show_black_description ? '<td align="left">' . '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_black.png" border="0">' . '</td>' . '<td>' . '- module is applicable to user' . '</td>' : '' ) .
		     ( $flag_show_green_description ? '<td align="left">' . '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_green.png" border="0">' . '</td>' . '<td>' . '- module is passed (completed)' . '</td>' : '' ) .	
		     ( $flag_show_yellow_description ? '<td align="left">' . '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_yellow.png" border="0">' . '</td>' . '<td>' . '- module is pass-retaked' . '</td>' : '' ) .
		     ( $flag_show_red_description ? '<td align="left">' . '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_red.png" border="0">' . '</td>' . '<td>' . '- module is fell (not completed)' . '</td>' : '' ) .
		     ( $flag_show_purple_description ? '<td align="left">' . '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_purple.png" border="0">' . '</td>' . '<td>' . '- module is not attended' . '</td>' : '' ) .
	 		 '</tr>' .
			 '</table>' . 
 	 		 ( $flag_show_m_explanation == true ? '<p>(M) - Manually added by Trainer</p>' : '' );

	 		 $mpdf->WriteHTML(utf8_encode($table_notices));
	}

	$sPDFContent = $mpdf->Output('', 'S');
	ns_http::outputFile($sPDFContent, str_replace(' ', '_', $current_course_name) . '_' . $current_planned_course_number . '_' . date('d.m.y', $current_planned_course_date) . '.pdf', array('from_content' => true));
}
?>
