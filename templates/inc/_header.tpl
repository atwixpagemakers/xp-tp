<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?=$pageTitle?></title>
<base href="<?=$basePath?>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/style.css?rev=<?=CONFP('REV')?>" media="all">
<link rel="stylesheet" type="text/css" href="css/style_print.css" media="print">
<link rel="stylesheet" type="text/css" href="<?=$CONFP['REL_PATH_XPLORER']?>js/facebox/facebox.css">
<script type="text/javascript" src="js/inc/config.php"></script>
<script type="text/javascript" src="js/inc/jquery.js"></script>
<script type="text/javascript" src="<?=$CONFP['REL_PATH_XPLORER']?>js/facebox/facebox.js"></script>
<script type="text/javascript" src="js/inc/common.js"></script>
<script type="text/javascript" src="js/inc/project.js?rev=<?=CONFP('REV')?>"></script>
<?php
if ( (int)$C['pdf_status'] > 0 ) {
?>
<script type="text/javascript">
var pdf_global_time = <?php echo $C['pdf_status']; ?>;
</script>
<?php	
}
?>
<? if ( $aErrors || $aOK || $aInfoMessages ) { ?>
<script type="text/javascript">
<? if ( $aInfoMessages ) { ?>info_messages = '<?=jh(implode('<br>', $aInfoMessages))?>';<? } ?>
<? if ( $aErrors ) { ?>errors = '<?=jh(implode('<br>', $aErrors))?>';<? } ?>
<? if ( $aOK ) { ?>messages = '<?=jh(implode('<br>', $aOK))?>';<? } ?>
</script>
<? } 

if ( isset($default_day) && isset($default_month) && isset($default_year) ) {
	echo '<script type="text/javascript">' . "\n";
	if ( isset($default_day) ) {
		echo 'var default_day = ' . $default_day . ';' . "\n";
	}

	if ( isset($default_month) ) {
		echo 'var default_month = ' . $default_month . ';' . "\n";
	}

	if ( isset($default_year) ) {
		echo 'var default_year = ' . $default_year . ';' . "\n";
	}
	echo '</script>' . "\n";
}
?>
<?=@$headAdditional?>
</head>
<body>
<div id="main"<?=@$main_div_classes ? ' class="' . $main_div_classes . '"' : ''?>>
