<div id="header">
<?php
	$flag_show_save_pdf_button = true;

	// *****************************************************************************
	//                            BOF header menu section
	// *****************************************************************************
	echo ddMenuAdmin('reports');

	$reports_array = array();
	$reports_array[] = array( 'id' => '', 'text' => 'Select Report...');	
	$reports_array[] = array( 'id' => 'report_exp', 'text' => 'Expiry Report');
	$reports_array[] = array( 'id' => 'report_mod', 'text' => 'Module Report');
	$reports_array[] = array( 'id' => 'report_user', 'text' => 'User Report');
	$reports_array[] = array( 'id' => 'report_course', 'text' => 'Course Report');
	$reports_array[] = array( 'id' => 'report_fail', 'text' => 'Fail Report');	
	
	echo  '<form name="report_form" id="tp_report_form" method="get" action="' . $_SERVER['PHP_SELF'] . '">' . 
	      //'<select name="report_type" id="tp_report_type">';
	      '<select name="report_type" onchange="this.form.submit()">';
	foreach ( $reports_array as $key => $report_data ) {
		echo '<option value="' . $report_data['id'] . '"' . ( @$selected_report_type == $report_data['id'] ? ' selected' : '') . '>' . $report_data['text'] . '</option>';
	}
	echo '</select>' . '</form>';
	// *****************************************************************************
	//                            EOF header menu section
	// *****************************************************************************
?>
</div>
<?php
// *****************************************************************************
//                            BOF SEARCH section for EXPIRY report and MODULE report and FAIL report
// *****************************************************************************
if ( $selected_report_type == 'report_exp' || $selected_report_type == 'report_mod' || $selected_report_type == 'report_fail' ) {
?>
<form name="report_expiry" id="tp_report_expiry" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
<input type="hidden" name="action" value="confirm_report">
<input type="hidden" name="report_type" value="<?php echo $selected_report_type; ?>">
<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
	<tr>
		<td style="padding:3px;" width="100"><b><?php echo 'From:'; ?></b></td>
		<td style="padding:3px;"><?php 
			echo '<select name="month_from" id="tp_month_from">';
			foreach ( $months_array as $key => $month_data ) {
				echo '<option value="' . $month_data['id'] . '"' . ( $month_from == $month_data['id'] ? ' selected' : '') . '>' . $month_data['text'] . '</option>';
			}
			echo '</select>';
			echo '&nbsp;';
			echo '<select name="year_from" id="tp_year_from">';
			foreach ( $years_array as $key => $year_data ) {
				echo '<option value="' . $year_data['id'] . '"' . ( $year_from == $year_data['id'] ? ' selected' : '') . '>' . $year_data['text'] . '</option>';
			}
			echo '</select>';
		?></td>
	</tr>
	<tr>
		<td style="padding:3px;"><b><?php echo 'To:'; ?></b></td>
		<td style="padding:3px;"><?php 
			echo '<select name="month_to" id="tp_month_to">';
			foreach ( $months_array as $key => $month_data ) {
				echo '<option value="' . $month_data['id'] . '"' . ( $month_to == $month_data['id'] ? ' selected' : '') . '>' . $month_data['text'] . '</option>';
			}
			echo '</select>';
			echo '&nbsp;';
			echo '<select name="year_to" id="tp_year_to">';
			foreach ( $years_array as $key => $year_data ) {
				echo '<option value="' . $year_data['id'] . '"' . ( $year_to == $year_data['id'] ? ' selected' : '') . '>' . $year_data['text'] . '</option>';
			}
			echo '</select>';
		?></td>
	</tr>
	<tr class="block_base" <?php echo ( $selected_report_type == 'report_fail' && (int)$_GET['include_deleted_users']==1 ? 'style="display: none;"' : '' ); ?>>
		<td style="padding:3px;"><b><?php echo 'Base:'; ?></b></td>
		<td style="padding:3px;"><?php 
			echo '<select name="id_base">';
			foreach ( $bases_array as $key => $base_data ) {
				echo '<option value="' . $base_data['id'] . '"' . ( $id_base == $base_data['id'] ? ' selected' : '') . '>' . $base_data['text'] . '</option>';
			}
			echo '</select>';
		?></td>
	</tr>
	<?php
	if ( $selected_report_type == 'report_fail' ) {
	?>
	<tr>
		<td style="padding:3px;"><b><?php echo 'Failure:'; ?></b></td>
		<td style="padding:3px;"><?php 
			$id_fail = (int)$_GET['id_fail'];
			
			echo '<select name="id_fail">';
			foreach ( $fail_data_array as $key => $fail_data ) {
				echo '<option value="' . $fail_data['id'] . '"' . ( $id_fail == $fail_data['id'] ? ' selected' : '') . '>' . $fail_data['text'] . '</option>';
			}
			echo '</select>';
		?></td>
	</tr>
	<tr>
		<td style="padding:3px;"><b><?php echo 'Deleted Users:'; ?></b></td>
		<td style="padding:3px;"><?php 
			echo '<input class="include_deleted_users" type="checkbox" value="1" ' . ( (int)$_GET['include_deleted_users'] == 1 ? 'checked' : '' ) . ' name="include_deleted_users">';
		?></td>
	</tr>
	<?php
	}
	?>
	<tr>
		<td style="padding:3px;"><?php echo '&nbsp;'; ?></td>
		<td style="padding:3px;"><?php echo '<input type="submit" name="Go" value="Go">'; ?></td>
	</tr>
</table>
</form>
<?php
}
// *****************************************************************************
//                            EOF SEARCH section for EXPIRY report and MODULE report
// *****************************************************************************

// *****************************************************************************
//                            BOF SEARCH section for USER report
// *****************************************************************************
if ( $selected_report_type == 'report_user' ) {
?>
<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
<form name="report_user" id="tp_report_user" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
<input type="hidden" name="action" value="confirm_report">
<input type="hidden" name="report_type" value="<?php echo $selected_report_type; ?>">
<!--
<input type="hidden" id="report_user_tp_planned_course_id" name="tp_planned_course_id" value="<?php echo $current_tp_planned_course_id; ?>">
-->
	<tr>
		<td style="padding:3px;" width="100" align="right"><b><?php echo 'USER:'; ?></b></td>
		<td style="padding:3px;"><?php echo '<input type="text" id="report_user_no_staff" name="no_staff" value="' . $selected_no_staff . '" size="25">' . '&nbsp;' . '<input type="submit" name="Search" value="Search">'; ?></td>
	</tr>
	<?php
	if ( $reports_data ) {
	?>	
	<tr>
		<td style="padding:3px;" align="right"><b><?php echo 'COURSE:'; ?></b></td>
		<td style="padding:3px;"><?php 
			echo '<select id="tp_report_user_course">'; 
			if ( count($reports_data) > 1 ) {
				echo '<option value="0"' . ( 0 == $current_tp_planned_course_id ? ' selected' : '' ) . '>' . 'All Courses' . '</option>';
			}
			foreach ( $reports_data as $_tp_planned_course_id => $course_info ) {
				if ( $_tp_planned_course_id > 0 ) {
					echo '<option value="' . $course_info['tp_planned_course_id'] . '"' . ( $course_info['tp_planned_course_id'] == $current_tp_planned_course_id ? ' selected' : '' ) . '>' . $course_info['tp_planned_course_date'] . ' ' . $course_info['tp_courses_name'] . ' - ' . $course_info['tp_planned_course_notes'] . '</option>';
				}
			}
			echo '</select>'; 
		?></td>
	</tr>
	<?php	
	}
	?>
</table>
</form>
<?php
	if ( count($tp_report_user_users) > 1 ) {
		$flag_show_save_pdf_button = false;
	?>
	<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
	<tr>
		<th colspan="2" style="text-align: center; color:#FBC60E;"><b><?php echo 'SELECT USER'; ?></b></th>
	</tr>
	<tr>
		<th style="color:#FBC60E;" width="150"><b><?php echo 'STAFF NO'; ?></b></th>
		<th style="color:#FBC60E;"><b><?php echo 'NAME'; ?></b></th>
	</tr>
	<?php
		$flag_use_odd = false;
		foreach ( $tp_report_user_users as $key => $tp_report_user_item ) {
		?>
		<tr <?php echo ( $flag_use_odd ? 'class="odd"' : '' ); ?>>
			<td><?php echo $tp_report_user_item['no_staff']; ?></td>
			<td><?php echo '<a href="' . $_SERVER['PHP_SELF'] . '?action=confirm_report&report_type=' . $selected_report_type . '&no_staff=' . $tp_report_user_item['no_staff'] . '&get_user_info">' . $tp_report_user_item['surname'] . ' ' . $tp_report_user_item['firstname'] . '</a>'; ?></td>
		</tr>
		<?php	
			if ( $flag_use_odd ) {
				$flag_use_odd = false;
			} else {
				$flag_use_odd = true;
			}
		}
	?>
	</table>
	<?php
	}
}

// *****************************************************************************
//                            EOF SEARCH section for USER report
// *****************************************************************************


// *****************************************************************************
//                            BOF SEARCH section for COURSE report
// *****************************************************************************
if ( $selected_report_type == 'report_course' ) {
?>
<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
<form name="report_user" id="tp_report_course_form" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
<input type="hidden" name="action" value="confirm_report">
<input type="hidden" name="report_type" value="<?php echo $selected_report_type; ?>">
	<tr>
		<td style="padding:3px;" width="100" align="right"><b><?php echo 'COURSE:'; ?></b></td>
		<td style="padding:3px;"><?php 
		
			echo '<select name="tp_courses_id" id="tp_report_course_id">'; 
			foreach ( $completed_courses_array as $key => $course_info ) {
				echo '<option value="' . $course_info['id'] . '"' . ( $course_info['id'] == $current_tp_courses_id ? ' selected' : '' ) . '>' . $course_info['text'] . '</option>';
			}
			echo '</select>'; 
		?></td>
	</tr>
	<tr>
		<td style="padding:3px;" align="right"><b><?php echo 'From:'; ?></b></td>
		<td style="padding:3px;"><?php 
			echo '<select name="month_from" id="tp_month_from">';
			foreach ( $months_array as $key => $month_data ) {
				echo '<option value="' . $month_data['id'] . '"' . ( $month_from == $month_data['id'] ? ' selected' : '') . '>' . $month_data['text'] . '</option>';
			}
			echo '</select>';
			echo '&nbsp;';
			echo '<select name="year_from" id="tp_year_from">';
			foreach ( $years_array as $key => $year_data ) {
				echo '<option value="' . $year_data['id'] . '"' . ( $year_from == $year_data['id'] ? ' selected' : '') . '>' . $year_data['text'] . '</option>';
			}
			echo '</select>';
		?></td>
	</tr>
	<tr>
		<td style="padding:3px;" align="right"><b><?php echo 'To:'; ?></b></td>
		<td style="padding:3px;"><?php 
			echo '<select name="month_to" id="tp_month_to">';
			foreach ( $months_array as $key => $month_data ) {
				echo '<option value="' . $month_data['id'] . '"' . ( $month_to == $month_data['id'] ? ' selected' : '') . '>' . $month_data['text'] . '</option>';
			}
			echo '</select>';
			echo '&nbsp;';
			echo '<select name="year_to" id="tp_year_to">';
			foreach ( $years_array as $key => $year_data ) {
				echo '<option value="' . $year_data['id'] . '"' . ( $year_to == $year_data['id'] ? ' selected' : '') . '>' . $year_data['text'] . '</option>';
			}
			echo '</select>';
		?></td>
	</tr>
	<tr>
		<td style="padding:3px;" align="right"><b><?php echo 'Deleted Users:'; ?></b></td>
		<td style="padding:3px;"><?php 
			echo '<input type="checkbox" value="1" ' . ( (int)$_GET['include_deleted_users'] == 1 ? 'checked' : '' ) . ' name="include_deleted_users">';
		?></td>
	</tr>
	<tr>
		<td style="padding:3px;"><?php echo '&nbsp;'; ?></td>
		<td style="padding:3px;"><?php echo '<input type="submit" name="Go" value="Go">'; ?></td>
	</tr>
</table>
</form>
<?php
}
// *****************************************************************************
//                            EOF SEARCH section for COURSE report
// *****************************************************************************

?>





<?php
if ( $_GET['action']=='confirm_report' && !empty($selected_report_type) && !$flag_error ) {
	$url_string = '';

	// *****************************************************************************
	//                            BOF OUTPUT section for EXPIRY report 
	// *****************************************************************************
	if ( $selected_report_type=='report_exp' ) {
		$form_generate_pdf_report_id = 'form_pdf_report_exp';
		
		$url_string = $_SERVER['PHP_SELF'] . '?action=confirm_report' . 
	              '&report_type=' . $selected_report_type . 
				  '&month_from=' . $month_from . 
				  '&year_from=' . $year_from .
				  '&month_to=' . $month_to . 
				  '&year_to=' . $year_to .
				  '&id_base=' . $id_base . 
				  '&order_type=' . ( $_GET['order_type']=='a' ? 'd' : 'a' );
		?>
		<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
		<tr>
			<th colspan="6" style="text-align: center; color:#FBC60E;"><b><?php echo 'EXPIRY REPORT'; ?></b></th>
		</tr>
		<tr>
			<th width="200"><a href="<?php echo $url_string . '&sort_field=user_name'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='user_name' ? 'text-decoration: underline;' : '' ); ?>"><?php echo 'NAME'; ?></a></th>
			<th width="50"><a href="<?php echo $url_string . '&sort_field=base_shortname'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='base_shortname' ? 'text-decoration: underline;' : '' ); ?>"><?php echo 'BASE'; ?></a></th>
			<th width="100"><a href="<?php echo $url_string . '&sort_field=sabre_code'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='sabre_code' ? 'text-decoration: underline;' : '' ); ?>"><?php echo 'CODE'; ?></a></th>
			<th><a href="<?php echo $url_string . '&sort_field=sabre_subject'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='sabre_subject' ? 'text-decoration: underline;' : '' ); ?>"><?php echo 'MODULE'; ?></a></th>
			<th width="120"><a href="<?php echo $url_string . '&sort_field=date'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='date' ? 'text-decoration: underline;' : '' ); ?>"><?php echo 'COMPLETE DATE'; ?></a></th>
			<th width="120"><a href="<?php echo $url_string . '&sort_field=exp_date'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='exp_date' ? 'text-decoration: underline;' : '' ); ?>"><?php echo 'EXPIRY DATE'; ?></a></th>
		</tr>
		<?php
		$flag_use_odd = false;
		for ($i = 0, $N = count($reports_data); $i < $N; $i++) {
		?>
		<tr <?php echo ( $flag_use_odd ? 'class="odd"' : '' ); ?>>
			<td><?php echo $reports_data[$i]['user_name']; ?></td>
			<td><?php echo $reports_data[$i]['base_shortname']; ?></td>
			<td><?php echo $reports_data[$i]['sabre_code']; ?></td>
			<td><?php echo $reports_data[$i]['sabre_subject']; ?></td>
			<td><?php echo $reports_data[$i]['date']; ?></td>
			<td><?php echo $reports_data[$i]['exp_date']; ?></td>
		</tr>
		<?php
			if ( $flag_use_odd ) {
				$flag_use_odd = false;
			} else {
				$flag_use_odd = true;
			}
		}
		?>
		</table>
		<?php
	}
	// *****************************************************************************
	//                            EOF OUTPUT section for EXPIRY report 
	// *****************************************************************************


	// *****************************************************************************
	//                            BOF OUTPUT section for MODULE report 
	// *****************************************************************************
	if ( $selected_report_type=='report_mod' ) {
		$form_generate_pdf_report_id = 'form_pdf_report_mod';
		
		$url_string = $_SERVER['PHP_SELF'] . '?action=confirm_report' . 
	              '&report_type=' . $selected_report_type . 
				  '&month_from=' . $month_from . 
				  '&year_from=' . $year_from .
				  '&month_to=' . $month_to . 
				  '&year_to=' . $year_to .
				  '&id_base=' . $id_base . 
				  '&order_type=' . ( $_GET['order_type']=='a' ? 'd' : 'a' );
?>
	<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
		<tr>
			<th colspan="6" style="text-align: center; color:#FBC60E;"><b><?php echo 'MODULE REPORT'; ?></b></th>
		</tr>
		<tr>
			<th width="50"><a href="<?php echo $url_string . '&sort_field=sabre_code'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='sabre_code' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'CODE'; 
			?></a></th>
			<th><a href="<?php echo $url_string . '&sort_field=sabre_subject'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='sabre_subject' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'NAME'; 
			?></a></th>
			<th width="50" style="text-align: center;"><a href="<?php echo $url_string . '&sort_field=base_shortname'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='base_shortname' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'BASE'; 
			?></a></th>

			<th width="80" style="text-align: center;"><a href="<?php echo $url_string . '&sort_field=total_count_modules'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='total_count_modules' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'TOTAL'; 
			?></a></th>
			<th width="80" style="text-align: center;"><a href="<?php echo $url_string . '&sort_field=total_count_passed'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='total_count_passed' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'PASSED'; 
			?></a></th>
			<th width="100" style="text-align: center;"><a href="<?php echo $url_string . '&sort_field=percentage'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='percentage' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'PASS %'; 
			?></a></th>
		</tr>
		<?php
		$flag_use_odd = false;
		for ($i = 0, $N = count($reports_data); $i < $N; $i++) {
		?>
		<tr <?php echo ( $flag_use_odd ? 'class="odd"' : '' ); ?>>
			<td><?php echo $reports_data[$i]['sabre_code']; ?></td>
			<td><?php echo $reports_data[$i]['sabre_subject']; ?></td>
			<td align="center"><?php echo $reports_data[$i]['base_shortname']; ?></td>
			<td align="center"><?php echo $reports_data[$i]['total_count_modules']; ?></td>
			<td align="center"><?php echo $reports_data[$i]['total_count_passed']; ?></td>			
			<td align="center"><?php echo $reports_data[$i]['percentage']; ?></td>
		</tr>
		<?php

			if ( $flag_use_odd ) {
				$flag_use_odd = false;
			} else {
				$flag_use_odd = true;
			}
		}
		?>
	</table>
<?php
	}  
	// *****************************************************************************
	//                            EOF OUTPUT section for MODULE report 
	// *****************************************************************************

	// *****************************************************************************
	//                            BOF OUTPUT section for USER report 
	// *****************************************************************************
	if ( $selected_report_type=='report_user' ) {
		$form_generate_pdf_report_id = 'form_pdf_report_user';

		$url_string = $_SERVER['PHP_SELF'] . '?action=confirm_report' . 
	              '&report_type=' . $selected_report_type . 
				  '&no_staff=' . $selected_no_staff . 
				  '&get_user_info' . 
				  '&order_type=' . ( $_GET['order_type']=='a' ? 'd' : 'a' );
	
		$tmp_user_info = userNameByStaffNo($selected_no_staff);

		foreach ( $reports_data as $_tp_planned_course_id => $course_info ) {
		?>
		<table class="regular thickbox botmar4 report_user" border="0" cellpadding="0" cellspacing="1" width="100%" id="<?php echo 'tp_planned_course_id_' . $_tp_planned_course_id; ?>" <?php echo ( $_tp_planned_course_id == $current_tp_planned_course_id /*|| $current_tp_planned_course_id > 0*/ ? '' : 'style="display:none;"' );?>>
		<?php
		if ( $_tp_planned_course_id > 0 ) {
		?>
		<tr>
			<th colspan="6" style="text-align: center; color:#FBC60E;"><b><?php echo 'COURSE: ' . $course_info['tp_planned_course_date'] . ' ' . $course_info['tp_courses_name'] . ' - ' . $course_info['tp_planned_course_notes']; ?></b></th>
		</tr>
		<tr>
			<th colspan="6" style="text-align: center; color:#FBC60E;"><b><?php echo 'USER REPORT: ' . $tmp_user_info['firstname'] . ' ' . $tmp_user_info['surname']; ?></b></th>
		</tr>
		<?php
		} else {
		?>
		<tr>
			<th colspan="6" style="text-align: center; color:#FBC60E;"><b><?php echo 'ALL COURSES'; ?></b></th>
		</tr>
		<?php
		}
		?>
		<tr>
			<th width="50"><a class="report_mod_sort_url" href="<?php echo $url_string . '&sort_field=sabre_code'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='sabre_code' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'CODE'; 
			?></a></th>
			<th><a class="report_mod_sort_url" href="<?php echo $url_string . '&sort_field=sabre_subject'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='sabre_subject' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'NAME'; 
			?></a></th>
			<th width="150" style="text-align: center;"><a class="report_mod_sort_url" href="<?php echo $url_string . '&sort_field=passed_by_trainer'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='passed_by_trainer' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'TRAINER'; 
			?></a></th>

			<th width="100" style="text-align: center;"><a class="report_mod_sort_url" href="<?php echo $url_string . '&sort_field=sabre_passed_status'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='sabre_passed_status' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'STATUS'; 
			?></a></th>
			<th width="120" style="text-align: center;"><a class="report_mod_sort_url" href="<?php echo $url_string . '&sort_field=passed_date'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='passed_date' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'COMPLETE DATE'; 
			?></a></th>
			<th width="120" style="text-align: center;"><a class="report_mod_sort_url" href="<?php echo $url_string . '&sort_field=exp_date'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='exp_date' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'EXPIRY DATE'; 
			?></a></th>
		</tr>
		<?php
			$flag_use_odd = false;
			for ($i = 0, $N = count($course_info['course_info']); $i < $N; $i++) {
			?>
			<tr <?php echo ( $flag_use_odd ? 'class="odd"' : '' ); ?>>
				<td><?php echo $course_info['course_info'][$i]['sabre_code']; ?></td>
				<td><?php echo $course_info['course_info'][$i]['sabre_subject']; ?></td>
				<td align="center"><?php 
				//echo $course_info['tp_planned_course_trainer']; 
				echo $course_info['course_info'][$i]['sabre_passed_by_trainer'];
				?></td>
				<td align="center" <?php echo ( $course_info['course_info'][$i]['sabre_none_passed_reason'] != '' ? 'hint="' . $course_info['course_info'][$i]['sabre_none_passed_reason'] . '"' : '' )?>><?php echo ( $course_info['course_info'][$i]['sabre_none_passed_reason'] != '' ? '<u>' : '' ) . $course_info['course_info'][$i]['sabre_passed_status'] . ( $course_info['course_info'][$i]['sabre_none_passed_reason'] != '' ? '</u>' : '' ); ?></td>
				<td align="center"><?php echo $course_info['course_info'][$i]['date']; ?></td>			
				<td align="center"><?php echo $course_info['course_info'][$i]['exp_date']; ?></td>			
			</tr>
			<?php

				if ( $flag_use_odd ) {
					$flag_use_odd = false;
				} else {
					$flag_use_odd = true;
				}
			}
		?>
		</table>
		<?php
		}
		$url_string .= '&sort_field=' . $selected_sort_field . '&order_type=' . $_GET['order_type'];
	}
	// *****************************************************************************
	//                            EOF OUTPUT section for USER report 
	// *****************************************************************************


	// *****************************************************************************
	//                            BOF OUTPUT section for FAIL report 
	// *****************************************************************************
	if ( $selected_report_type=='report_fail' ) {
		$form_generate_pdf_report_id = 'form_pdf_report_fail';

		$url_string = $_SERVER['PHP_SELF'] . '?action=confirm_report' . 
	              '&report_type=' . $selected_report_type . 
				  '&month_from=' . $month_from . 
				  '&year_from=' . $year_from .
				  '&month_to=' . $month_to . 
				  '&year_to=' . $year_to .
				  '&id_base=' . $id_base . 
				  '&id_fail=' . $id_fail . 
				  ( (int)$_GET['include_deleted_users']==1 ? '&include_deleted_users=1' : '' ) .
				  '&order_type=' . ( $_GET['order_type']=='a' ? 'd' : 'a' );
	
		foreach ( $reports_data as $_tp_planned_course_id => $course_info ) {
		?>
		<table class="regular thickbox botmar4 report_fail" border="0" cellpadding="0" cellspacing="1" width="100%">
		<tr>
			<th colspan="7" style="text-align: center; color:#FBC60E;"><b><?php echo 'COURSE: ' . $course_info['course_date'] . ' ' . $course_info['course_name'] . ' - ' . $course_info['course_notes']; ?></b></th>
		</tr>
		<tr>
			<th width="50"><a class="report_mod_sort_url" href="<?php echo $url_string . '&sort_field=sabre_code'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='sabre_code' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'CODE'; 
			?></a></th>
			<th><a class="report_mod_sort_url" href="<?php echo $url_string . '&sort_field=sabre_subject'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='sabre_subject' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'NAME'; 
			?></a></th>
			<th width="150" style="text-align: center;"><a class="report_mod_sort_url" href="<?php echo $url_string . '&sort_field=user_name'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='user_name' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'USER'; 
			?></a></th>
			<th width="150" style="text-align: center;"><a class="report_mod_sort_url" href="<?php echo $url_string . '&sort_field=passed_by_trainer'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='passed_by_trainer' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'TRAINER'; 
			?></a></th>

			<th width="80" style="text-align: center;"><a class="report_mod_sort_url" href="<?php echo $url_string . '&sort_field=sabre_passed_status'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='sabre_passed_status' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'STATUS'; 
			?></a></th>
			<th width="120" style="text-align: center;"><a class="report_mod_sort_url" href="<?php echo $url_string . '&sort_field=tp_planned_course_date'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='tp_planned_course_date' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'COMPLETE DATE'; 
			?></a></th>
			<th width="120" style="text-align: center;"><a class="report_mod_sort_url" href="<?php echo $url_string . '&sort_field=exp_date'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='exp_date' ? 'text-decoration: underline;' : '' ); ?>"><?php 
				echo 'EXPIRY DATE'; 
			?></a></th>
		</tr>
		<?php
			$flag_use_odd = false;
			for ($i = 0, $N = count($course_info['course_info']); $i < $N; $i++) {
			?>
			<tr <?php echo ( $flag_use_odd ? 'class="odd"' : '' ); ?>>
				<td><?php echo $course_info['course_info'][$i]['sabre_code']; ?></td>
				<td><?php echo $course_info['course_info'][$i]['sabre_subject']; ?></td>
				<td align="center"><?php echo $course_info['course_info'][$i]['user_name']; ?></td>				
				<td align="center"><?php echo $course_info['course_info'][$i]['sabre_passed_by_trainer']; ?></td>
				<td align="center" <?php echo ( $course_info['course_info'][$i]['sabre_none_passed_reason'] != '' ? 'hint="' . $course_info['course_info'][$i]['sabre_none_passed_reason'] . '"' : '' )?>><?php echo ( $course_info['course_info'][$i]['sabre_none_passed_reason'] != '' ? '<u>' : '' ) . $course_info['course_info'][$i]['sabre_passed_status'] . ( $course_info['course_info'][$i]['sabre_none_passed_reason'] != '' ? '</u>' : '' ); ?></td>
				<td align="center"><?php echo $course_info['complete_date']; ?></td>			
				<td align="center"><?php echo $course_info['course_info'][$i]['exp_date']; ?></td>			
			</tr>
			<?php

				if ( $flag_use_odd ) {
					$flag_use_odd = false;
				} else {
					$flag_use_odd = true;
				}
			}
		?>
		</table>
		<?php
		}

	}
	// *****************************************************************************
	//                            EOF OUTPUT section for FAIL report 
	// *****************************************************************************


	// *****************************************************************************
	//                            BOF OUTPUT section for COURSE report 
	// *****************************************************************************
	if ( $selected_report_type=='report_course' ) {
		$form_generate_pdf_report_id = 'form_pdf_report_course';
		
		$url_string = $_SERVER['PHP_SELF'] . '?action=confirm_report&save_to_pdf' . 
	              '&report_type=' . $selected_report_type . 
				  '&month_from=' . $month_from . 
				  '&year_from=' . $year_from .
				  '&month_to=' . $month_to . 
				  '&year_to=' . $year_to .
				  '&tp_courses_id=' . $current_tp_courses_id . 
				  ( (int)$_GET['include_deleted_users']==1 ? '&include_deleted_users=1' : '' ) .
				  '&order_type=' . ( $_GET['order_type']=='a' ? 'd' : 'a' );
		?>
		<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
		<tr>
			<th colspan="6" style="text-align: center; color:#FBC60E;"><b><?php echo 'COURSE REPORT'; ?></b></th>
		</tr>
		<tr>
			<th width="110"><a href="<?php echo $url_string . '&sort_field=tp_planned_course_date'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='tp_planned_course_date' ? 'text-decoration: underline;' : '' ); ?>"><?php echo 'DATE'; ?></a></th>
			<th width="170"><a href="<?php echo $url_string . '&sort_field=tp_planned_course_notes'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='tp_planned_course_notes' ? 'text-decoration: underline;' : '' ); ?>"><?php echo 'COURSE NOTES'; ?></a></th>
			<th><a href="<?php echo $url_string . '&sort_field=tp_planned_course_trainer'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='tp_planned_course_trainer' ? 'text-decoration: underline;' : '' ); ?>"><?php echo 'TRAINER'; ?></a></th>
			<th width="100" style="text-align: center;"><a href="<?php echo $url_string . '&sort_field=percentage'; ?>" style="color:#FBC60E; <?php echo ( $selected_sort_field=='percentage' ? 'text-decoration: underline;' : '' ); ?>"><?php echo 'PASS STATUS'; ?></a></th>
			<th width="150" style="color:#FBC60E;"><?php echo 'ATTENDEES'; ?></th>
			<th width="150" style="color:#FBC60E;"><?php echo 'MODULES'; ?></th>
		</tr>
		<?php
		$flag_use_odd = false;

		for ($i = 0, $N = count($reports_data); $i < $N; $i++) {
		?>
		<tr <?php echo ( $flag_use_odd ? 'class="odd"' : '' ); ?>>
			<td><?php echo $reports_data[$i]['tp_planned_course_date']; ?></td>
			<td><?php echo $reports_data[$i]['tp_planned_course_notes']; ?></td>
			<td><?php echo $reports_data[$i]['tp_planned_course_trainer']; ?></td>
			<td align="center"><?php echo $reports_data[$i]['percentage']; ?></td>
			<td><?php 
				echo '<select style="width:140px;">';
				echo '<option>' . 'Attendee List...' . '</option>';
				foreach ($reports_data[$i]['attendees'] as $key => $user_name ) {
					echo '<option>'	. $user_name . '</option>';
				}
				echo '</select>';
			?></td>
			<td><?php 
				echo '<select style="width:140px;">';
				echo '<option>' . 'Modules List...' . '</option>';
				foreach ($reports_data[$i]['modules'] as $key => $module_name ) {
					echo '<option>'	. $module_name . '</option>';
				}
				echo '</select>';
			?></td>
		</tr>
		<?php
			if ( $flag_use_odd ) {
				$flag_use_odd = false;
			} else {
				$flag_use_odd = true;
			}
		}
		?>
		</table>
		<?php
	}
	// *****************************************************************************
	//                            EOF OUTPUT section for COURSE report 
	// *****************************************************************************


	if ( $url_string!='' && $flag_show_save_pdf_button ) {
		echo '<form name="generate_pdf_report" id="' . $form_generate_pdf_report_id . '" method="post" action="' . $url_string . '" enctype="multipart/form-data">' .
			 '<p align="center">' . 
			 '<input type="submit" value="Save As PDF" name="pdf">' . 
			 '</p>' . 
 			 '</form>';
 		if ( (int)$C['pdf_status'] > 0 ) echo '<br>' . '<div class="report_main_container"><div class="report_progressbar"><table align="center" border="0" cellpadding="2" cellspacing="2"><tr><td>Creating PDF report... Process will take about ' . $C['pdf_status'] . ' seconds</td><td><img src="images/ajax-loader.gif" border="0"></td></tr></table></div></div>';
	}

}
?>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
