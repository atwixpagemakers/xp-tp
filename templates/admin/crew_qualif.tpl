<div id="header">
<?=ddMenuAdmin('cq')?>
</div>

<form name="crew_qualif" method="post" action="<?=$_SERVER['PHP_SELF'] . ($iEdit ?  '?edit=' . $iEdit : '')?>">
<input type="hidden" name="submitted" value="1">
<table class="regular thickbox tc botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
<tr>
	<th width="10%">Code</th>
	<th class="l">Item</th>
	<th width="12%">&nbsp;</th>
</tr>
<tr>
	<td><input type="text" name="cq_code" maxlength="6" value="<?=h(@ $aData['cq_code'])?>" size="5"></td>
	<td class="l"><input type="text" name="cq_desc" value="<?=h(@ $aData['cq_desc'])?>" style="width: 100%"></td>
	<td class="c"><input type="submit" value="  <?=$iEdit ? 'Update' : 'Add'?>  "></td>
</tr>
</table>
</form>

<?php if ( $aCQ ) { ?>
<br>
<form name="delete" method="post" action="<?=$_SERVER['PHP_SELF']?>">
<table id="crew_qualifs" class="regular thickbox tc botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
<tr>
	<th width="100">Code</th>
	<th class="l">Item</th>
	<th width="50">DEL</th>
</tr><?php 
$flag_use_odd = true;		
foreach ($aCQ as $id => $cq) { 
?>
<tr <?php echo ( $flag_use_odd ? 'class="odd"' : '' ); ?>>
	<td><a href="<?=$_SERVER['PHP_SELF'] . '?edit=' . $id?>" title="<?php echo $cq['cq_desc']; ?>"><b><?=h($cq['cq_code'])?></b></a></td>
	<td class="l"><?=h($cq['cq_desc'])?></td>
	<td class="c"><input type="checkbox" name="delete[]" value="<?=$id?>"></td>
</tr>
<? 
	if ( $flag_use_odd ) {
		$flag_use_odd = false;
	} else {
		$flag_use_odd = true;
	}
} # foreach 
?>
</table>
<div class="r"><input type="submit" value="Delete"></div>
</form>
<?php } # if ?>

