<?php
if ( $action != 'edit_planned_course' ) { // when CSV - file is uploaded
?>
<div id="header">
<?php
	echo ddMenuAdmin('cp');

	if ( count($courses_users_table) ) {
		$coursesData = coursesGet();
		
		echo '<form action="' . $_SERVER['PHP_SELF'] . '" method="get" id="form_user_course">';	
		echo '<select id="user_course" name="tp_courses_id">';
		echo '<option ' . ((int)$_GET['tp_courses_id']==0 ? 'selected' : '') . ' value="0">' . '--Select Course--' . '</option>';
		foreach ( $coursesData as $_tp_courses_id => $course_item ) {
			echo '<option ' . ((int)$_GET['tp_courses_id']==$course_item['tp_courses_id'] ? 'selected' : '') . ' value="' . $course_item['tp_courses_id'] . '">' . $course_item['tp_courses_name'] . '</option>';
		}
		echo '</select>';
		echo '</form>';
	}
?>
</div>
<?php
	if ( count($_SESSION['UPLOADED_USERS']) == 0 ) {
?>
		<form name="course_planner" method="post" action="<?php echo $_SERVER['PHP_SELF'] . '?action=new_course_upload'; ?>" enctype="multipart/form-data">
		<input type="hidden" name="submitted" value="1">
		<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td width="100" style="padding:3px;"><b><?php echo 'DATE:'; ?></b></td>
					<td style="padding:3px;"><?php echo '<input type="text" name="planned_course_date" maxlength="6" value="' . ( isset($_POST['planned_course_date']) ? trim($_POST['planned_course_date']) : '' ) . '" size="10">' . '&nbsp;' . '<small>(date format <b>DDMMYY</b>)</small>'; ?></td>
				</tr>	
				<tr>
					<td style="padding:3px;"><b><?php echo 'COURSE NOTES:'; ?></b></td>
					<td style="padding:3px;"><?php echo '<input type="text" name="planned_course_notes" maxlength="255" value="' . ( isset($_POST['planned_course_notes']) ? trim($_POST['planned_course_notes']) : '' ) . '" size="30">'; ?></td>
				</tr>	
				<tr>
					<td style="padding:3px;"><b><?php echo 'TRAINER:'; ?></b></td>
					<td style="padding:3px;"><?php echo '<input type="text" name="planned_course_trainer" maxlength="50" value="' . ( isset($_POST['planned_course_trainer']) ? trim($_POST['planned_course_trainer']) : '' ) . '" size="30">'; ?></td>
				</tr>	
				<tr>
					<td style="padding:3px;"><b><?php echo 'CREW CSV FILE:'; ?></b></td>
					<td style="padding:3px;"><?php 
						echo '<input type="file" name="planned_course_csv">' . 
					    	 '&nbsp;&nbsp;&nbsp;' . 'or' . '&nbsp;&nbsp;&nbsp;' .
							 '<b>' . 'Single User:' . '</b>' . '&nbsp;' . 
							 '<input type="text" name="planned_course_single_user" maxlength="50" value="' . ( isset($_POST['planned_course_single_user']) ? trim($_POST['planned_course_single_user']) : '' ) . '" size="15">' . '&nbsp;' . 
							 '<small>(for example: <b>168302,31</b>)</small>'; 
					?></td>
				</tr>	
				<tr>
					<td style="padding:3px;">&nbsp;</td>
					<td style="padding:3px;"><input type="submit" value="<?php echo 'Submit'; ?>"></td>
				</tr>	

		</table>
		</form>
<?php
	}

	if ( count($courses_users_table) ) {
		echo '<form name="course_custom_user" id="course_custom_user" method="post" action="' . $_SERVER['PHP_SELF'] . '?action=add_new_user&tp_courses_id=' . (int)$_GET['tp_courses_id'] . '">' . 
		     '<input type="hidden" name="new_staff_no" id="new_staff_no" value="">' . 
			 '</form>'; 
		     
		echo '<form name="course_planner" id="course_planner_save" method="post" action="' . $_SERVER['PHP_SELF'] . '?action=course_upload_save">' . 
		     '<input type="hidden" name="tp_courses_id" value="' . (int)$_GET['tp_courses_id'] . '">'; 
	
		
			echo '<div class="tp_course_container">';
	
			echo '<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">';

			echo '<tr>';
			echo '<th align="right">' . $CONFP['TEXT_CONSTANTS']['TABLE_HEADER_DATE'] . '</th>';		
			echo '<td style="padding:3px; border-bottom: 4px solid #2263AD;" colspan="' . ( (count($course_sabres_array) + 1) < $CONFP['COUNT_COLUMNS'] ? $CONFP['COUNT_COLUMNS'] : count($course_sabres_array) ) . '">' . '<input type="text" name="planned_course_date" maxlength="6" value="' . $_SESSION['planned_course_date'] . '" size="10" class="planned_course_date">' . '&nbsp;' . '<small>(date format <b>DDMMYY</b>)</small>' . '</td>';		
			echo '</tr>';

			echo '<tr>';
			echo '<th align="right">' . 'COURSE NOTES:' . '</th>';		
			echo '<td style="padding:3px; border-bottom: 4px solid #2263AD;" colspan="' . ( (count($course_sabres_array) + 1) < $CONFP['COUNT_COLUMNS'] ? $CONFP['COUNT_COLUMNS'] : count($course_sabres_array) ) . '">' . '<input type="text" name="planned_course_notes" maxlength="255" value="' . $_SESSION['planned_course_notes'] . '" size="30">' . '</td>';		
			echo '</tr>';
			
			echo '<tr>';
			echo '<th align="right">' . 'TRAINER:' . '</th>';		
			echo '<td style="padding:3px; border-bottom: 4px solid #2263AD;" colspan="' . ( (count($course_sabres_array) + 1) < $CONFP['COUNT_COLUMNS'] ? $CONFP['COUNT_COLUMNS'] : count($course_sabres_array) ) . '">' . '<input type="text" name="planned_course_trainer" maxlength="50" value="' . ucwords($_SESSION['planned_course_trainer']) . '" size="30">' . '</td>';		
			echo '</tr>';



		$i = 0;
		if ( (int)$_GET['tp_courses_id'] > 0 ) {
			$CS = count($course_sabres_array);
		} else {
			$CS = 1;
		}
		
		
		$_disabled_array = array();		
		while ( $i < $CS ) {
			if ($i > 0 ) {
				echo '<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">';
			}

			echo '<tr>';
			echo '<th width="200" align="right">' . $CONFP['TEXT_CONSTANTS']['TABLE_HEADER_REQUIREMENTS'] . '</th>';

			$start = $i;
			$finish = $start + ($CONFP['COUNT_COLUMNS'] - 2);

			for ( $j = $start, $CR = $finish; $j < $CR; $j++ ) {
				echo '<td width="35" align="center">' . "\n";
				if ( isset($course_sabres_array[$j]) ) {
					$sabre_item_data = $course_sabres_array[$j];

					echo '<a class="sabre_title" href="javascript: void(0);" title="' . $sabre_item_data['sabre_subject'] . '">' . $sabre_item_data['sabre_code'] . '</a>';

					$_disabled_array[] = $sabre_item_data['sabre_id']; // used for DDM options					
				} else {
					echo '&nbsp;&nbsp;&nbsp;';
				}     
				echo '</td>' . "\n";
			}
			echo '</tr>';

			$flag_use_odd = true;				
			foreach ( $courses_users_table as $_no_staff => $user_item_data ) {
				echo '<tr ' . ( $flag_use_odd ? 'class="odd"' : '' ) . ' user_id="row_' . $_no_staff . '">';
				$tmp_user_name = userNameByStaffNo($_no_staff);
				if ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']=='' ) {
					$_user_name = '<font color=red><b>User not detected</b></font>' . '<br>' . 'Staff No.' . $_no_staff;
				} elseif ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']!='' ) {
					$_user_name = $tmp_user_name['firstname'];
				} elseif ( $tmp_user_name['surname']!='' && $tmp_user_name['firstname']=='' ) {
					$_user_name = $tmp_user_name['surname'];
				} else {
					$_user_name = $tmp_user_name['surname'] . ', ' . $tmp_user_name['firstname'];
				}
			
				echo '<td><table border="0" cellpadding="2" cellspacing="0">' . 
						'<tr>' . 
							'<td>' . '<img class="img_delete_user" src="images/icon_remove_user.png" operation_type="remove_row" user_staff_no="' . $_no_staff . '">' . '</td>' .
							'<td>' . $_user_name . '</td>' . 
						'</tr>' . 
					 '</table></td>';

				for ( $j = $start, $CR = $finish; $j < $CR; $j++ ) {
					if ( isset($course_sabres_array[$j]) ) {
						$sabre_item_data = $course_sabres_array[$j];
						echo '<td ' . ( $current_tp_courses_id > 0 ? 'class="tp_sabre"' : '' ) . ' input_code="' . $current_tp_courses_id . '_' . trim($_no_staff) . '_' . $sabre_item_data['sabre_id'] . '" align="center" applied_toggle="0">';

						$sabre_status = $courses_users_table[$_no_staff][$sabre_item_data['sabre_id']];
					
						if ( $sabre_status == 0 ) {
							echo '<img src="images/circle_blank.png" title="' . $CONFP['TEXT_CONSTANTS']['IMG_TITLE_NOT_USERS_MODULE'] . '" border="0" id="img' . $current_tp_courses_id . '_' . trim($_no_staff) . '_' . $sabre_item_data['sabre_id'] . '">';
						} elseif ( $sabre_status == 1 ) {
							echo '<img src="images/circle_black.png" title="' . $CONFP['TEXT_CONSTANTS']['IMG_TITLE_USERS_MODULE'] . '" border="0" id="img' . $current_tp_courses_id . '_' . trim($_no_staff) . '_' . $sabre_item_data['sabre_id'] . '">';
						} elseif ( $sabre_status == 2 ) {
							echo '<img src="images/circle_red.png" title="' . $CONFP['TEXT_CONSTANTS']['IMG_TITLE_USERS_ADDITIONAL_MODULE'] . '" border="0" id="img' . $current_tp_courses_id . '_' . trim($_no_staff) . '_' . $sabre_item_data['sabre_id'] . '">';
						}
				
						if ( $current_tp_courses_id > 0 ) {
							echo '<input type="hidden" id="i' . $current_tp_courses_id . '_' . trim($_no_staff) . '_' . $sabre_item_data['sabre_id'] . '" name="sabre_status[' . $current_tp_courses_id . '][' . trim($_no_staff) . '][' . $sabre_item_data['sabre_id'] . ']" value="' . $sabre_status . '">';
						}
					
						echo '</td>';
					} else {
						echo '<td>' . '&nbsp;&nbsp;&nbsp;' . '</td>';
					}
				}
			}
						
			$i = $j;
			
			echo '</table>';
		}

		if ( isset($_GET['tp_courses_id']) && (int)$_GET['tp_courses_id']>0 ) {
			echo '<p>&nbsp;</p>';
			echo '<p><b>' . 'Additional modules:' . '</b></p>';
			echo '<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">';
			echo '<tr>';
			echo '<th width="200" align="right">' . $CONFP['TEXT_CONSTANTS']['TABLE_HEADER_REQUIREMENTS'] . '</th>';
			for ( $i = 0, $CR = ($CONFP['COUNT_COLUMNS'] - 2); $i < $CR; $i++ ) {
				echo '<td width="35" align="center">' . "\n";
					
				echo '<select style="width: 40px;" class="ddm_additional" name="ddm_additional[' . $i . ']" ddm_id="' . $i . '">' . getDDMOptions(0, $_disabled_array) . '</select>';	 

				echo '</td>' . "\n";
			}
			echo '</tr>';
			
			$flag_use_odd = true;				
			foreach ( $courses_users_table as $_no_staff => $user_item_data ) {
				echo '<tr ' . ( $flag_use_odd ? 'class="odd"' : '' ) . ' user_id="row_' . $_no_staff . '">';
				$tmp_user_name = userNameByStaffNo($_no_staff);
				if ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']=='' ) {
					$_user_name = '<font color=red><b>User not detected</b></font>' . '<br>' . 'Staff No.' . $_no_staff;
				} elseif ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']!='' ) {
					$_user_name = $tmp_user_name['firstname'];
				} elseif ( $tmp_user_name['surname']!='' && $tmp_user_name['firstname']=='' ) {
					$_user_name = $tmp_user_name['surname'];
				} else {
					$_user_name = $tmp_user_name['surname'] . ', ' . $tmp_user_name['firstname'];
				}
			
				echo '<td><table border="0" cellpadding="2" cellspacing="0">' . 
						'<tr>' . 
							'<td>' . '<img class="img_delete_user" src="images/icon_remove_user.png" operation_type="remove_row" user_staff_no="' . $_no_staff . '">' . '</td>' .
							'<td>' . $_user_name . '</td>' . 
						'</tr>' . 
					 '</table></td>';
				
				for ( $i = 0, $CR = ($CONFP['COUNT_COLUMNS'] - 2); $i < $CR; $i++ ) {
					echo '<td align="center" ' . ( $current_tp_courses_id > 0 ? 'class="tp_sabre_additional"' : '' ) . ' input_code="ddm_' . $current_tp_courses_id . '_' . trim($_no_staff) . '_' . $i . '" ddm_id="' . $i . '">';
					echo '<img src="images/circle_blank.png" border="0" ddm_id="' . $i . '" id="img_ddm_' . $current_tp_courses_id . '_' . trim($_no_staff) . '_' . $i . '">';
					echo '<input type="hidden" ddm_id="' . $i . '" id="i_ddm_' . $current_tp_courses_id . '_' . trim($_no_staff) . '_' . $i . '" name="ddm_sabre_status[' . $current_tp_courses_id . '][' . trim($_no_staff) . '][' . $i . ']" value="0">';
					echo '</td>';	
				}
			}
			
			echo '</table>';
		}

		echo '</div>';
		

		echo '<div style="height:30px;">';	
		echo '<div style="display: inline; float: right;"><input type="submit" value="Finalise"></div>';
		echo '</form>';	
	
		echo '<div style="display: inline; float: right;">' .
		 '<form name="course_planner" method="post" action="' . $_SERVER['PHP_SELF'] . '?action=cancel_course_upload" onSubmit="return confirm(\'Do you really want to terminate course planner process?\');">' . 
		 '<input type="submit" value="Cancel">' . 
		 '</form>' . 
		 '</div>';
		echo '<div style="display: inline; float: right;"><input type="button" value="Add User" class="img_add_new_user"></div>';		
		echo '</div>';		 
	}
	
	echo '<div style="height: 100px;">&nbsp;</div>';
} elseif ( $action == 'edit_planned_course' ) { // EDIT course form
// ******************************************************************************
//                               BOF EDIT SECTIN
// ******************************************************************************
	$courses_sabres_table = getSabresByCourse($current_tp_courses_id);
	$another_courses_sabres_table = getAnotherSabresFromPlannedCourse($current_tp_planned_course_id);

	$_disabled_array = array();
	foreach ( $courses_sabres_table as $key => $sabre_item ) {
		$courses_all_sabres_table[] = $sabre_item;

		$_disabled_array[] = $sabre_item['sabre_id']; // used for DDM options
	}

	$another_courses_sabres_ids_table = array();
	foreach ( $another_courses_sabres_table as $key => $sabre_item ) {
		$courses_all_sabres_table[] = $sabre_item;
		$another_courses_sabres_ids_table[] = $sabre_item['sabre_id'];
				
		$_disabled_array[] = $sabre_item['sabre_id']; // used for DDM options
	}
?>
<div id="header">
<?php
	echo ddMenuAdmin('cp');

	echo '<form action="/external/tp/admin/course_planner.php" method="get" id="form_user_course">';
	echo '<input type="hidden" name="action" value="edit_planned_course">';
	echo '<select id="edit_user_course" style="margin: 0 0 0 5px;" name="tp_courses_id">';
	foreach ( $planned_courses as $_tp_courses_id => $course_item_data ) {
		echo '<option ' . ( $current_tp_courses_id == $_tp_courses_id ? 'selected' : '' ) . ' value="' . $_tp_courses_id . '">' . $course_item_data['tp_courses_name'] . '</option>';
	}
	echo '</select>';
	echo '</form>';

	echo '<form action="/external/tp/admin/course_planner.php" method="get" id="form_user_course_modules">';	
	echo '<input type="hidden" name="action" value="edit_planned_course">';
	echo '<select id="edit_user_course_modules" style="margin: 0 0 0 5px;" name="tp_planned_course_id">';
	$i = 1;
	foreach ( $planned_courses_dates[$current_tp_courses_id] as $_tp_planned_course_id => $planned_course_data ) {
		echo '<option ' . ( $current_tp_planned_course_id == $_tp_planned_course_id ? 'selected' : '' ) . ' value="' . $_tp_planned_course_id . '">' . $i . ': ' . date('d/m/y', $planned_course_data['tp_planned_course_date']) . '</option>';
		
		$i++;
	}
	echo '</select>';
	echo '</form>';
?>
</div>
<?php
	if ( count($courses_users_table) ) {
		echo '<form name="course_custom_user" id="course_custom_user" method="post" action="' . $_SERVER['PHP_SELF'] . '?action=add_new_user_to_existing_course&tp_planned_course_id=' . $current_tp_planned_course_id . '">' . 
		     '<input type="hidden" name="new_staff_no" id="new_staff_no" value="">' . 
			 '</form>'; 
	
	
		echo '<form name="course_planner" id="course_planner_save" method="post" action="' . $_SERVER['PHP_SELF'] . '?action=course_edit_save">' .
		     '<input type="hidden" name="tp_planned_course_id" value="' . $current_tp_planned_course_id . '">' . 
		     '<input type="hidden" name="tp_courses_id" value="' . $current_tp_courses_id . '">'; 


		echo '<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">';

		echo '<tr>';
		echo '<th  style="text-align: right">' . $CONFP['TEXT_CONSTANTS']['TABLE_HEADER_DATE'] . '</th>';		
		echo '<td style="padding:3px; border-bottom: 4px solid #2263AD;" colspan="' . ( $CONFP['COUNT_COLUMNS'] - 1 ) . '">' . '<input type="text" name="planned_course_date" maxlength="6" value="' . date( 'dmy', $planned_courses_dates[$current_tp_courses_id][$current_tp_planned_course_id]['tp_planned_course_date'] ) . '" size="10" class="planned_course_date">' . '&nbsp;' . '<small>(date format <b>DDMMYY</b>)</small>' . '</td>';		
		echo '</tr>';

		echo '<tr>';
		echo '<th style="text-align: right">' . 'COURSE NOTES' . '</th>';		
		echo '<td style="padding:3px; border-bottom: 4px solid #2263AD;" colspan="' . ( $CONFP['COUNT_COLUMNS'] - 1 ) . '">' . '<input type="text" name="planned_course_notes" maxlength="255" value="' . get_course_notes($current_tp_planned_course_id) . '" size="30">' . '</td>';		
		echo '</tr>';

		echo '<tr>';
		echo '<th style="text-align: right">' . 'TRAINER' . '</th>';		
		echo '<td style="padding:3px; border-bottom: 4px solid #2263AD;" colspan="' . ( $CONFP['COUNT_COLUMNS'] - 1 ) . '">' . '<input type="text" name="planned_course_trainer" maxlength="50" value="' . get_course_trainer($current_tp_planned_course_id) . '" size="30">' . '</td>';		
		echo '</tr>';

		$i = 0;
		$CS = count($courses_all_sabres_table);

		$ddm_next_counter = 0;
		
		while ( $i < $CS ) {
			if ($i > 0 ) {
				echo '<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">';
			}
			echo '<tr>';
			echo '<th width="200" style="text-align: right">' . $CONFP['TEXT_CONSTANTS']['TABLE_HEADER_REQUIREMENTS'] . '</th>';

			$start = $i;
			$finish = $start + ($CONFP['COUNT_COLUMNS'] - 2);

			$ddm_counter = $ddm_next_counter;
			for ( $j = $start, $CR = $finish; $j < $CR; $j++ ) {
				echo '<td width="35" align="center">' . "\n";
				if ( isset($courses_all_sabres_table[$j]) ) {
					$sabre_item = $courses_all_sabres_table[$j];
					
					if ( in_array($sabre_item['sabre_id'], $another_courses_sabres_ids_table) ) {
						echo '<select style="width: 40px;" class="ddm_additional" name="ddm_additional[' . $ddm_counter . ']" ddm_id="' . $ddm_counter . '">' . getDDMOptions($sabre_item['sabre_id'], $_disabled_array) . '</select>' . 
				 			 '<input type="hidden" name="ddm_additional_prev[' . $ddm_counter . ']" value="' . $sabre_item['sabre_id'] . '">';
					
						$ddm_counter++;
					} else {
						echo '<a class="sabre_title" href="javascript: void(0);" title="' . $sabre_item['sabre_subject'] . '">' . ( isset($sabre_item['color']) && $sabre_item['color']=='red' ? '<font color="red">' : '' ) . $sabre_item['sabre_code'] . ( isset($sabre_item['color']) && $sabre_item['color']=='red' ? '</font>' : '' ) . '</a>';
					}
				} else {
					echo '&nbsp;&nbsp;&nbsp;';
				}     
				echo '</td>' . "\n";
			}
			
			echo '</tr>';
			
			$flag_use_odd = true;	
			
			foreach ( $courses_users_table as $_no_staff => $user_sabres_data ) {
				echo '<tr ' . ( $flag_use_odd ? 'class="odd"' : '' ) . ' user_id="row_' . $_no_staff . '" ' . ( in_array($_no_staff, $_SESSION['DELETED_EDIT_USERS']) ? 'style="display: none;"' : '' ) . '>';

				$tmp_user_name = userNameByStaffNo($_no_staff);
				if ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']=='' ) {
					$_user_name = '<font color=red><b>User not detected</b></font>' . '<br>' . 'Staff No.' . $_no_staff;
				} elseif ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']!='' ) {
					$_user_name = $tmp_user_name['firstname'];
				} elseif ( $tmp_user_name['surname']!='' && $tmp_user_name['firstname']=='' ) {
					$_user_name = $tmp_user_name['surname'];
				} else {
					$_user_name = $tmp_user_name['surname'] . ', ' . $tmp_user_name['firstname'];
				}
			
				echo '<td><table border="0" cellpadding="2" cellspacing="0">' . 
						'<tr>' . 
							'<td>' . '<img class="img_delete_user" src="images/icon_remove_user.png" tp_planned_course_id="' . $current_tp_planned_course_id . '" operation_type="remove_user" user_staff_no="' . $_no_staff . '">' . '</td>' .
							'<td>' . $_user_name . '<input type="hidden" name="delete_staff_no[' . $_no_staff . ']" value="' . ( in_array($_no_staff, $_SESSION['DELETED_EDIT_USERS']) ? '1' : '0' ) . '">' . '</td>' . 
						'</tr>' . 
					 '</table></td>';
			

				//$ddm_counter = 0;
				$ddm_counter = $ddm_next_counter;
				
				for ( $j = $start, $CR = $finish; $j < $CR; $j++ ) {
					if ( isset($courses_all_sabres_table[$j]) ) {
						$sabre_item = $courses_all_sabres_table[$j];
						$sabre_info = $user_sabres_data[$sabre_item['sabre_id']];

						if ( empty($sabre_info['tp_planned_courses_to_sabres_id']) ) {
							$input_code = $current_tp_planned_course_id . '_' . trim($_no_staff) . '_' . $sabre_item['sabre_id'];
						} else {
							$input_code = $sabre_info['tp_planned_courses_to_sabres_id'];
						}

						$sabre_status = $sabre_info['sabre_added_status'];

						if ( in_array($sabre_item['sabre_id'], $another_courses_sabres_ids_table) ) {
							echo '<td align="center" ' . ( $current_tp_planned_course_id > 0 ? 'class="tp_sabre_additional"' : '' ) . ' input_code="ddm_' . $current_tp_planned_course_id . '_' . trim($_no_staff) . '_' . $ddm_counter . '" ddm_id="' . $ddm_counter . '">';

							if ( $sabre_status == 0 ) {
								echo '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_blank.png" title="' . $CONFP['TEXT_CONSTANTS']['IMG_TITLE_NOT_USERS_MODULE'] . '" border="0" ddm_id="' . $ddm_counter . '" id="img_ddm_' . $current_tp_planned_course_id . '_' . trim($_no_staff) . '_' . $ddm_counter . '">';
							} elseif ( $sabre_status == 2 ) {
								echo '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_red.png" title="' . $CONFP['TEXT_CONSTANTS']['IMG_TITLE_USERS_ADDITIONAL_MODULE'] . '" border="0" ddm_id="' . $ddm_counter . '" id="img_ddm_' . $current_tp_planned_course_id . '_' . trim($_no_staff) . '_' . $ddm_counter . '">';
							}

							echo '<input type="hidden" ddm_id="' . $ddm_counter . '" id="i_ddm_' . $current_tp_planned_course_id . '_' . trim($_no_staff) . '_' . $ddm_counter . '" name="ddm_sabre_status[' . $current_tp_planned_course_id . '][' . trim($_no_staff) . '][' . $ddm_counter . ']" value="' . $sabre_status . '">';
							echo '</td>';	

							$ddm_counter++;				
						} else {
							echo '<td class="tp_sabre" input_code="' . $input_code . '" align="center" applied_toggle="0">';
							if ( $sabre_status == 0 ) {
								echo '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_blank.png" title="' . $CONFP['TEXT_CONSTANTS']['IMG_TITLE_NOT_USERS_MODULE'] . '" border="0" id="img' . $input_code . '">';
							} elseif ( $sabre_status == 1 ) {
								echo '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_black.png" title="' . $CONFP['TEXT_CONSTANTS']['IMG_TITLE_USERS_MODULE'] . '" border="0" id="img' . $input_code . '">';
							} elseif ( $sabre_status == 2 ) {
								echo '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_red.png" title="' . $CONFP['TEXT_CONSTANTS']['IMG_TITLE_USERS_ADDITIONAL_MODULE'] . '" border="0" id="img' . $input_code . '">';
							}
				
							echo '<input type="hidden" id="i' . $input_code . '" name="sabre_status[' . $input_code . ']" value="' . $sabre_status . '">';
							echo '</td>';
						}
					} else {
						echo '<td>' . '&nbsp;&nbsp;&nbsp;' . '</td>';
					}		
				}
				
				//$ddm_next_counter = $ddm_counter;
				
				echo '</tr>';		
		
				if ( $flag_use_odd ) {
					$flag_use_odd = false;
				} else {
					$flag_use_odd = true;
				}
			}

			$i = $j;
			echo '</table>';
			
			$ddm_next_counter = $ddm_counter;
		}

			$additional_ddm_counter = $ddm_counter = $ddm_next_counter;

			echo '<p>&nbsp;</p>';
			echo '<p><b>' . 'Additional modules:' . '</b></p>';
			echo '<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">';
			echo '<tr>';
			echo '<th width="200" align="right">' . $CONFP['TEXT_CONSTANTS']['TABLE_HEADER_REQUIREMENTS'] . '</th>';
			
			for ( $i = 0, $CR = ($CONFP['COUNT_COLUMNS'] - 2); $i < $CR; $i++ ) {
				echo '<td width="35" align="center">' . "\n";
					
				echo '<select style="width: 40px;" class="ddm_additional" name="ddm_additional[' . $ddm_counter . ']" ddm_id="' . $ddm_counter . '">' . getDDMOptions(0, $_disabled_array) . '</select>' . 
	 				 '<input type="hidden" name="ddm_additional_prev[' . $ddm_counter . ']" value="0">';	 
					
				$ddm_counter++;

				echo '</td>' . "\n";
			}
			echo '</tr>';
			
			$flag_use_odd = true;				
			foreach ( $courses_users_table as $_no_staff => $user_item_data ) {
				echo '<tr ' . ( $flag_use_odd ? 'class="odd"' : '' ) . ' user_id="row_' . $_no_staff . '" ' . ( in_array($_no_staff, $_SESSION['DELETED_EDIT_USERS']) ? 'style="display: none;"' : '' ) . '>';		
				$tmp_user_name = userNameByStaffNo($_no_staff);
				if ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']=='' ) {
					$_user_name = '<font color=red><b>User not detected</b></font>' . '<br>' . 'Staff No.' . $_no_staff;
				} elseif ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']!='' ) {
					$_user_name = $tmp_user_name['firstname'];
				} elseif ( $tmp_user_name['surname']!='' && $tmp_user_name['firstname']=='' ) {
					$_user_name = $tmp_user_name['surname'];
				} else {
					$_user_name = $tmp_user_name['surname'] . ', ' . $tmp_user_name['firstname'];
				}
			
				echo '<td><table border="0" cellpadding="2" cellspacing="0">' . 
						'<tr>' . 
							'<td>' . '<img class="img_delete_user" src="images/icon_remove_user.png" operation_type="remove_row" user_staff_no="' . $_no_staff . '">' . '</td>' .
							'<td>' . $_user_name . '</td>' . 
						'</tr>' . 
					 '</table></td>';
				
				$ddm_counter = $additional_ddm_counter;
				for ( $i = 0, $CR = ($CONFP['COUNT_COLUMNS'] - 2); $i < $CR; $i++ ) {
					echo '<td align="center" ' . ( $current_tp_planned_course_id > 0 ? 'class="tp_sabre_additional"' : '' ) . ' input_code="ddm_' . $current_tp_planned_course_id . '_' . trim($_no_staff) . '_' . $ddm_counter . '" ddm_id="' . $ddm_counter . '">';
					echo '<img src="images/circle_blank.png" border="0" ddm_id="' . $ddm_counter . '" id="img_ddm_' . $current_tp_planned_course_id . '_' . trim($_no_staff) . '_' . $ddm_counter . '">';
					echo '<input type="hidden" ddm_id="' . $ddm_counter . '" id="i_ddm_' . $current_tp_planned_course_id . '_' . trim($_no_staff) . '_' . $ddm_counter . '" name="ddm_sabre_status[' . $current_tp_planned_course_id . '][' . trim($_no_staff) . '][' . $ddm_counter . ']" value="0">';
					echo '</td>';	
					
					$ddm_counter++;
				}
			}
			
			echo '</table>';



		echo '<div style="height:30px;">';	
		echo '<div style="display: inline; float: right;"><input type="submit" value="Finalise"></div>';
		echo '</form>';	
	
		echo '<div style="display: inline; float: right;">' .
		 '<form id="form_back_to_planned_courses" name="course_planner" method="post" action="admin/planned_course.php?tp_planned_course_id=' . $current_tp_planned_course_id  . '">' . 
		 '<input type="submit" value="Back To Planned Courses">' . 
		 '</form>' . 
		 '</div>';

		echo '<div style="display: inline; float: right;"><input type="button" value="Add User" class="img_add_new_user"></div>';				 
		echo '</div>';		 
	}
// ******************************************************************************
//                               EOF EDIT SECTIN
// ******************************************************************************
}
?>
