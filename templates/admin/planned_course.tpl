<div id="header">
<?php
	echo ddMenuAdmin('pc');

	if ( count($planned_courses) ) {
		echo '<form action="/external/tp/admin/planned_course.php" method="get" id="form_user_course">';	
		echo '<select id="user_course" style="margin: 0 0 0 5px;" name="tp_courses_id">';
		foreach ( $planned_courses as $_tp_courses_id => $course_item_data ) {
			echo '<option ' . ( $current_tp_courses_id == $_tp_courses_id ? 'selected' : '' ) . ' value="' . $_tp_courses_id . '">' . $course_item_data['tp_courses_name'] . '</option>';
		}
		echo '</select>';
		echo '</form>';
	}

	if ( count($planned_courses_dates[$current_tp_courses_id]) ) {
		echo '<form action="/external/tp/admin/planned_course.php" method="get" id="form_user_course_modules">';	
		echo '<select id="user_course_modules" style="margin: 0 10px 0 5px;" name="tp_planned_course_id">';

		$i = 1;
		foreach ( $planned_courses_dates[$current_tp_courses_id] as $_tp_planned_course_id => $planned_course_data ) {
			echo '<option ' . ( $current_tp_planned_course_id == $_tp_planned_course_id ? 'selected' : '' ) . ' value="' . $_tp_planned_course_id . '">' . $i . ': ' . date('d/m/y', $planned_course_data['tp_planned_course_date']) . ( !empty($planned_course_data['tp_planned_course_notes']) ? ' ' . strip_tags($planned_course_data['tp_planned_course_notes']) : '' ) . '</option>';
		
			$i++;
		}
		echo '</select>';
		echo '</form>';
	}
	
	$course_info = getPlannedCourseInfo($current_tp_planned_course_id);
	$course_trainer = ( !empty($course_info['tp_planned_course_trainer']) ? $course_info['tp_planned_course_trainer'] : '<font color="red">' . 'none' . '</font>' );
	echo '<b>' . 'Trainer:' . '</b>' . '&nbsp;' . $course_trainer;
?>
</div>
<?php
if ( count($courses_users_table) ) {

	$i = 0;
	$CS = count($courses_sabres_table);
	while ( $i < $CS ) {
		echo '<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">';
		echo '<tr>';
		echo '<th width="208" align="right">' . 'REQUIREMENTS' . '</th>';
		
		$start = $i;
		$finish = $start + ($CONFP['COUNT_COLUMNS'] - 2);
		for ( $j = $start, $CR = $finish; $j < $CR; $j++ ) {
			if ( isset($courses_sabres_table[$j]) ) {
				$sabre_item = $courses_sabres_table[$j];

				echo '<td width="35" align="center">' . 
					 '<a class="sabre_title" href="javascript: void(0);" title="' . $sabre_item['sabre_subject'] . '">' . ( isset($sabre_item['color']) && $sabre_item['color']=='red' ? '<font color="red">' : '' ) . $sabre_item['sabre_code'] . ( isset($sabre_item['color']) && $sabre_item['color']=='red' ? '</font>' : '' ) . '</a>' .
				     '</td>';
			} else {
				echo '<td width="35">' . '&nbsp;&nbsp;&nbsp;' . '</td>';
			}     
		}
		echo '</tr>';
		

		$flag_use_odd = true;
		foreach ( $courses_users_table as $_no_staff => $user_sabres_data ) {
			echo '<tr ' . ( $flag_use_odd ? 'class="odd"' : '' ) . '>';		

			$tmp_user_name = userNameByStaffNo($_no_staff);
			if ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']=='' ) {
				$_user_name = '<font color=red><b>User not detected</b></font>' . '<br>' . 'Staff No.' . $_no_staff;
			} elseif ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']!='' ) {
				$_user_name = $tmp_user_name['firstname'];
			} elseif ( $tmp_user_name['surname']!='' && $tmp_user_name['firstname']=='' ) {
				$_user_name = $tmp_user_name['surname'];
			} else {
				$_user_name = $tmp_user_name['surname'] . ', ' . $tmp_user_name['firstname'];
			}
			echo '<td>' . $_user_name . '</td>';

			for ( $j = $start, $CR = $finish; $j < $CR; $j++ ) {
				if ( isset($courses_sabres_table[$j]) ) {
					$sabre_item = $courses_sabres_table[$j];

					$sabre_status = $user_sabres_data[$sabre_item['sabre_id']]['sabre_added_status'];
			
					echo '<td align="center">';
					if ( $sabre_status == 0 ) {
						echo '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_blank.png" title="' . $CONFP['TEXT_CONSTANTS']['IMG_TITLE_NOT_USERS_MODULE'] . '" border="0" id="img' . $_tp_courses_id . '_' . trim($_no_staff) . '_' . $_sabre_id . '">';
					} elseif ( $sabre_status == 1 ) {
						echo '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_black.png" title="' . $CONFP['TEXT_CONSTANTS']['IMG_TITLE_USERS_MODULE'] . '" border="0" id="img' . $_tp_courses_id . '_' . trim($_no_staff) . '_' . $_sabre_id . '">';
					} elseif ( $sabre_status == 2 ) {
						echo '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_red.png" title="' . $CONFP['TEXT_CONSTANTS']['IMG_TITLE_USERS_ADDITIONAL_MODULE'] . '" border="0" id="img' . $_tp_courses_id . '_' . trim($_no_staff) . '_' . $_sabre_id . '">';
					}
					echo '</td>';
				} else {
					echo '<td>' . '&nbsp;&nbsp;&nbsp;' . '</td>';
				}		
			}
			echo '</tr>';		
		
			if ( $flag_use_odd ) {
				$flag_use_odd = false;
			} else {
				$flag_use_odd = true;
			}
		}
		
		$i = $j;
		
		echo '</table>' . "\n";
	}
	
	echo '<div style="height:30px;">';
	echo '<div style="display: inline; float: right;">' .
		 '<form name="form_print" method="post" action="' . $_SERVER['PHP_SELF'] . '?action=print_course&tp_planned_course_id=' . $current_tp_planned_course_id . '">' . 
		 '<input type="submit" value="Print">' . 
		 '</form>' . 
		 '</div>';

	echo '<div style="display: inline; float: right;">' .
		 '<form name="form_print" method="post" action="/external/tp/admin/course_planner.php?action=edit_planned_course&tp_planned_course_id=' . $current_tp_planned_course_id . '">' . 
		 '<input type="submit" value="Edit">' . 
		 '</form>' . 
		 '</div>';

	echo '<div style="display: inline; float: right;">' .
		 '<form name="form_print" method="post" action="' . $_SERVER['PHP_SELF'] . '?action=delete_planned_course&tp_planned_course_id=' . $current_tp_planned_course_id . '" onSubmit="return confirm(\'Are you sure you want to delete?\');">' . 
		 '<input type="submit" value="Delete">' . 
		 '</form>' . 
		 '</div>';
	echo '</div>';
} else {
	echo '<center>' . '<font color="red"><b>No courses</b></font>' . '</center>';
}
?>
