<div id="header">
<?php
	echo ddMenuAdmin('cb');

	$_sabre_data = getSabresByCourse($courseData['tp_courses_id']);
	$sabre_id_array = array();
	foreach ( $_sabre_data as $key => $_sabre_item ) {
		$sabre_id_array[] = $_sabre_item['sabre_id'];
	}
?>
</div>

<form name="course_builder" method="post" action="<?=$_SERVER['PHP_SELF'] . ($iEdit ?  '?edit=' . $iEdit : '')?>">
<input type="hidden" name="submitted" value="1">
<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
<tr>
	<th>COURSE NAME</th>
</tr>
<tr>
	<td style="padding: 3px;"><input type="text" name="tp_courses_name" value="<?php echo h(@ $courseData['tp_courses_name']); ?>" size="100"></td>
</tr>
<tr>
	<th class="l" align="left">SABRE CODES:</th>
</tr>
<tr>
	<td><table width="100%">
	<?php
	$aSQ = sabreQualifGet();	

	$output_array = array();
	foreach ( $aSQ as $_sabre_id => $_sabre_data) {
		$output_array[] = $_sabre_data;
	}

	$flag_use_odd = true;	
	$N = count($output_array);
	
	$max_items_per_row = 15;
	if ( $N > $max_items_per_row )	{
		$M = $max_items_per_row;
	} else {
		$M = $N;
	}
	
	for ($i = 0; $i < $N; $i++ ) {
	?>
		<tr <?php echo ( $flag_use_odd ? 'class="odd"' : '' );?>>
			<?php
			for ( $j = 0; $j < $M; $j++ ) {
			?>
			<td width="30" align="center"><?php 
			if ( isset($output_array[$i + $j]['sabre_id']) ) {
				echo '<input type="checkbox" name="sabre_id[' . $output_array[$i + $j]['sabre_id'] . ']" value="1" ' . ( in_array($output_array[$i + $j]['sabre_id'], $sabre_id_array) ? 'checked' : '' ) . '>' . 
				     '<br>' . 
					 '<a class="sabre_title" href="javascript: void(0);" title="' . $output_array[$i + $j]['sabre_subject'] . '">' . $output_array[$i + $j]['sabre_code'] . '</a>';
			} else {
				echo '&nbsp;';
			}
			?></td>
			<?php
			}
			?>
		</tr>
	<?php	
		$i = $i + $M - 1;
		if ( $flag_use_odd ) {
			$flag_use_odd = false;
		} else {
			$flag_use_odd = true;
		}
	}	
	?>
	</table></td>
</tr>
<tr>
	<td align="right" colspan="6"><input type="submit" value="<?php echo 'Submit'; ?>"></td>	
</tr>	
</table>

</form>

<?php 
if ( $coursesData ) { 
?>
<br>
<form name="delete" method="post" action="<?=$_SERVER['PHP_SELF']?>">
<table id="crew_qualifs" class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
<tr>
	<th width="200">COURSE NAME</th>
	<th class="l">SABRE CODES</th>
	<th width="30">DEL</th>
</tr>
<? 

$flag_use_odd = false;
foreach ($coursesData as $_tp_courses_id => $_course_data) { 
?>
<tr <?php echo ( $flag_use_odd ? 'class="odd"' : '' );?>>
	<td><a href="<?php echo $_SERVER['PHP_SELF'] . '?edit=' . $_tp_courses_id; ?>"><b><?php echo h($_course_data['tp_courses_name']); ?></b></a></td>
	<td align="left"><?php
		$_sabre_data = getSabresByCourse($_tp_courses_id);
	
		$_sabre_output_array = array();
		foreach ( $_sabre_data as $key => $_sabre_item ) {
			$_sabre_output_array[] = '<a href="admin/sabre_qualif.php?edit=' . $_sabre_item['sabre_id'] . '" title="' . $_sabre_item['sabre_subject'] . '">' . $_sabre_item['sabre_code'] . '</a>';
		}
	
		echo implode(', ', $_sabre_output_array); 
	?></td>
	<td class="c"><input type="checkbox" name="delete[]" value="<?=$_tp_courses_id?>"></td>
</tr>
<? 
		if ( $flag_use_odd ) {
			$flag_use_odd = false;
		} else {
			$flag_use_odd = true;
		}
} # foreach 

?>
</table>
<div class="r"><input type="submit" value="Delete"></div>
</form>
<?php 
} # if 
?>

