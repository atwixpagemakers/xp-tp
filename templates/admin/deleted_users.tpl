<div id="header">
<?=ddMenuAdmin('deleted_users')?>
</div>
<?php if ( $duData ) { ?>
<br>
<form name="delete" method="post" action="<?=$_SERVER['PHP_SELF']?>">
<table id="crew_qualifs" class="regular thickbox tc botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
<tr>
	<th width="100">Staff No</th>
	<th class="l">User Name</th>
	<th width="50">DEL</th>
</tr><?php 
$flag_use_odd = true;		
foreach ($duData as $id => $du_item) { 
?>
<tr <?php echo ( $flag_use_odd ? 'class="odd"' : '' ); ?>>
	<td><b><?php echo h($du_item['no_staff']); ?></b></td>
	<td class="l"><?php echo h($du_item['user_name']); ?></td>
	<td class="c"><input type="checkbox" name="delete[]" value="<?php echo $du_item['no_staff']; ?>"></td>
</tr>
<?php
	if ( $flag_use_odd ) {
		$flag_use_odd = false;
	} else {
		$flag_use_odd = true;
	}
} # foreach 
?>
</table>
<div class="r"><input type="submit" value="Delete"></div>
</form>
<?php } # if ?>
