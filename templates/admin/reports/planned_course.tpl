<?php
$course_info = getPlannedCourseInfo($current_tp_planned_course_id);
$course_notes = strip_tags($course_info['tp_planned_course_notes']);
$course_trainer = ( !empty($course_info['tp_planned_course_trainer']) ? strip_tags($course_info['tp_planned_course_trainer']) : '<font color="red">' . 'none' . '</font>' );
$current_planned_course_date = $course_info['tp_planned_course_date'];

foreach ( $planned_courses as $_tp_courses_id => $course_item_data ) {
	if ( $current_tp_courses_id == $_tp_courses_id ) {
		$current_course_name = $course_item_data['tp_courses_name'];
		break;		
	}
}

$i = 1;
foreach ( $planned_courses_dates[$current_tp_courses_id] as $_tp_planned_course_id => $planned_course_data ) {
	if ( $current_tp_planned_course_id == $_tp_planned_course_id ) {
		$current_planned_course_number = $i;
		break;
	}

	$i++;
}

if ( count($courses_users_table) ) {
//	ob_start();
	//$mpdf = new mPDF();
	$mpdf = new mPDF('win-1252', 'A4', 0, '', 15, 15, 16, 16, 9, 5);
	
	$mpdf->SetTitle($current_course_name . ' ' . $current_planned_course_number . ':' . date('d/m/y', $current_planned_course_date) );
	$mpdf->WriteHTML(utf8_encode(file_get_contents(ROOT_PROJ . 'css/style.css')), 1);   
	$mpdf->SetHTMLFooter($CONFP['PDF_FOOTER_TEXT']);
		
	$course_notices = '<table border="0" cellpadding="2" cellspacing="2" width="100%" style="font-size:14px;">' . 
			 '<tr>' . 
			 '<td width="120">Course Name:</td>' .
			 '<td><b>' . $current_course_name . '</b></td>' .
			 '<td rowspan="4" align="right">' . '<img src="' . $CONFP['TP_SITE_URL'] . 'images/thomas_cook.jpg" border="0">' . '</td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td>Course Notes:</td>' .
			 '<td><b>' . $course_notes . '</b></td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td>Trainer:</td>' .
			 '<td><b>' . $course_trainer . '</b></td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td>Course Date:</td>' .
			 '<td><b>' . date('d/m/y', $current_planned_course_date) . '</b></td>' .
			 '</tr>' . 
			 '</table><br>';

	$i = 0;
	$CS = count($courses_sabres_table);
	while ( $i < $CS ) {
		$content = '';

		$content .= $course_notices;
	
		$content .= '<div style="border: 5px solid #2263AD;">';
		$content .= '<table style="background: #2263AD;" border="0" cellpadding="0" cellspacing="1" width="100%">';
		
		$content_modules = '';
		$content_modules .= '<tr>';
		$content_modules .= '<td width="200" align="right" style="color: #ffffff; font-weight: bold; padding: 5px;">' . 'REQUIREMENTS' . '</td>';
		
		$start = $i;
		$finish = $start + ($CONFP['COUNT_COLUMNS'] - 2);
		for ( $j = $start, $CR = $finish; $j < $CR; $j++ ) {
			$content_modules .= '<td width="35" align="center" style="background: #ffffff; padding: 5px;">';
			if ( isset($courses_sabres_table[$j]) ) {
				$sabre_item = $courses_sabres_table[$j];

				$content_modules .= ( isset($sabre_item['color']) && $sabre_item['color']=='red' ? '<font color="red">' : '' ) . $sabre_item['sabre_code'] . ( isset($sabre_item['color']) && $sabre_item['color']=='red' ? '</font>' : '' );
			} else {
				$content_modules .= '&nbsp;&nbsp;&nbsp;';
			}     
			$content_modules .= '</td>';
		}
		$content_modules .= '</tr>';

		$content .= $content_modules; 
		
		$flag_use_odd = true;
		$flag_show_black_description = false;
		$flag_show_red_description = false;
		

		$users_count = 1;
		foreach ( $courses_users_table as $_no_staff => $user_sabres_data ) {
			if ( $users_count > 17 ) {
				$users_count = 1;
				
				$content .= '</table>' . "\n";
				$content .= '</div>' . "\n";

				$mpdf->AddPage('L');
				$mpdf->WriteHTML(utf8_encode($content));		

				$content = '';
	
				$content .= $course_notices;

				$content .= '<div style="border: 5px solid #2263AD;">';
				$content .= '<table style="background: #2263AD;" border="0" cellpadding="0" cellspacing="1" width="100%">';
				$content .= $content_modules; 
			}
			
			$content .= '<tr ' . ( $flag_use_odd ? 'style="background-color: #D4E3F2;"' : 'style="background-color: #ffffff;"' ) . '>';		

			$tmp_user_name = userNameByStaffNo($_no_staff);
			if ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']=='' ) {
				$_user_name = '<font color=red><b>User not detected</b></font>' . '<br>' . 'Staff No.' . $_no_staff;
			} elseif ( $tmp_user_name['surname']=='' && $tmp_user_name['firstname']!='' ) {
				$_user_name = $tmp_user_name['firstname'];
			} elseif ( $tmp_user_name['surname']!='' && $tmp_user_name['firstname']=='' ) {
				$_user_name = $tmp_user_name['surname'];
			} else {
				$_user_name = $tmp_user_name['surname'] . ', ' . $tmp_user_name['firstname'];
			}
			$content .= '<td align="right" style="padding: 5px;">' . $_user_name . '</td>';

			for ( $j = $start, $CR = $finish; $j < $CR; $j++ ) {
				$content .= '<td align="center" style="padding: 5px;">';
				if ( isset($courses_sabres_table[$j]) ) {
					$sabre_item = $courses_sabres_table[$j];

					$sabre_status = $user_sabres_data[$sabre_item['sabre_id']]['sabre_added_status'];
			
					if ( $sabre_status == 0 ) {
						$content .= '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_blank.png" title="' . $CONFP['TEXT_CONSTANTS']['IMG_TITLE_NOT_USERS_MODULE'] . '" border="0" id="img' . $_tp_courses_id . '_' . trim($_no_staff) . '_' . $_sabre_id . '">';
					} elseif ( $sabre_status == 1 ) {
						$content .= '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_black.png" title="' . $CONFP['TEXT_CONSTANTS']['IMG_TITLE_USERS_MODULE'] . '" border="0" id="img' . $_tp_courses_id . '_' . trim($_no_staff) . '_' . $_sabre_id . '">';
						$flag_show_black_description = true;
					} elseif ( $sabre_status == 2 ) {
						$content .= '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_red.png" title="' . $CONFP['TEXT_CONSTANTS']['IMG_TITLE_USERS_ADDITIONAL_MODULE'] . '" border="0" id="img' . $_tp_courses_id . '_' . trim($_no_staff) . '_' . $_sabre_id . '">';
						$flag_show_red_description = true;
					}
				} else {
					$content .= '&nbsp;&nbsp;&nbsp;';
				}		
				$content .= '</td>';
			}
			$content .= '</tr>';		
		
			if ( $flag_use_odd ) {
				$flag_use_odd = false;
			} else {
				$flag_use_odd = true;
			}
			
			$users_count++;
		}
		
		$i = $j;
		
		$content .= '</table>' . "\n";
		$content .= '</div>' . "\n";

		$mpdf->AddPage('L');
		$mpdf->WriteHTML(utf8_encode($content));		
	}

	if ( $flag_show_black_description || $flag_show_red_description ) {
		$table_notices = '<br>' . 
				 '<table border="0" cellpadding="2" cellspacing="2" align="left">' .
				 '<tr><td colspan="2"><b>Description of the table:</b></td></tr>' .
				 '<tr>' . 
				 ( $flag_show_black_description ? '<td align="left">' . '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_black.png" border="0">' . '</td>' . '<td>' . '- module is applicable to user' . '</td>' : '' ) .
			     ( $flag_show_red_description ? '<td align="left">' . '<img src="' . $CONFP['TP_SITE_URL'] . 'images/circle_red.png" border="0">' . '</td>' . '<td>' . '- module is applicable to user as additional' . '</td>' : '' ) .
		 		 '</tr>' .
				 '</table>';
		 $mpdf->WriteHTML(utf8_encode($table_notices));		 
	}

//	$content = ob_get_contents();
//	ob_end_clean();

//	$mpdf = new mPDF();
//	$mpdf->SetTitle($current_course_name . ' ' . $current_planned_course_number . ':' . date('d/m/y', $current_planned_course_date) );
//	$mpdf->CurOrientation = 'landscape';
//	$mpdf->WriteHTML(utf8_encode(file_get_contents(ROOT_PROJ . 'css/style.css')), 1);   
//	$mpdf->WriteHTML(utf8_encode($content));		

	$sPDFContent = $mpdf->Output('', 'S');
	ns_http::outputFile($sPDFContent, str_replace(' ', '_', $current_course_name) . '_' . $current_planned_course_number . '_' . date('d.m.y', $current_planned_course_date) . '.pdf', array('from_content' => true));

	exit;
} else {
	echo '<center>' . '<font color="red"><b>No courses</b></font>' . '</center>';
}
?>
