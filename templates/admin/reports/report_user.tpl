<?php
	define('_MPDF_PATH', ROOT_PROJ . '/inc/MPDF/');
	define('ROOT_PAS', realpath(ROOT_PROJ . '../pas') . '/');
	
	require_once _MPDF_PATH . 'mpdf.php';
	require_once ROOT_PAS . 'inc/class.ns.http.php';

	//$mpdf = new mPDF();
	$mpdf = new mPDF('win-1252', 'A4', 0, '', 15, 15, 16, 16, 9, 5);
	
	$mpdf->SetTitle( 'User Report' );
	$mpdf->WriteHTML(utf8_encode(file_get_contents(ROOT_PROJ . 'css/style.css')), 1);   
	$mpdf->SetHTMLFooter($CONFP['PDF_FOOTER_TEXT']);

//	$output_reports_data = array();
//	if ( $current_tp_planned_course_id == 0 ) {
//		$output_reports_data = $reports_data;
//	} else {
//	$output_reports_data[] = $reports_data[$current_tp_planned_course_id];	
//	}	

	//foreach ( $output_reports_data as $_tp_planned_course_id => $course_info ) {
		$course_info = $reports_data[$current_tp_planned_course_id];
	
		//$current_tp_planned_course_id = $course_info['tp_planned_course_id'];
		
		$out_course = $course_info['tp_planned_course_date'] . ' ' . $course_info['tp_courses_name'] . ' - ' . $course_info['tp_planned_course_notes'];	
		$tmp_user_info = userReportName($course_info['tp_planned_course_id'], $selected_no_staff);

		$report_content = '';
		$report_content .= '<table border="0" cellpadding="2" cellspacing="2" width="100%">' . 
					   '<tr>' . 
					   '<td align="center" style="float: center">' . '<h1>User Report</h1>' . '</td>' .
					   '</tr>' . 
					   '</table>';

		if ( $current_tp_planned_course_id > 0 ) {
			$report_content .= '<table border="0" cellpadding="2" cellspacing="2" width="100%" style="font-size:16px;">' . 
			 '<tr>' . 
			 '<td width="120">Course:</td>' .
			 '<td><b>' . $out_course . '</b></td>' .
			 '<td rowspan="3" align="right">' . '<img src="' . $CONFP['TP_SITE_URL'] . 'images/thomas_cook.jpg" border="0">' . '</td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td>User:</td>' .
			 '<td><b>' . $tmp_user_info['firstname'] . ' ' . $tmp_user_info['surname'] . '</b></td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td>&nbsp;</td>' .
			 '<td>&nbsp;</td>' .
			 '</tr>' . 
			 '</table><br>';
		} else {
			$report_content .= '<table border="0" cellpadding="2" cellspacing="2" width="100%" style="font-size:16px;">' . 
			 '<tr>' . 
			 '<td colspan="2"><b>All Courses</b></td>' .
			 '<td rowspan="3" align="right">' . '<img src="' . $CONFP['TP_SITE_URL'] . 'images/thomas_cook.jpg" border="0">' . '</td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td width="120">User:</td>' .
			 '<td><b>' . $tmp_user_info['firstname'] . ' ' . $tmp_user_info['surname'] . '</b></td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td>&nbsp;</td>' .
			 '<td>&nbsp;</td>' .
			 '</tr>' . 
			 '</table><br>';
		}

		$report_content .= '<table style="background: #2263AD;" border="0" cellpadding="0" cellspacing="1" width="100%">' . 
					   '<tr>' . 
					   '<td style="width:100px; color: #FBC60E; font-weight: bold; padding: 5px;">' . ( $selected_sort_field=='sabre_code' ? '<u>' : '' ) . 'CODE' . ( $selected_sort_field=='sabre_code' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:300px; color: #FBC60E; font-weight: bold; padding: 5px;">' . ( $selected_sort_field=='sabre_subject' ? '<u>' : '' ) . 'NAME' . ( $selected_sort_field=='sabre_subject' ? '</u>' : '' ) . '</td>' .
					   '<td style="color: #FBC60E; font-weight: bold; padding: 5px; text-align: center;">' . ( $selected_sort_field=='passed_by_trainer' ? '<u>' : '' ) . 'TRAINER' . ( $selected_sort_field=='passed_by_trainer' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:150px; color: #FBC60E; font-weight: bold; padding: 5px; text-align: center;">' . ( $selected_sort_field=='sabre_passed_status' ? '<u>' : '' ) . 'STATUS' . ( $selected_sort_field=='sabre_passed_status' ? '</u>' : '' ) . '</td>' .
					   //'<td style="width:150px; color: #FBC60E; font-weight: bold; padding: 5px; text-align: center;">' . ( $selected_sort_field=='tp_planned_course_complete_date' ? '<u>' : '' ) . 'COMPLETE DATE' . ( $selected_sort_field=='tp_planned_course_complete_date' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:150px; color: #FBC60E; font-weight: bold; padding: 5px; text-align: center;">' . ( $selected_sort_field=='passed_date' ? '<u>' : '' ) . 'COMPLETE DATE' . ( $selected_sort_field=='passed_date' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:150px; color: #FBC60E; font-weight: bold; padding: 5px; text-align: center;">' . ( $selected_sort_field=='exp_date' ? '<u>' : '' ) . 'EXPIRY DATE' . ( $selected_sort_field=='exp_date' ? '</u>' : '' ) . '</td>' .
					   '</tr>';
		$flag_use_odd = false;
		
		$out_reports_data = $course_info['course_info'];
		for ($i = 0, $N = count($out_reports_data); $i < $N; $i++) {
			$report_content .= '<tr ' . ( $flag_use_odd ? 'style="background-color: #D4E3F2;"' : 'style="background-color: #ffffff;"' ) . '>' . 
							   '<td style="padding: 5px;">' . $out_reports_data[$i]['sabre_code'] . '</td>' . 
							   '<td style="padding: 5px;">' . $out_reports_data[$i]['sabre_subject'] . '</td>' . 

							   '<td style="padding: 5px; text-align:center;">' . $out_reports_data[$i]['sabre_passed_by_trainer'] . '</td>' . 
							   '<td style="padding: 5px; text-align:center;">' . $out_reports_data[$i]['sabre_passed_status'] . ( $out_reports_data[$i]['sabre_none_passed_reason']!='' ? '<br>' . '<font color="red">(' . $out_reports_data[$i]['sabre_none_passed_reason'] . ')</font>' : '' ) . '</td>' . 
							   '<td style="padding: 5px; text-align:center;">' . $out_reports_data[$i]['date'] . '</td>' . 
							   '<td style="padding: 5px; text-align:center;">' . $out_reports_data[$i]['exp_date'] . '</td>' . 
							   '</tr>';

			if ( $flag_use_odd ) {
				$flag_use_odd = false;
			} else {
				$flag_use_odd = true;
			}
		}
		$report_content .= '</table>';

		$mpdf->AddPage('L');
		$mpdf->WriteHTML(utf8_encode($report_content));		
	//}	

	$sPDFContent = $mpdf->Output('', 'S');

	ns_http::outputFile($sPDFContent, 'user_report.pdf', array('from_content' => true));
	
?>
