<?php
	foreach ( $months_array as $key => $month_data ) {
		if ($month_from == $month_data['id']) {
				$month_from_caption = $month_data['text'];

				break;
		}
	}

	foreach ( $years_array as $key => $year_data ) {
		if ($year_from == $year_data['id']) {
			$year_from_caption = $year_data['text'];

			break;
		}
	}

	foreach ( $months_array as $key => $month_data ) {
		if ($month_to == $month_data['id']) {
			$month_to_caption = $month_data['text'];

			break;
		}
	}

	foreach ( $years_array as $key => $year_data ) {
		if ($year_to == $year_data['id']) {
			$year_to_caption = $year_data['text'];

			break;
		}
	}

	foreach ( $bases_array as $key => $base_data ) {
		if ($id_base == $base_data['id']) {
			$id_base_caption = $base_data['text'];
			
			break;
		}
	}

	$report_content = '';
	$report_content .= '<table border="0" cellpadding="2" cellspacing="2" width="100%">' . 
					   '<tr>' . 
					   '<td align="center" style="float: center">' . '<h1>Expiry Report</h1>' . '</td>' .
					   '</tr>' . 
					   '</table>';

	$report_content .= '<table border="0" cellpadding="2" cellspacing="2" width="100%" style="font-size:16px;">' . 
			 '<tr>' . 
			 '<td width="120">Report Period:</td>' .
			 '<td><b>' . $month_from_caption . ' ' . $year_from_caption . ' - ' . $month_to_caption . ' ' . $year_to_caption . '</b></td>' .
			 '<td rowspan="3" align="right">' . '<img src="' . $CONFP['TP_SITE_URL'] . 'images/thomas_cook.jpg" border="0">' . '</td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td>Base:</td>' .
			 '<td><b>' . $id_base_caption . '</b></td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td>&nbsp;</td>' .
			 '<td>&nbsp;</td>' .
			 '</tr>' . 
			 '</table><br>';

	$report_content .= '<table style="background: #2263AD;" border="0" cellpadding="0" cellspacing="1" width="100%">' . 
					   '<tr>' . 
					   '<td style="width:250px; color: #FBC60E; font-weight: bold; padding: 5px;">' . ( $selected_sort_field=='user_name' ? '<u>' : '' ) . 'NAME' . ( $selected_sort_field=='user_name' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:100px; color: #FBC60E; font-weight: bold; padding: 5px;">' . ( $selected_sort_field=='base_shortname' ? '<u>' : '' ) . 'BASE' . ( $selected_sort_field=='base_shortname' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:50px; color: #FBC60E; font-weight: bold; padding: 5px;">' . ( $selected_sort_field=='sabre_code' ? '<u>' : '' ) . 'CODE' . ( $selected_sort_field=='sabre_code' ? '</u>' : '' ) . '</td>' .
					   '<td style="color: #FBC60E; font-weight: bold; padding: 5px;">' . ( $selected_sort_field=='sabre_subject' ? '<u>' : '' ) . 'MODULE' . ( $selected_sort_field=='sabre_subject' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:200px; color: #FBC60E; font-weight: bold; padding: 5px;">' . ( $selected_sort_field=='date' ? '<u>' : '' ) . 'COMPLETE DATE' . ( $selected_sort_field=='date' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:200px; color: #FBC60E; font-weight: bold; padding: 5px;">' . ( $selected_sort_field=='exp_date' ? '<u>' : '' ) . 'EXPIRY DATE' . ( $selected_sort_field=='exp_date' ? '</u>' : '' ) . '</td>' .
					   '</tr>';

	$flag_use_odd = false;
	$flag_show_m_explanation = false;
	for ($i = 0, $N = count($reports_data); $i < $N; $i++) {
		if ( $reports_data[$i]['manually_added_by_trainer']==1 ) {
			$reports_data[$i]['user_name'] .= ' (M)';
			
			$flag_show_m_explanation = true;
		}
	
		$report_content .= '<tr ' . ( $flag_use_odd ? 'style="background-color: #D4E3F2;"' : 'style="background-color: #ffffff;"' ) . '>' . 
							   '<td style="padding: 5px;">' . $reports_data[$i]['user_name'] . '</td>' . 
							   '<td style="padding: 5px;">' . $reports_data[$i]['base_shortname'] . '</td>' . 
							   '<td style="padding: 5px;">' . $reports_data[$i]['sabre_code'] . '</td>' . 
							   '<td style="padding: 5px;">' . $reports_data[$i]['sabre_subject'] . '</td>' . 
							   '<td style="padding: 5px;">' . $reports_data[$i]['date'] . '</td>' . 
							   '<td style="padding: 5px;">' . $reports_data[$i]['exp_date'] . '</td>' . 
							   '</tr>';

		if ( $flag_use_odd ) {
			$flag_use_odd = false;
		} else {
			$flag_use_odd = true;
		}
	}
	$report_content .= '</table>';
	
	if ( $flag_show_m_explanation == true ) {
		$report_content .= '<p>&nbsp;</p>' . 
		                   '<p>(M) - Manually added by Trainer</p>';
	}

	define('_MPDF_PATH', ROOT_PROJ . '/inc/MPDF/');
	define('ROOT_PAS', realpath(ROOT_PROJ . '../pas') . '/');
	
	require_once _MPDF_PATH . 'mpdf.php';
	require_once ROOT_PAS . 'inc/class.ns.http.php';
			
	//$mpdf = new mPDF();
	$mpdf = new mPDF('win-1252', 'A4', 0, '', 15, 15, 16, 16, 9, 5);
	
	$mpdf->SetTitle( 'Expiry Report' );
	$mpdf->CurOrientation = 'landscape';
	$mpdf->WriteHTML(utf8_encode(file_get_contents(ROOT_PROJ . 'css/style.css')), 1);   
	$mpdf->SetHTMLFooter($CONFP['PDF_FOOTER_TEXT']);
	$mpdf->WriteHTML(utf8_encode($report_content));		

	$sPDFContent = $mpdf->Output('', 'S');

	ns_http::outputFile($sPDFContent, 'expiry_report.pdf', array('from_content' => true));
?>
