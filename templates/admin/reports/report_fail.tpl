<?php
	foreach ( $months_array as $key => $month_data ) {
		if ($month_from == $month_data['id']) {
				$month_from_caption = $month_data['text'];

				break;
		}
	}

	foreach ( $years_array as $key => $year_data ) {
		if ($year_from == $year_data['id']) {
			$year_from_caption = $year_data['text'];

			break;
		}
	}

	foreach ( $months_array as $key => $month_data ) {
		if ($month_to == $month_data['id']) {
			$month_to_caption = $month_data['text'];

			break;
		}
	}

	foreach ( $years_array as $key => $year_data ) {
		if ($year_to == $year_data['id']) {
			$year_to_caption = $year_data['text'];

			break;
		}
	}

	foreach ( $bases_array as $key => $base_data ) {
		if ($id_base == $base_data['id']) {
			$id_base_caption = $base_data['text'];
			
			break;
		}
	}
	
	$id_fail = (int)$_GET['id_fail'];
	foreach ( $fail_data_array as $key => $fail_data ) {
		if ($id_fail == $fail_data['id']) {
			$id_fail_caption = $fail_data['text'];
			
			break;
		}
	}

	$report_header_content .= '<table border="0" cellpadding="2" cellspacing="2" width="100%">' . 
					   '<tr>' . 
					   '<td align="center" style="float: center">' . '<h1>Failure Report</h1>' . '</td>' .
					   '</tr>' . 
					   '</table>';

	$report_header_content .= '<table border="0" cellpadding="2" cellspacing="2" width="100%" style="font-size:16px;">' . 
			 '<tr>' . 
			 '<td width="120">Report Period:</td>' .
			 '<td><b>' . $month_from_caption . ' ' . $year_from_caption . ' - ' . $month_to_caption . ' ' . $year_to_caption . '</b></td>' .
			 '<td rowspan="3" align="right">' . '<img src="' . $CONFP['TP_SITE_URL'] . 'images/thomas_cook.jpg" border="0">' . '</td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td>Base:</td>' .
			 '<td><b>' . $id_base_caption . '</b></td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td>Failure:</td>' .
			 '<td><b>' . $id_fail_caption . '</b></td>' .
			 '</tr>' . 
			 ( (int)$_GET['include_deleted_users']==1 ? '<tr><td>Deleted Users:</td>' .	'<td><b>Yes</b></td>' . '</tr>' : '' ) . "\n" .
 			 '</table><br>';

	define('_MPDF_PATH', ROOT_PROJ . '/inc/MPDF/');
	define('ROOT_PAS', realpath(ROOT_PROJ . '../pas') . '/');
	
	require_once _MPDF_PATH . 'mpdf.php';
	require_once ROOT_PAS . 'inc/class.ns.http.php';

	//$mpdf = new mPDF();
	$mpdf = new mPDF('win-1252', 'A4', 0, '', 15, 15, 16, 16, 9, 5);
	
	$mpdf->SetTitle( 'Failure Report' );
	$mpdf->WriteHTML(utf8_encode(file_get_contents(ROOT_PROJ . 'css/style.css')), 1);   
	$mpdf->SetHTMLFooter($CONFP['PDF_FOOTER_TEXT']);

	foreach ( $reports_data as $_tp_planned_course_id => $course_info ) {
		$report_content = '';
		$report_content .= $report_header_content;

		$report_content .= '<table style="background: #2263AD;" border="0" cellpadding="0" cellspacing="1" width="100%">' . 
					   '<tr>' . 
					   '<td colspan="7" style="color: #FBC60E; font-weight: bold; padding: 5px; text-align: center;">' . 'COURSE: ' . $course_info['course_date'] . ' ' . $course_info['course_name'] . ' - ' . $course_info['course_notes'] . '</td>' . 
					   '</tr>' . 
					   '<tr>' . 
					   '<td style="width:50px; color: #FBC60E; font-weight: bold; padding: 5px;">' . ( $selected_sort_field=='sabre_code' ? '<u>' : '' ) . 'CODE' . ( $selected_sort_field=='sabre_code' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:300px; color: #FBC60E; font-weight: bold; padding: 5px;">' . ( $selected_sort_field=='sabre_subject' ? '<u>' : '' ) . 'NAME' . ( $selected_sort_field=='sabre_subject' ? '</u>' : '' ) . '</td>' .
					   '<td style="color: #FBC60E; font-weight: bold; padding: 5px; text-align: center;">' . ( $selected_sort_field=='user_name' ? '<u>' : '' ) . 'USER' . ( $selected_sort_field=='user_name' ? '</u>' : '' ) . '</td>' .
					   '<td style="color: #FBC60E; font-weight: bold; padding: 5px; text-align: center;">' . ( $selected_sort_field=='passed_by_trainer' ? '<u>' : '' ) . 'TRAINER' . ( $selected_sort_field=='passed_by_trainer' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:150px; color: #FBC60E; font-weight: bold; padding: 5px; text-align: center;">' . ( $selected_sort_field=='sabre_passed_status' ? '<u>' : '' ) . 'STATUS' . ( $selected_sort_field=='sabre_passed_status' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:150px; color: #FBC60E; font-weight: bold; padding: 5px; text-align: center;">' . ( $selected_sort_field=='tp_planned_course_date' ? '<u>' : '' ) . 'COMPLETE DATE' . ( $selected_sort_field=='tp_planned_course_date' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:150px; color: #FBC60E; font-weight: bold; padding: 5px; text-align: center;">' . ( $selected_sort_field=='exp_date' ? '<u>' : '' ) . 'EXPIRY DATE' . ( $selected_sort_field=='exp_date' ? '</u>' : '' ) . '</td>' .
					   '</tr>';
		$flag_use_odd = false;
		
		$out_reports_data = $course_info['course_info'];
		$flag_show_m_explanation = false;
		for ($i = 0, $N = count($out_reports_data); $i < $N; $i++) {
			if ( $out_reports_data[$i]['manually_added_by_trainer']==1 ) {
				$out_reports_data[$i]['user_name'] .= ' (M)';
			
				$flag_show_m_explanation = true;
			}
		
			$report_content .= '<tr ' . ( $flag_use_odd ? 'style="background-color: #D4E3F2;"' : 'style="background-color: #ffffff;"' ) . '>' . 
							   '<td style="padding: 5px;">' . $out_reports_data[$i]['sabre_code'] . '</td>' . 
							   '<td style="padding: 5px;">' . $out_reports_data[$i]['sabre_subject'] . '</td>' . 
							   '<td style="padding: 5px; text-align:center;">' . $out_reports_data[$i]['user_name'] . '</td>' . 
							   '<td style="padding: 5px; text-align:center;">' . $out_reports_data[$i]['sabre_passed_by_trainer'] . '</td>' . 
							   '<td style="padding: 5px; text-align:center;">' . $out_reports_data[$i]['sabre_passed_status'] . ( $out_reports_data[$i]['sabre_none_passed_reason']!='' ? '<br>' . '<font color="red">(' . $out_reports_data[$i]['sabre_none_passed_reason'] . ')</font>' : '' ) . '</td>' . 
							   '<td style="padding: 5px; text-align:center;">' . $course_info['complete_date'] . '</td>' . 
							   '<td style="padding: 5px; text-align:center;">' . $out_reports_data[$i]['exp_date'] . '</td>' . 
							   '</tr>';

			if ( $flag_use_odd ) {
				$flag_use_odd = false;
			} else {
				$flag_use_odd = true;
			}
		}
		$report_content .= '</table>';
		if ( $flag_show_m_explanation == true ) {
			$report_content .= '<p>&nbsp;</p>' . 
		                   '<p>(M) - Manually added by Trainer</p>';
		}

		$mpdf->AddPage('L');
		$mpdf->WriteHTML(utf8_encode($report_content));		
	}	

	$sPDFContent = $mpdf->Output('', 'S');

	ns_http::outputFile($sPDFContent, 'fail_report.pdf', array('from_content' => true));
	
?>
