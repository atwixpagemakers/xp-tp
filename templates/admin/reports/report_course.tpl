<?php
	foreach ( $months_array as $key => $month_data ) {
		if ($month_from == $month_data['id']) {
				$month_from_caption = $month_data['text'];

				break;
		}
	}

	foreach ( $years_array as $key => $year_data ) {
		if ($year_from == $year_data['id']) {
			$year_from_caption = $year_data['text'];

			break;
		}
	}

	foreach ( $months_array as $key => $month_data ) {
		if ($month_to == $month_data['id']) {
			$month_to_caption = $month_data['text'];

			break;
		}
	}

	foreach ( $years_array as $key => $year_data ) {
		if ($year_to == $year_data['id']) {
			$year_to_caption = $year_data['text'];

			break;
		}
	}

	foreach ( $completed_courses_array as $key => $course_info ) {
		if ( $course_info['id'] == $current_tp_courses_id ) {
			$out_course = $course_info['text'];
			
			break;
		}
	}

	$report_content = '';
	$report_content .= '<table border="0" cellpadding="2" cellspacing="2" width="100%">' . 
					   '<tr>' . 
					   '<td align="center" style="float: center">' . '<h1>COURSE Report</h1>' . '</td>' .
					   '</tr>' . 
					   '</table>';

	$report_content .= '<table border="0" cellpadding="2" cellspacing="2" width="100%" style="font-size:16px;">' . 
			 '<tr>' . 
			 '<td width="120">Report Period:</td>' .
			 '<td><b>' . $month_from_caption . ' ' . $year_from_caption . ' - ' . $month_to_caption . ' ' . $year_to_caption . '</b></td>' .
			 '<td rowspan="3" align="right">' . '<img src="' . $CONFP['TP_SITE_URL'] . 'images/thomas_cook.jpg" border="0">' . '</td>' .
			 '</tr>' . 
			 '<tr>' . 
			 '<td>Course:</td>' .
			 '<td><b>' . $out_course . '</b></td>' .
			 '</tr>' . 
			 ( (int)$_GET['include_deleted_users']==1 ? '<tr><td>Deleted Users:</td>' .	'<td><b>Yes</b></td>' . '</tr>' : '' ) . "\n" .
			 '<tr>' . 
			 '<td>&nbsp;</td>' .
			 '<td>&nbsp;</td>' .
			 '</tr>' . 
			 '</table><br>';

		 
	$report_content .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">' . 
					   '<tr style="background: #2263AD;">' . 
					   '<td style="width:120px; color: #FBC60E; font-weight: bold; padding: 5px;">' . ( $selected_sort_field=='tp_planned_course_date' ? '<u>' : '' ) . 'DATE' . ( $selected_sort_field=='tp_planned_course_date' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:180px; color: #FBC60E; font-weight: bold; padding: 5px;">' . ( $selected_sort_field=='tp_planned_course_notes' ? '<u>' : '' ) . 'COURSE NOTES' . ( $selected_sort_field=='tp_planned_course_notes' ? '</u>' : '' ) . '</td>' .
					   '<td style="color: #FBC60E; font-weight: bold; padding: 5px;">' . ( $selected_sort_field=='tp_planned_course_trainer' ? '<u>' : '' ) . 'TRAINER' . ( $selected_sort_field=='tp_planned_course_trainer' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:100px; color: #FBC60E; font-weight: bold; padding: 5px; text-align: center;">' . ( $selected_sort_field=='percentage' ? '<u>' : '' ) . 'STATUS' . ( $selected_sort_field=='percentage' ? '</u>' : '' ) . '</td>' .
					   '<td style="width:200px; color: #FBC60E; font-weight: bold; padding: 5px; ">' . 'ATTENDEES' . '</td>' .
					   '<td style="width:300px; color: #FBC60E; font-weight: bold; padding: 5px; ">' . 'MODULES' . '</td>' .
					   '</tr>';

	$flag_use_odd = false;
	$flag_show_m_explanation = false;

	$available_statuses = array(
		'1' => 'Pass',
		'2' => 'Pass-Retake',
		'3' => 'Fail',
		'4' => 'Complete',
		'5' => 'Not Complete',
		'6' => 'Not-Attended');

	for ($i = 0, $N = count($reports_data); $i < $N; $i++) {
		$out_status = '';
		$out_users = '';
		$out_modules = '';

		$flag_first_line = true;
		if ( count($pdf_passed_data_array[$reports_data[$i]['tp_planned_course_id']]) > 0 ) {
			foreach ( $pdf_passed_data_array[$reports_data[$i]['tp_planned_course_id']] as $status_id => $status_data ) {
				$out_status .= '<p>' . $available_statuses[$status_id] . '</p>';
				
				$additional_tags_count = 0;

				foreach ( $status_data['modules'] as $sabre_id => $module_data  ) {
					if ( !$flag_first_line ) {
						$out_modules .= '<p>&nbsp;</p>';
					}
					$out_modules .= '<p>' . $module_data['module_info']['sabre_code'] . ': ' . $module_data['module_info']['sabre_subject'] . '</p>';
					
					$additional_modules_tags_count = 0;
					$flag_first_user = true;

					$additional_tags_count += count($module_data['users']);
					
					if ( !$flag_first_line ) {
						$out_users .= '<p>&nbsp;</p>';
					}
					
					$additional_tags_count++;					
					foreach ( $module_data['users'] as $key => $user_data  ) {
						$out_users .= '<p ' . ($flag_first_user ? ''/*'style="color:red;"'*/ : '' ) . '>' . ( empty($user_data['username']) ? $user_data['no_staff'] : $user_data['username'] ) . '</p>' . "\n";
						
						$additional_modules_tags_count++;
						
						if ( $flag_first_user ) $flag_first_user = false;
					}
					
					$report_content .= '<tr ' . ( $flag_use_odd ? 'style="background-color: #D4E3F2;"' : 'style="background-color: #ffffff;"' ) . '>' . "\n" .
						'<td style="padding: 5px; border-left: 1px solid #2263AD;' . ( $flag_first_line ? 'border-top: 1px solid #2263AD;' : '' ) . '" valign="top">' . ( $flag_first_line ? $reports_data[$i]['tp_planned_course_date'] : '&nbsp;' ) . '</td>' . "\n" .
						'<td style="padding: 5px; border-left: 1px solid #2263AD;' . ( $flag_first_line ? 'border-top: 1px solid #2263AD;' : '' ) . '" valign="top">' . ( $flag_first_line ? $reports_data[$i]['tp_planned_course_notes'] : '&nbsp;' ) . '</td>' . "\n" . 
						'<td style="padding: 5px; border-left: 1px solid #2263AD;' . ( $flag_first_line ? 'border-top: 1px solid #2263AD;' : '' ) . '" valign="top">' . ( $flag_first_line ? $reports_data[$i]['tp_planned_course_trainer'] : '&nbsp;' ) . '</td>' . "\n";
					$report_content .= '<td style="padding: 5px; text-align:center; border-left: 1px solid #2263AD;' . ( $flag_first_line ? 'border-top: 1px solid #2263AD;' : '' ) . '" valign="top">' . $out_status . '</td>' . "\n";
					$report_content .= '<td style="padding: 5px; border-left: 1px solid #2263AD;' . ( $flag_first_line ? 'border-top: 1px solid #2263AD;' : '' ) . '" valign="top">' . $out_users . '</td>' . "\n";
					$report_content .= '<td style="padding: 5px; border-left: 1px solid #2263AD; border-right: 1px solid #2263AD;' . ( $flag_first_line ? 'border-top: 1px solid #2263AD;' : '' ) . '" valign="top">' . $out_modules . '</td>' . "\n";
					$report_content .= '</tr>' . "\n";
			
					$out_status = '&nbsp;';
					$out_users = '';
					$out_modules = '';

					if ( $flag_first_line ) {
						$flag_first_line = false;
					}
				}
			}
		}

		$report_content .= '<tr ' . ( $flag_use_odd ? 'style="background-color: #D4E3F2;"' : 'style="background-color: #ffffff;"' ) . '>' . 
				'<td style="padding: 5px; border-left: 1px solid #2263AD;border-bottom: 1px solid #2263AD;" valign="top">' . '&nbsp;' . '</td>' . 
				'<td style="padding: 5px; border-left: 1px solid #2263AD;border-bottom: 1px solid #2263AD;" valign="top">' . '&nbsp;' . '</td>' . 
				'<td style="padding: 5px; border-left: 1px solid #2263AD;border-bottom: 1px solid #2263AD;" valign="top">' . '&nbsp;' . '</td>';
			$report_content .= '<td style="padding: 5px; border-left: 1px solid #2263AD; border-bottom: 1px solid #2263AD;text-align:center;" valign="top">' . '&nbsp;' . '</td>';
			$report_content .= '<td style="padding: 5px; border-left: 1px solid #2263AD;border-bottom: 1px solid #2263AD;" valign="top">' . '&nbsp;' . '</td>';
			$report_content .= '<td style="padding: 5px; border-left: 1px solid #2263AD; border-right: 1px solid #2263AD; border-bottom: 1px solid #2263AD;" valign="top">' . '&nbsp;' . '</td>';
			$report_content .= '</tr>' . "\n";
		
		if ( $flag_use_odd ) {
			$flag_use_odd = false;
		} else {
			$flag_use_odd = true;
		}
	}

	$report_content .= '</table>';

	if ( $flag_show_m_explanation == true ) {
		$report_content .= '<p>&nbsp;</p>' . 
		                   '<p>(M) - Manually added by Trainer</p>';
	}

	define('_MPDF_PATH', ROOT_PROJ . '/inc/MPDF/');
	define('ROOT_PAS', realpath(ROOT_PROJ . '../pas') . '/');
	
	require_once _MPDF_PATH . 'mpdf.php';
	require_once ROOT_PAS . 'inc/class.ns.http.php';
			
	//$mpdf = new mPDF();
	$mpdf = new mPDF('win-1252', 'A4', 0, '', 15, 15, 16, 16, 9, 5);
	
	$mpdf->SetTitle( 'User Report' );
	$mpdf->CurOrientation = 'landscape';
	$mpdf->WriteHTML(utf8_encode(file_get_contents(ROOT_PROJ . 'css/style.css')), 1);   
	$mpdf->SetHTMLFooter($CONFP['PDF_FOOTER_TEXT']);
	
	$mpdf->WriteHTML(utf8_encode($report_content));		

	$sPDFContent = $mpdf->Output('', 'S');

	ns_http::outputFile($sPDFContent, 'course_report.pdf', array('from_content' => true));
?>
