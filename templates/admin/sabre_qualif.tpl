<div id="header">
<?php
	echo ddMenuAdmin('sq');

	$_crew_data = getCrewsBySabre($sData['sabre_id']);
	$cq_id_array = array();
	foreach ( $_crew_data as $key => $_crew_item ) {
		$cq_id_array[] = $_crew_item['cq_id'];
	}
	
?>
</div>

<form name="sabre_qualif" method="post" action="<?=$_SERVER['PHP_SELF'] . ($iEdit ?  '?edit=' . $iEdit : '')?>" id="sabre_qualif">
<input type="hidden" name="submitted" value="1">
<table class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
<tr>
	<th width="50">CODE</th>
	<th class="l">SUBJECT</th>
	<th width="60" align="left" style="text-align: center;">RANK</th>
	<th width="40" align="center" style="text-align: center;">EXP</th>
	<th width="40" align="center" style="text-align: center;">P/F</th>
	<th width="40" align="center" style="text-align: center;">EOM</th>
	<th width="40" align="center" style="text-align: center;">GRACE</th>
</tr>
<tr>
	<td style="padding: 3px;"><input type="text" name="sabre_code" value="<?php echo h(@ $sData['sabre_code']); ?>" size="3"></td>
	<td class="l"style="padding: 3px;"><input type="text" name="sabre_subject" value="<?php echo h(@ $sData['sabre_subject']); ?>" style="width: 100%"></td>
	<td style="padding: 3px;" align="left"><select name="sabre_rank">
		<option value="0" <?php echo ( $sData['sabre_rank']==0 ? 'selected' : '' ); ?>><?php echo $CONFP['SABRE_RANKS'][0]; ?></option>
		<option value="1" <?php echo ( $sData['sabre_rank']==1 ? 'selected' : '' ); ?>><?php echo $CONFP['SABRE_RANKS'][1]; ?></option>
		<option value="2" <?php echo ( $sData['sabre_rank']==2 ? 'selected' : '' ); ?>><?php echo $CONFP['SABRE_RANKS'][2]; ?></option>
	</select></td>
	<td style="padding: 3px;" align="center"><input type="text" id="sabre_exp" name="sabre_exp" size="2" value="<?php echo h(@ $sData['sabre_exp']); ?>"></td>
	<td style="padding: 3px;" align="center"><input type="checkbox" name="sabre_pf" value="1" <?php echo ( $sData['sabre_pf'] > 0 ? "checked" : "" ); ?>></td>
	<td style="padding: 3px;" align="center"><input type="checkbox" name="sabre_eom" value="1" <?php echo ( $sData['sabre_eom'] > 0 ? "checked" : "" ); ?>></td>
	<td style="padding: 3px;" align="center"><input type="checkbox" name="sabre_grace" value="1" <?php echo ( $sData['sabre_grace'] > 0 ? "checked" : "" ); ?>></td>
</tr>
<tr>
	<th colspan="7" class="l" align="left">CREW:</th>
</tr>
<tr>
	<td colspan="7"><table width="100%">
	<?php

	$aCQ = crewQualifGet();	

	$output_array = array();
	foreach ( $aCQ as $_cq_id => $_crew_data) {
		$output_array[] = $_crew_data;
	}

	$flag_use_odd = true;	
	for ($i = 0, $N = count($output_array); $i < $N; $i++ ) {
	?>
		<tr <?php echo ( $flag_use_odd ? 'class="odd"' : '' );?>>
			<?php
			for ( $j = 0, $M = 3; $j < $M; $j++ ) {
			?>
			<td><?php 
			if ( isset($output_array[$i + $j]['cq_id']) ) {
				echo '<input class="cq_id" type="checkbox" name="cq_id[' . $output_array[$i + $j]['cq_id'] . ']" value="1" ' . ( in_array($output_array[$i + $j]['cq_id'], $cq_id_array) ? 'checked' : '' ) . '>' . '&nbsp;' .  $output_array[$i + $j]['cq_code'] . ' - ' . $output_array[$i + $j]['cq_desc'];
			} else {
				echo '&nbsp;';
			}
			?></td>
			<?php
			}
			?>
		</tr>
	<?php	
		$i = $i + $M - 1;
		if ( $flag_use_odd ) {
			$flag_use_odd = false;
		} else {
			$flag_use_odd = true;
		}
	}	
	?>
	</table></td>
</tr>
<tr>
	<td align="right" colspan="7"><input type="submit" value="<?php echo 'Submit'; ?>"></td>	
</tr>	
</table>

</form>

<?php 
if ( $aSQ ) { 
?>
<br>
<form name="delete" method="post" action="<?=$_SERVER['PHP_SELF']?>">
<table id="crew_qualifs" class="regular thickbox botmar4" border="0" cellpadding="0" cellspacing="1" width="100%">
<tr>
	<th width="50">CODE</th>
	<th width="250" class="l">SUBJECT</th>
	<th align="left">CREW</th>	
	<th width="60" align="center" style="text-align: center;">RANK</th>	
	<th width="40" align="center" style="text-align: center;">EXP</th>
	<th width="40" align="center" style="text-align: center;">P/F</th>
	<th width="40" align="center" style="text-align: center;">EOM</th>
	<th width="40" align="center" style="text-align: center;">GRACE</th>
	<th width="30">DEL</th>
</tr>
<? 

$flag_use_odd = false;
foreach ($aSQ as $id => $sq) { 
?>
<tr <?php echo ( $flag_use_odd ? 'class="odd"' : '' );?>>
	<td><a href="<?php echo $_SERVER['PHP_SELF'] . '?edit=' . $id; ?>"><b><?php echo h($sq['sabre_code']); ?></b></a></td>
	<td class="l"><?php echo h($sq['sabre_subject']); ?></td>
	<td align="left"><?php
		$_crew_data = getCrewsBySabre($id);
	
		$_crew_output_array = array();
		foreach ( $_crew_data as $key => $_crew_item ) {
			$_crew_output_array[] = '<a href="admin/crew_qualif.php?edit=' . $_crew_item['cq_id'] . '" title="' . $_crew_item['cq_desc'] . '">' . $_crew_item['cq_code'] . '</a>';
		}
	
		echo implode(', ', $_crew_output_array); 
	?></td>
	<td align="center"><b><?php echo $CONFP['SABRE_RANKS'][$sq['sabre_rank']]; ?></b></td>	
	<td align="center"><b><?php echo $sq['sabre_exp']; ?></b></td>
	<td align="center"><b><?php echo ( $sq['sabre_pf'] == 1 ? 'Y' : 'N' ); ?></b></td>
	<td align="center"><b><?php echo ( $sq['sabre_eom'] == 1 ? 'Y' : 'N' ); ?></b></td>
	<td align="center"><b><?php echo ( $sq['sabre_grace'] == 1 ? 'Y' : 'N' ); ?></b></td>
	<td class="c"><input type="checkbox" name="delete[]" value="<?=$id?>"></td>
</tr>
<? 
		if ( $flag_use_odd ) {
			$flag_use_odd = false;
		} else {
			$flag_use_odd = true;
		}
} # foreach 

?>
</table>
<div class="r"><input type="submit" value="Delete"></div>
</form>
<?php 
} # if 
?>

