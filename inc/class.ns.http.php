<?

class ns_http
{
	function prepareRequest()
	{
		// Save some memory.. (since we don't use these anyway.)
		unset($GLOBALS['HTTP_POST_VARS'], $GLOBALS['HTTP_POST_VARS']);
		unset($GLOBALS['HTTP_POST_FILES'], $GLOBALS['HTTP_POST_FILES']);

		// These keys shouldn't be set...ever.
		if (isset($_REQUEST['GLOBALS']) || isset($_COOKIE['GLOBALS']))
			die('Invalid request variable.');

		// Same goes for numeric keys.
		foreach (array_merge(array_keys($_REQUEST), array_keys($_FILES)) as $key)
			if (is_numeric($key)) die('Invalid (numeric) request variable.');

		// Numeric keys in cookies are less of a problem. Just unset those.
		foreach ($_COOKIE as $key => $value)
			if (is_numeric($key)) unset($_COOKIE[$key]);

		// Get the correct query string.  It may be in an environment variable...
		if (!isset($_SERVER['QUERY_STRING']))
			$_SERVER['QUERY_STRING'] = getenv('QUERY_STRING');

		// There's no query string, but there is a URL... try to get the data from there.
		if (!empty($_SERVER['QUERY_STRING']))
		{
			$request = $_SERVER['QUERY_STRING'];

			// Replace '*.php?a.b.c/d/e.f' with 'a=b.c&d=&e=f' and parse it into $_GET.
			mb_parse_str(strtr(preg_replace('~(?<=^|/)([^\./]+)\.~', '/$1=', strtr($request, '?', '/')), '/', '&'), $temp);

			foreach ($temp as $k => $v) if ( '?' == $k{0} ) unset($temp[$k]);
			$_GET = $temp;
		}

		if ( get_magic_quotes_gpc() || ini_get('magic_quotes_sybase') )
		{
			$_GET = ns_http::stripslashesRecursive($_GET);
			$_POST = ns_http::stripslashesRecursive($_POST);
			$_COOKIE = ns_http::stripslashesRecursive($_COOKIE);
		}

		// Let's not depend on the ini settings... why even have COOKIE in there, anyway?
		$_REQUEST = $_POST + $_GET;
	}

	# ----------

	// Strips the slashes off any array or variable.  Two underscores for the normal reason.
	function stripslashesRecursive($var)
	{
		if (!is_array($var))
			return stripslashes($var);

		// Reindex the array without slashes, this time.
		$new_var = array();

		// Strip the slashes from every element.
		foreach ($var as $k => $v)
			$new_var[stripslashes($k)] = ns_http::stripslashesRecursive($v);

		return $new_var;
	}

	# ----------

	// Adds slashes to the array/variable.  Uses two underscores to guard against overloading.
	function addslashesRecursive($var)
	{
		if (!is_array($var))
			return addslashes($var);

		// Reindex the array with slashes.
		$new_var = array();

		// Add slashes to every element, even the indexes!
		foreach ($var as $k => $v)
			$new_var[addslashes($k)] = ns_http::addslashesRecursive($v);

		return $new_var;
	}

	# ----------

	// Removes url stuff from the array/variable. Uses two underscores to guard against overloading.
	function urldecodeRecursive($var)
	{
		if (!is_array($var))
			return urldecode($var);

		// Reindex the array...
		$new_var = array();

		// Add the htmlspecialchars to every element.
		foreach ($var as $k => $v)
			$new_var[urldecode($k)] = ns_http::urldecodeRecursive($v);

		return $new_var;
	}

	# ----------

	function appendSID($url, $with_qm = true)
	{
		if ( defined('SID') && trim(SID) && !preg_match('~'. preg_quote(session_name()) .'[=\.]~', $url) )
		{
			$url = ns_http::queryStringAppend($url, str_replace('=','.',SID), $with_qm);
		}

		return $url;
	}

	# ----------

	function queryStringMake($arr = array())
	{
		if ( !is_array($arr) ) return @strval($arr);
		$qs = '';
		foreach ($arr as $k => $v) if ( $k ) $qs .= '/'. urlencode($k) . ($v ? '.'. urlencode($v) : '');
		return trim($qs, '/');
	}

	# ----------

	function queryStringAppend($uri, $app, $with_qm = true)
	{
		if ( is_array($app) ) $app = ns_http::queryStringMake($app);
		$app = trim($app, '/');
		if ( !$app ) return $uri;

		$pos = strpos($uri, '?');
		$need_qm = ($with_qm && $pos === false);
		$need_sep = !$need_qm && preg_match('~[^\?\/]$~', $uri);

		return $uri . ($need_qm ? '?' : '') . ($need_sep ? '/' : '') . $app;
	}

	# ----------

	function get_headers($url, $format = false, $referer = '')
	{
		$url_info = parse_url($url);
		
		$authline = $refererline = '';
		if ( @$url_info['user'] )
		{
			$authentification = base64_encode(@$url_info['user'].':'.@$url_info['pass']);
			$authline = "Authorization: Basic $authentification\r\n";
		}
		if (!empty($referer))
		{
			$refererline = "Referer: $referer\r\n";
		}

		$port = isset($url_info['port']) ? $url_info['port'] : 80;

		if( $fp = fsockopen($url_info['host'], $port, $errno, $errstr, 30) )
		{
			$head = "GET ".@$url_info['path']."?".@$url_info['query']." HTTP/1.0\r\n";
			if (!empty($url_info['port']))
			{
				$head .= "Host: ".@$url_info['host'].":".$url_info['port']."\r\n";
			}
			else
			{
				$head .= "Host: ".@$url_info['host']."\r\n";
			}
			$head .= "Connection: Close\r\n";
			$head .= "Accept: */*\r\n";
			$head .= $refererline;
			$head .= $authline;
			$head .= "\r\n";
			fputs($fp, $head);

			while( !feof($fp) || (true == $eoheader) )
			{
				if($header=fgets($fp, 1024))
				{
					if ( "\r\n" == $header )
					{
						$eoheader = true; break;
					}
					else
					{
						$header = trim($header);
					}

					if( $format )
					{
						$key = array_shift(explode(':',$header));
						if($key == $header)
						{
							$headers[] = $header;
						}
						else
						{
							$headers[$key] = substr($header,strlen($key)+2);
						}
						unset($key);
					}
					else
					{
						$headers[] = $header;
					}
				} 
			}
			return $headers;
		}
		else
		{
			return false;
		}
	}

	# ----------

	function isHttpFile($_sUrl)
	{
		$aTmp = get_headers($_sUrl);
		return (false !== $aTmp && false !== strpos($aTmp[0], ' 200 ')); # HTTP/1.0 200 OK
	}

	# ----------

	function outputFile($file, $showfilename='', $options = array())
	{
		$from_content = isset($options['from_content']);
		//First, see if the file exists
		if ( ! $from_content && ! is_file($file)) { die('<b>404 File not found!</b>'); }

		//Gather relevent info about file
		if ( $from_content )
		{
			$len = function_exists('mb_strlen') ? mb_strlen($file) : strlen($file);
			$pathinfo = pathinfo($showfilename);
			$filename = @ $pathinfo['basename'];
			$fileext = @ $pathinfo['extension'];
		}
		else
		{
			$len = filesize($file);
			$pathinfo = pathinfo($file);
			$filename = @ $pathinfo['basename'];
			$fileext = @ $pathinfo['extension'];
		}

		if ( $showfilename ) $filename = $showfilename;

		if ( ! $from_content && function_exists('mime_content_type') && ($mime = @mime_content_type($file)) )
		{
			$ctype = $mime;
		}
		else
		{
			//This will set the Content-Type to the appropriate setting for the file
			switch( $fileext )
			{
				case 'pdf': $ctype = 'application/pdf'; break;
				case 'exe': $ctype = 'application/octet-stream'; break;
				case 'zip': $ctype = 'application/zip'; break;
				case 'doc': $ctype = 'application/msword'; break;
				case 'xls': $ctype = 'application/vnd.ms-excel'; break;
				case 'ppt': $ctype = 'application/vnd.ms-powerpoint'; break;
				case 'gif': $ctype = 'image/gif'; break;
				case 'png': $ctype = 'image/png'; break;
				case 'jpeg':
				case 'jpg': $ctype = 'image/jpg'; break;
				case 'mp3': $ctype = 'audio/mpeg'; break;
				case 'wav': $ctype = 'audio/x-wav'; break;
				case 'mpeg':
				case 'mpg':
				case 'mpe': $ctype = 'video/mpeg'; break;
				case 'mov': $ctype = 'video/quicktime'; break;
				case 'avi': $ctype = 'video/x-msvideo'; break;

				//The following are for extensions that shouldn't be downloaded (sensitive stuff, like php files)
				case 'php':
				case 'txt': $ctype ='text/plain'; break;
				case 'htm':
				case 'html': $ctype ='text/html'; break;
			}
		}

		if ( empty($ctype) ) $ctype='application/force-download';

		//Begin writing headers
		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Cache-Control: public'); 
		header('Content-Description: File Transfer');
		
		//Use the switch-generated Content-Type
		header('Content-Type: ' . $ctype);

		$content_disposition = 'attachment';
		if ( @ $options['content_disposition'] && in_array(strtolower($options['content_disposition']), array('inline', 'extension-token')) )
		{
			$content_disposition = strtolower($options['content_disposition']);
		}

		header('Content-Disposition: ' . $content_disposition . '; filename="' . $filename . '";');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . $len);
		if ( $from_content ) echo $file;
		else @readfile($file);
		exit;
	}

	# ----------

}

?>