<?php

function pageTitle($title = '')
{
	global $CONFP;

	pageAssignVar('pageTitle', ($title ? $title . ' - ' : '') . $CONFP['PROJECT_NAME']);
}

/*
 * project error handler
 */
function errorHandlerProj($no, $str, $file='', $line='', $context='')
{
	global $errors_caught;

	if ( $no == E_STRICT ) return false;

	$er = error_reporting();
	if ( ! $er || ! ($er & $no) ) return false;

	switch ($no)
	{
		case E_NOTICE:
		case E_USER_NOTICE:
			$type = 'notice';
			break;

		case E_WARNING:
		case E_USER_WARNING:
			$type = 'warning';
			break;

		case E_ERROR:
		case E_USER_ERROR:
			$type = 'error';
			break;

		default:
			$type = 'unknown';
			break;
	}
	$errors_caught[] = array($type, $no, $str, $file, $line);
	return true;
}

/*
 * Formats date in MySQL format to specified format
 *
 * @param string $format   format
 * @param string $date     source date
 * @param boolean $na      when date is empty returns 'N/A' if TRUE or '' otherwise
 *
 * @return boolean
 */
function dateFormat($format, $date, $na = true)
{
	if ( dateEmpty($date) ) return $na ? 'N/A' : '';
    return date($format, is_numeric($date) ? $date : strtotime($date));
}

/*
 * Parses and converts date from project format (DDMMYY) to MySQL format
 *
 * @param string $date          source date
 * @param bool $allow_convert   indicates if date convert say Nov 31 (incorrect) to Dec 01 is allowed
 *
 * @return string
 */
function dateParse($date, $allow_convert = false)
{
	if ( ! empty($date) && preg_match('~^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])([0-9]{2})$~', trim($date), $arr) )
	{
		$ymd = $arr[3] . '-' . $arr[2] . '-' . $arr[1];
		if ( ! $allow_convert && dateFormat('d', $ymd) != $arr[1] ) return ''; # prevent to return converted date
		return dateFormat('Y-m-d', $ymd);
	}

	return '';
}

/*
 * Loads/returns Crew Qualifications from DB
 *
 * @param int $id           if not 0 - returns CQ row
 * @param boolean $forced   forced loads from DB (without using cache)
 *
 * @return array (keys are fleets IDs)/string
 */
function crewQualifGet($id = NULL, $forced = false)
{
	global $CONFP, $db;
	static $cache = array();

	if ( $forced || ! $cache )
	{
		$cache = array();
		$CONFP['CREW_QUALIF_CODES'] = array();
		$CONFP['CREW_QUALIF_CODE_2_ID'] = array();
		$CONFP['CREW_QUALIF_DESC'] = array();

		$res = $db->query('SELECT * FROM ' . $CONFP['TABLE_CREW_QUALIF'] . '
		WHERE 1
		ORDER BY cq_code');

		if ( ! PEAR::isError($res) )
		{
			while ( $row = $res->fetchRow() )
			{
				$cache[$row['cq_id']] = $row;
				$CONFP['CREW_QUALIF_CODES'][$row['cq_id']] = mb_strtoupper($row['cq_code']);
				$CONFP['CREW_QUALIF_DESC'][$row['cq_id']] = $row['cq_desc'];
			}
			$CONFP['CREW_QUALIF_CODE_2_ID'] = array_flip($CONFP['CREW_QUALIF_CODES']);
		}
	}

	if ( ! empty($id) ) return @ $cache[$id];

	return $cache;
}

function sabreQualifGet($id = NULL, $forced = false)
{
	global $CONFP, $db;
	static $cache = array();

	if ( $forced || ! $cache )
	{
		$cache = array();

		if ( $id == NULL ) {
			$res = $db->query('SELECT * FROM ' . $CONFP['TABLE_TP_SABRE_QUALIF'] . ' ORDER BY sabre_code');
		} else {
			$res = $db->query('SELECT * FROM ' . $CONFP['TABLE_TP_SABRE_QUALIF'] . ' WHERE sabre_id = "' . $id . '"');
		}
		

		if ( ! PEAR::isError($res) )
		{
			while ( $row = $res->fetchRow() )
			{
				$cache[$row['sabre_id']] = $row;
			}
		}
	}

	if ( ! empty($id) ) return @ $cache[$id];

	return $cache;
}

function sabreCodeExists($_sabre_code) {
	global $CONFP, $db;

	$res = $db->query('SELECT sabre_id FROM ' . $CONFP['TABLE_TP_SABRE_QUALIF'] . ' WHERE sabre_code="' . $_sabre_code . '"');

	if ( $res->numRows() > 0 ) {
		$row = $res->fetchRow();
		
		return $row['sabre_id'];
	} else {
		return false;
	}
}

/*
function sabreIDByCode($_sabre_code) {
	global $CONFP, $db;

	$res = $db->query('SELECT sabre_id FROM ' . $CONFP['TABLE_TP_SABRE_QUALIF'] . ' WHERE sabre_code="' . $_sabre_code . '"');

	if ( $res->numRows() > 0 ) {
		
		
		return true;
	} else {
		return 0;
	}
}
*/
function crewAssignedToSabres($_cq_id) {
	global $CONFP, $db;

	$res = $db->query('SELECT DISTINCT sabre_id FROM ' . $CONFP['TABLE_TP_CREWS_TO_SABRES'] . ' WHERE cq_id = "' . $_cq_id . '"');

	return $res->numRows();
}

function crewCodeAssignedToSabre($_cq_code, $_sabre_id) {
	global $CONFP, $db;

	$res = $db->query('SELECT * 
	                     FROM ' . $CONFP['TABLE_TP_CREWS_TO_SABRES'] . ' c2s, ' . $CONFP['TABLE_CREW_QUALIF'] . ' cq 
	                   WHERE cq.cq_code = "' . addslashes($_cq_code) . '" 
					     and c2s.cq_id = cq.cq_id 
					     and c2s.sabre_id = "' . $_sabre_id . '"');
	if ( $res->numRows() > 0 ) {
		return true;
	} else {
		return false;
	}
}

/*
function userPassedSabreInCourse( $_no_staff, $_sabre_id, $_tp_courses_id ) {
	global $CONFP, $db;
						 
	$res = $db->query('SELECT pc2s.tp_planned_courses_to_sabres_id 
	                     FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' pc2s, ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' pc
	                   WHERE pc2s.no_staff = "' . $_no_staff . '" 
						 and pc2s.sabre_id = "' . $_sabre_id . '"
					     and pc2s.tp_planned_course_id = pc.tp_planned_course_id 
					     and pc.tp_courses_id = "' . $_tp_courses_id . '"
						 and pc2s.sabre_passed_status in (' . $CONFP['TRAINER_MARKS']['pass'] . ',' . $CONFP['TRAINER_MARKS']['pass-retake'] . ',' . $CONFP['TRAINER_MARKS']['complete'] . ')
					ORDER BY pc2s.passed_date desc');
	if ( $res->numRows() > 0 ) {
		$row = $res->fetchRow();

		return $row['tp_planned_courses_to_sabres_id'];
	} else {
		return false;
	}
}
*/
function userPassedSabreInCourse( $_no_staff, $_sabre_id, $current_tp_planned_courses_to_sabres_id = 0 ) {
	global $CONFP, $db;
						 
	$res = $db->query('SELECT pc2s.tp_planned_courses_to_sabres_id 
	                     FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' pc2s
	                   WHERE pc2s.no_staff = "' . $_no_staff . '" 
						 ' . ( $current_tp_planned_courses_to_sabres_id > 0 ? ' and pc2s.tp_planned_courses_to_sabres_id <> ' . $current_tp_planned_courses_to_sabres_id : '' ) . '
						 and pc2s.sabre_id = "' . $_sabre_id . '"
						 and pc2s.sabre_passed_status in (' . $CONFP['TRAINER_MARKS']['pass'] . ',' . $CONFP['TRAINER_MARKS']['pass-retake'] . ',' . $CONFP['TRAINER_MARKS']['complete'] . ')
					ORDER BY pc2s.passed_date desc');
	if ( $res->numRows() > 0 ) {
		$row = $res->fetchRow();

		return $row['tp_planned_courses_to_sabres_id'];
	} else {
		return false;
	}
}


function getCrewsBySabre($_sabre_id) {
	global $CONFP, $db;

	$res = $db->query('SELECT cq.cq_id, cq.cq_code, cq.cq_desc  
					   FROM ' . $CONFP['TABLE_CREW_QUALIF'] . ' cq, ' . $CONFP['TABLE_TP_CREWS_TO_SABRES'] . ' c2s 
					   WHERE c2s.sabre_id = "' . $_sabre_id . '"
					     AND c2s.cq_id = cq.cq_id
					ORDER BY cq.cq_code');

	$cache = array();

	if ( ! PEAR::isError($res) ) {
		while ( $row = $res->fetchRow() ) {
			$cache[] = $row;
		}
	}
	
	return $cache;
}

function getSabresByCourse($_tp_courses_id) {
	global $CONFP, $db;

	$res = $db->query('SELECT sq.sabre_id, sq.sabre_code, sq.sabre_subject, sq.sabre_rank  
					   FROM ' . $CONFP['TABLE_TP_SABRE_QUALIF'] . ' sq, ' . $CONFP['TABLE_TP_SABRES_TO_COURSES'] . ' s2c 
					   WHERE s2c.tp_courses_id = "' . $_tp_courses_id . '"
					     AND s2c.sabre_id = sq.sabre_id
					ORDER BY sq.sabre_code');

	$cache = array();

	if ( ! PEAR::isError($res) ) {
		while ( $row = $res->fetchRow() ) {
			$cache[] = $row;
		}
	}
	
	return $cache;
}

function getSabresByPlannedCourse($_tp_planned_course_id, $_no_staff) {
	global $CONFP, $db;

	$res = $db->query('SELECT sabre_id, 
	                          sabre_added_status, 
							  sabre_passed_status, 
							  tp_planned_courses_to_sabres_id, 
							  sabre_none_passed_reason, 
							  passed_date 
					   FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . '
					   WHERE tp_planned_course_id = "' . $_tp_planned_course_id . '"
					     AND no_staff = "' . $_no_staff . '"');

	$cache = array();

	if ( ! PEAR::isError($res) ) {
		while ( $row = $res->fetchRow() ) {
			$cache[$row['sabre_id']] = $row;
		}
	}
	
	return $cache;
}

function getAnotherSabresFromPlannedCourse($_tp_planned_course_id) {
	global $CONFP, $db;

/*
	$res = $db->query('SELECT sq.sabre_id, sq.sabre_code 
					   FROM ' . $CONFP['TABLE_TP_SABRE_QUALIF'] . ' sq, ' . $CONFP['TABLE_TP_SABRES_TO_COURSES'] . ' s2c,  ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' pc
					   WHERE pc.tp_planned_course_id = "' . $_tp_planned_course_id . '"
					   s2c.tp_courses_id = pc.tp_courses_id
					     AND s2c.sabre_id = sq.sabre_id
					ORDER BY sq.sabre_code');
*/

	$res = $db->query('SELECT DISTINCT sq.sabre_id, sq.sabre_code, sq.sabre_subject 
					   FROM ' . $CONFP['TABLE_TP_SABRE_QUALIF'] . ' sq, ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' pc2s
					   WHERE pc2s.tp_planned_course_id = "' . $_tp_planned_course_id . '"
					     AND pc2s.sabre_id = sq.sabre_id
					     AND pc2s.sabre_id NOT IN ( SELECT sq.sabre_id 
					   FROM ' . $CONFP['TABLE_TP_SABRE_QUALIF'] . ' sq, ' . $CONFP['TABLE_TP_SABRES_TO_COURSES'] . ' s2c,  ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' pc
					   WHERE pc.tp_planned_course_id = "' . $_tp_planned_course_id . '"
					     AND s2c.tp_courses_id = pc.tp_courses_id
					     AND s2c.sabre_id = sq.sabre_id )
					ORDER BY sq.sabre_code');

	$cache = array();

	if ( ! PEAR::isError($res) ) {
		while ( $row = $res->fetchRow() ) {
			$cache[] = $row;
		}
	}
	
	return $cache;
}


function coursesGet() {
	global $CONFP, $db;
	static $cache = array();

	if ( $forced || ! $cache )
	{
		$cache = array();

		$res = $db->query('SELECT * FROM ' . $CONFP['TABLE_TP_COURSES'] . '	ORDER BY tp_courses_name');

		if ( ! PEAR::isError($res) )
		{
			while ( $row = $res->fetchRow() )
			{
				$cache[$row['tp_courses_id']] = $row;
			}
		}
	}

	if ( ! empty($id) ) return @ $cache[$id];

	return $cache;
}

function getCourseIDByName($_tp_courses_name) {
	global $CONFP, $db;

	$res = $db->query('SELECT tp_courses_id FROM ' . $CONFP['TABLE_TP_COURSES'] . ' WHERE tp_courses_name = "' . $_tp_courses_name . '"');

	if ( $res->numRows() > 0 ) {
		$row = $res->fetchRow();
		
		return tp_courses_id;
	} else {
		return 0;
	}
}

function sabreAssignedToCourses($_sabre_id) {
	global $CONFP, $db;

	$res = $db->query('SELECT DISTINCT tp_courses_id FROM ' . $CONFP['TABLE_TP_SABRES_TO_COURSES'] . ' WHERE sabre_id = "' . $_sabre_id . '"');

	return $res->numRows();
}

function sabreAssignedToPlannedCourses($_sabre_id) {
	global $CONFP, $db;

	$res = $db->query('SELECT DISTINCT pc2s.tp_planned_course_id 
	                              FROM tp_planned_courses_to_sabres pc2s, tp_planned_courses pc 
								 WHERE pc2s.sabre_id = "' . $_sabre_id . '"
								   AND pc.tp_planned_course_completed = 0
								   AND pc2s.tp_planned_course_id = pc.tp_planned_course_id');

	return $res->numRows();
}

function getCoursesByCrewCode($_cq_code) {
	global $CONFP, $db;

	$res = $db->query('SELECT DISTINCT tp_c.tp_courses_id, tp_c.tp_courses_name, tp_c.date_added, tp_c.date_modified
						 FROM tp_courses tp_c, 
						      tp_sabres_to_courses tp_s2c, 
							  tp_crews_to_sabres tp_c2s, 
							  tp_crew_qualif tp_cq
                        WHERE tp_c.tp_courses_id = tp_s2c.tp_courses_id
						  AND tp_c2s.sabre_id = tp_s2c.sabre_id
						  AND tp_c2s.cq_id = tp_cq.cq_id
                          AND tp_cq.cq_code = "' . $_cq_code . '"
                     ORDER BY tp_c.tp_courses_id asc');
	$cache = array();
	while ( $row = $res->fetchRow() ) {
		$cache[$row['tp_courses_id']] = $row;
	}

	return $cache;
}


function userNameByStaffNo($_no_staff)
{
	global $db;

	$res = $db->query('SELECT firstname, surname FROM ' . CONFP('TABLE_USERS_XP') . ' WHERE no_staff = "' . trim($_no_staff) . '"');

	$row = $res->fetchRow();
	
	return $row;
}

function userReportName($_tp_planned_course_id, $_no_staff)
{
	global $db;

	$res = $db->query('SELECT DISTINCT pc2s.firstname, pc2s.surname 
								  FROM tp_planned_courses pc, tp_planned_courses_to_sabres pc2s 
								 WHERE pc.tp_planned_course_id = ' . (int)$_tp_planned_course_id . '
								   AND pc2s.tp_planned_course_id = pc.tp_planned_course_id
								   AND pc2s.no_staff = "' . trim($_no_staff) . '"
							     LIMIT 1');

	$row = $res->fetchRow();
	
	return $row;
}

function getAllPlannedCourses($_get_only_active_courses = false) {
	global $CONFP, $db;
	if ( $_get_only_active_courses ) {
		$res = $db->query('SELECT DISTINCT pc.tp_courses_id, c.tp_courses_name, pc.tp_planned_course_date 
						 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' pc, ' . $CONFP['TABLE_TP_COURSES'] . ' c
                        WHERE pc.tp_courses_id = c.tp_courses_id
                          AND pc.tp_planned_course_completed = 0
                     ORDER BY c.tp_courses_name asc');
	} else {
		$res = $db->query('SELECT DISTINCT pc.tp_courses_id, c.tp_courses_name, pc.tp_planned_course_date 
						 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' pc, ' . $CONFP['TABLE_TP_COURSES'] . ' c 
                        WHERE pc.tp_courses_id = c.tp_courses_id
                     ORDER BY c.tp_courses_name asc');
	}

	$cache = array();
	while ( $row = $res->fetchRow() ) {
		$cache[$row['tp_courses_id']] = $row;
	}

	return $cache;
}

function getPlannedCourseDates($_tp_courses_id, $_get_only_active_courses = false) {
	global $CONFP, $db;
	
	if ( $_get_only_active_courses ) {
		$res = $db->query('SELECT tp_planned_course_id, tp_planned_course_date, tp_planned_course_notes
						 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' 
                        WHERE tp_courses_id = "' . $_tp_courses_id . '"
                          AND tp_planned_course_completed = 0
                     ORDER BY tp_planned_course_date asc');
	} else {
		$res = $db->query('SELECT tp_planned_course_id, tp_planned_course_date, tp_planned_course_notes
						 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' 
                        WHERE tp_courses_id = "' . $_tp_courses_id . '"
                     ORDER BY tp_planned_course_date');
	}

	$cache = array();
	while ( $row = $res->fetchRow() ) {
		$cache[$row['tp_planned_course_id']] = $row;
	}

	return $cache;
}

function getPlannedCourseStatus($_tp_planned_course_id) {
	global $CONFP, $db;
	$res = $db->query('SELECT tp_planned_course_status 
						 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' 
                        WHERE tp_planned_course_id = "' . $_tp_planned_course_id . '"');
	$row = $res->fetchRow();

	return $row['tp_planned_course_status'];
}

function getPlannedCourseInfo($_tp_planned_course_id) {
	global $CONFP, $db;
	$res = $db->query('SELECT *  
						 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' 
                        WHERE tp_planned_course_id = "' . $_tp_planned_course_id . '"');
	$row = $res->fetchRow();

	return $row;
}


function getPlannedUserSabreStatus($_tp_planned_course_id, $_no_staff, $_sabre_id) {
	global $CONFP, $db;

	$res = $db->query('SELECT sabre_added_status
						 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' 
                        WHERE tp_planned_course_id = "' . $_tp_planned_course_id . '"
						  and sabre_id = "' . $_sabre_id . '"
						  and no_staff = "' . $_no_staff . '"');

	$row = $res->fetchRow();

	return (int)$row['sabre_added_status'];
}

function getPassedUserSabreStatus($_tp_planned_course_id, $_no_staff, $_sabre_id) {
	global $CONFP, $db;

	$res = $db->query('SELECT sabre_passed_status
						 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' 
                        WHERE tp_planned_course_id = "' . $_tp_planned_course_id . '"
						  and sabre_id = "' . $_sabre_id . '"
						  and no_staff = "' . $_no_staff . '"');

	$row = $res->fetchRow();

	return (int)$row['sabre_passed_status'];
}

function getPlannedUserSabreData($_tp_planned_course_id, $_no_staff, $_sabre_id) {
	global $CONFP, $db;

	$res = $db->query('SELECT *
						 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' 
                        WHERE tp_planned_course_id = "' . $_tp_planned_course_id . '"
						  and sabre_id = "' . $_sabre_id . '"
						  and no_staff = "' . $_no_staff . '"');

	$row = $res->fetchRow();

	return $row;
}

function getPlannedUserSabreDataByID($_tp_planned_courses_to_sabres_id) {
	global $CONFP, $db;

	$res = $db->query('SELECT *
						 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' 
                        WHERE tp_planned_courses_to_sabres_id = "' . $_tp_planned_courses_to_sabres_id . '"');

	$row = $res->fetchRow();

	return $row;
}

/*
 * Returns logged user's ID
 *
 * @param string $id   if specified - return it
 *
 * @return string
 */
function userID($id = 0)
{
	global $C;
	return $id ? intval($id) : intval($C['user']['id_user']);
}

/*
 * Returns logged user staff #
 *
 * @param string $staffno   if specified - return it
 *
 * @return string
 */
function userStaffNo($staffno = '')
{
	global $C;
	return $staffno ? $staffno : $C['user']['no_staff'];
}

/*
 * Returns user's staff # by his ID
 *
 * @param string $id   user ID
 *
 * @return string
 */
function userStaffNoByID($id)
{
	global $db;

	static $cache = array();

	$id = intval($id);

	if ( empty($cache[$id]) )
	{
		if ( PEAR::isError($tmp = $db->queryOne('SELECT no_staff FROM ' . CONFP('TABLE_USERS_XP') . ' WHERE id_user = ' . $id . '')) ) return false;
		$cache[$id] = $tmp;
	}

	return @ $cache[$id];
}

/*
 * Checks admin permisions
 *
 * @param boolean $exit     ends application with critical error if have no permisions
 *
 * @return bool
 */
function adminAccessCheck($exit = true)
{
	global $C, $CONFP, $db;

	$t = (bool) @ $C['user']['admin'] || (! empty($CONFP['XP_TP_ADMINS_GROUP_ID']) && @ in_array($CONFP['XP_TP_ADMINS_GROUP_ID'], @ $C['groups']));

	if ( ! $t && $exit ) errorCritical('<center><br><h2>Access denied</h2><h3>Please contact the Training Department</h3></center>');

	return $t;
}

/*
 * Returns HTML of admin's area main menu DDM
 *
 * @param string $page   current page ID for mark current menu item
 *
 * @return string
 */
function ddMenuAdmin($page = '')
{
	if ( ! adminAccessCheck(false) ) return '';

	$r = '';

	if ( 'home' != $page ) $r .= '<form method="get" action="' . CONFP('HTTP_PATH') . 'admin/"><input type="submit" value="Home"></form>&nbsp;';

	$aMenu = array
	(
		'cq' => array('Crew Qualification', 'crew_qualif.php'),
		'sq' => array('Sabre Qualification', 'sabre_qualif.php'),
		'cb' => array('Course Builder', 'course_builder.php'),
		'cp' => array('Course Planner', 'course_planner.php'),
		'pc' => array('Planned Courses', 'planned_course.php'),
		'em' => array('Email Setup', 'email_setup.php'),		
//		'all' => array('All Courses', 'all_courses.php'),	
		'reports' => array('Reports', 'reports.php'),	
		'deleted_users' => array('Deleted Users Data', 'deleted_users.php'),			
	);

	$r .= '
	<select id="nav_menu">
		<option value="">Navigation Menu...</option>' . "\n";

	foreach ($aMenu as $k => $v)
	{
		$r .= '
		<option value="' . CONFP('HTTP_PATH') . 'admin/' . $v[1] . '"' . ($k == $page ? ' selected' : '') . '>' . h($v[0]) . '</option>';
	}

	$r .= '
	</select>' . "\n";

	return $r;
}

/*
 * Checks trainer permisions
 *
 * @param boolean $exit     ends application with critical error if have no permisions
 *
 * @return bool
 */
function trainerAccessCheck($exit = true)
{
	global $C, $CONFP, $db;

	$t = ! empty($CONFP['XP_TP_TRAINERS_GROUP_ID']) && @ in_array($CONFP['XP_TP_TRAINERS_GROUP_ID'], @ $C['groups']);

	if ( ! $t && $exit ) errorCritical('<center><br><h2>Access denied</h2><h3>Please contact the Training Department</h3></center>');

	return $t;
}

/*
 * Save homepage message for user
 *
 * @param $staffno   staff #
 * @param $message   message text
 *
 * @return bool
 */
function infoMessageAdd($staffno, $message)
{
	global $db;

	return ! PEAR::isError($db->query('INSERT ' . CONFP('TABLE_MESSAGES') . ' SET
		m_staffno = "' . mres($staffno) . '",
		m_text = "' . mres($message) . '",
		m_date_added = ' . TIME . ''));
}

/*
 * Show homepage message for user
 *
 * @param $staffno   staff #
 * @param $delete    delete showed messages
 *
 * @return bool
 */
function infoMessagesShow($staffno = '', $delete = true)
{
	global $db, $aInfoMessages;

	$staffno = userStaffNo($staffno);

	if ( ! PEAR::isError($rows = $db->queryAll('SELECT * FROM ' . CONFP('TABLE_MESSAGES') . '
	WHERE m_staffno = "' . mres($staffno) . '"
	ORDER BY m_date_added'))
	&&
	(! $delete || ! PEAR::isError($db->queryRow('DELETE FROM ' . CONFP('TABLE_MESSAGES') . '
	WHERE m_staffno = "' . mres($staffno) . '"'))) )
	{
		foreach ($rows as $row) $aInfoMessages[] = nl2br($row['m_text']);
	}
}

/*
 * Save homepage message for user
 *
 * @param $mail_opts   array of options form TCX sendmail function
 * @param $tpl_opts    array of template variables (var-value)
 * @param $tpl_name    path to pemplate
 *
 * @return bool
 */
function mailProj($mail_opts, $tpl_opts = array(), $tpl_name = NULL)
{
	global $db;

	if ( CONFP('EMAIL_OFF') ) return true; # OFF by config

	if ( NULL === $tpl_name ) $tpl_name = ROOT_PROJ . 'templates/inc/mail.htm';

	$sEmailTpl = '';

	if ( $tpl_name && ! ($sEmailTpl = file_get_contents($tpl_name)) )
	{
		triggerError('Load mail template failed: ' . $tpl_name, true);
	}

	if ( ! $sEmailTpl ) $sEmailTpl = '{BODY}';

	$mail_opts = array_merge(
		array
		(
			'to' => '',
			'from' => CONFP('EMAIL_FROM_DEFAULT'),
			'subject' => CONFP('PROJECT_NAME') . ' Notification',
			'body' => '',
			'html' => true,
			'addHeaders' => array('MIME-Version' => "1.0"),
		),
		forcedArray($mail_opts));

	$tpl_opts = array_merge(
		array
		(
			'{BODY}' => $mail_opts['body'],
			'{HEAD_ADDITIONAL}' => '',
			'{BODY_ADDITIONAL}' => '',
		),
		forcedArray($tpl_opts));

	$mail_opts['body'] = trim(str_replace(array_keys($tpl_opts), $tpl_opts, $sEmailTpl));

	sendmail($mail_opts);

	return false;
}


function add_month_to_date($date, $m){	
/*
	if (is_numeric($m)){
		while ($m>0){
			$date += cal_days_in_month(CAL_GREGORIAN, strftime("%m",$date), strftime("%G",$date))*24*60*60;		
			$m--;
		}		
	}
*/
	$date = strtotime('+' . $m . ' month', $date);
	
	return $date;
}

function del_month_from_date($date, $m){	
/*
	if (is_numeric($m)){
		while ($m>0){
			$date -= cal_days_in_month(CAL_GREGORIAN, strftime("%m",$date), strftime("%G",$date))*24*60*60;		
			$m--;
		}		
	}
*/

	$date = strtotime('-' . $m . ' month', $date);
	
	return $date;
}

function getDDMOptions($selected_by_default = 0, $_disabled_array = array()) {
	global $CONFP, $db;

	$res = $db->query('SELECT sabre_id, sabre_code FROM ' . $CONFP['TABLE_TP_SABRE_QUALIF'] . ' ORDER BY sabre_code');
	$sabres_ddm = '<option ' . ($selected_by_default == 0 ? 'selected' : '') . ' value="0">+</option>';

	while ( $row = $res->fetchRow() ) {
		$disabled_st = '';

			if ( in_array($row['sabre_id'], $_disabled_array) && $selected_by_default != $row['sabre_id'] ) {
				$disabled_st = 'disabled';

			}

		$sabres_ddm .= '<option ' . ($selected_by_default == $row['sabre_id'] ? 'selected' : '') . ' value="' . $row['sabre_id'] . '"' . $disabled_st . '>' . $row['sabre_code'] . '</option>';
	}

	return $sabres_ddm;
}

function get_courses_users_table($current_tp_planned_course_id = 0, $flag_all_courses = true) {
	global $CONFP, $db;

		$res = $db->query('SELECT DISTINCT p_c2s.no_staff, p_c2s.manually_added_by_trainer, CONCAT(u.surname, ", ", u.firstname) as user_name  
						 FROM tp_planned_courses pc, tp_planned_courses_to_sabres p_c2s
				    LEFT JOIN users u on u.no_staff = p_c2s.no_staff
                        WHERE p_c2s.tp_planned_course_id = "' . $current_tp_planned_course_id . '"
                          AND pc.tp_planned_course_id = p_c2s.tp_planned_course_id ' . 
						  ( !$flag_all_courses ? ' AND pc.tp_planned_course_completed = 0 ' : '' ) .
						  ' ORDER BY user_name asc');

		$courses_users_table = array();
		while ( $row = $res->fetchRow() ) {
			$courses_users_table[$row['no_staff']] = getSabresByPlannedCourse($current_tp_planned_course_id, $row['no_staff']);
			$courses_users_table[$row['no_staff']]['manually_added_by_trainer'] = $row['manually_added_by_trainer'];
		}

	return $courses_users_table;
}

function course_date_valid($course_date) {
//bool checkdate ( int $month , int $day , int $year )
	$course_date = trim($course_date);

	if ( ( !empty($course_date) ) && ( strlen($course_date)==6 ) ) {
		$course_date_day = substr($course_date, 0, 2);
		$course_date_month = substr($course_date, 2, 2);
		$course_date_year = '20' . substr($course_date, 4, 2);

		if ( checkdate ( (int)$course_date_month, (int)$course_date_day, (int)$course_date_year ) ) {
			return true;
			
//			$course_date_in_time = mktime( 0, 0, 0, (int)$course_date_month, (int)$course_date_day, (int)$course_date_year); 
//			if ( $course_date_in_time > TIME ) {
//				return true;
//			} else {
//				return false;
//			}
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function is_user_custom($_no_staff) {
	global $CONFP, $db;

	$res = $db->query('SELECT * FROM tp_planned_courses_to_sabres  
		                    WHERE sabre_id = "0" 
							  AND sabre_added_status = "0"
							  AND no_staff = "' . $_no_staff . '"');
 	if ( $res->numRows() > 0 ) {	
 		return true;
 	} else {
 		return false;
	}
}

function get_course_notes($_tp_planned_course_id) {
	global $CONFP, $db;

	$res = $db->query('SELECT tp_planned_course_notes FROM tp_planned_courses WHERE tp_planned_course_id = "' . $_tp_planned_course_id . '"');
 	$row = $res->fetchRow();
 	
 	return strip_tags($row['tp_planned_course_notes']);
}

function get_course_trainer($_tp_planned_course_id) {
	global $CONFP, $db;

	$res = $db->query('SELECT tp_planned_course_trainer FROM tp_planned_courses WHERE tp_planned_course_id = "' . $_tp_planned_course_id . '"');
 	$row = $res->fetchRow();
 	
 	return strip_tags($row['tp_planned_course_trainer']);
}

function tep_validate_email($email) {
    $valid_address = true;

    $mail_pat = '/^(.+)@(.+)$/i';
    $valid_chars = "[^] \(\)<>@,;:\.\\\"\[]";
    $atom = "$valid_chars+";
    $quoted_user='(\"[^\"]*\")';
    $word = "($atom|$quoted_user)";
    $user_pat = "/^$word(\.$word)*$/i";
    $ip_domain_pat='/^\[([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\]$/i';
    $domain_pat = "/^$atom(\.$atom)*$/i";

    if (preg_match($mail_pat, $email, $components)) {
      $user = $components[1];
      $domain = $components[2];
      // validate user
      if (preg_match($user_pat, $user)) {
        // validate domain
        if (preg_match($ip_domain_pat, $domain, $ip_components)) {
          // this is an IP address
          for ($i=1;$i<=4;$i++) {
            if ($ip_components[$i] > 255) {
              $valid_address = false;
              break;
            }
          }
        }
        else {
          // Domain is a name, not an IP
          if (preg_match($domain_pat, $domain)) {
            /* domain name seems valid, but now make sure that it ends in a valid TLD or ccTLD
               and that there's a hostname preceding the domain or country. */
            $domain_components = explode(".", $domain);
            // Make sure there's a host name preceding the domain.
            if (sizeof($domain_components) < 2) {
              $valid_address = false;
            }
          }
          else {
            $valid_address = false;
          }
        }
      }
      else {
        $valid_address = false;
      }
    }
    else {
      $valid_address = false;
    }
    if ($valid_address ) {
      if (!checkdnsrr($domain, "MX") && !checkdnsrr($domain, "A")) {
        $valid_address = false;
      }
    }
    return $valid_address;
}

function get_report_ddm_months() {
	$months_array = array();
	$months_array[] = array( 'id' => 0, 'text' => 'Select Month...' );
	$months_array[] = array( 'id' => 1, 'text' => 'January' );
	$months_array[] = array( 'id' => 2, 'text' => 'February' );
	$months_array[] = array( 'id' => 3, 'text' => 'March' );
	$months_array[] = array( 'id' => 4, 'text' => 'April' );
	$months_array[] = array( 'id' => 5, 'text' => 'May' );
	$months_array[] = array( 'id' => 6, 'text' => 'June' );
	$months_array[] = array( 'id' => 7, 'text' => 'July' );
	$months_array[] = array( 'id' => 8, 'text' => 'August' );
	$months_array[] = array( 'id' => 9, 'text' => 'September' );
	$months_array[] = array( 'id' => 10, 'text' => 'October' );
	$months_array[] = array( 'id' => 11, 'text' => 'November' );
	$months_array[] = array( 'id' => 12, 'text' => 'December' );

	return $months_array;	
}

function get_report_min_date() {
	global $CONFP, $db;
	
	$tp_min_exp_date_query = $db->query('select min(pc2s.exp_date) as min_date 
                                       from tp_planned_courses_to_sabres pc2s, tp_planned_courses pc 
									  where pc2s.exp_date > 0
									    and pc.tp_planned_course_id = pc2s.tp_planned_course_id
									    and pc.tp_planned_course_completed = 1');
	$tp_min_exp_date_item = $tp_min_exp_date_query->fetchRow();
	$min_date = $tp_min_exp_date_item['min_date'];

	return $min_date;
}

function get_report_max_date() {
	global $CONFP, $db;
	
	$tp_max_exp_date_query = $db->query('select max(pc2s.exp_date) as max_date 
                                       from tp_planned_courses_to_sabres pc2s, tp_planned_courses pc 
									  where pc2s.exp_date > 0
									    and pc.tp_planned_course_id = pc2s.tp_planned_course_id
									    and pc.tp_planned_course_completed = 1');
	$tp_max_exp_date_item = $tp_max_exp_date_query->fetchRow();
	$max_date = $tp_max_exp_date_item['max_date'];

	return $max_date;
}

function get_report_ddm_bases() {
	global $CONFP, $db;

	$tp_bases_query = $db->query('select distinct b.id_base, b.base_shortname 
								from bases b
							   where is_active = 1
							order by b.base_shortname');

	$bases_array = array();
	$bases_array[] = array( 'id' => 0, 'text' => 'All Bases' );
	while ( $tp_bases_item = $tp_bases_query->fetchRow() ) {
		$bases_array[] = array( 'id' => $tp_bases_item['id_base'], 'text' => $tp_bases_item['base_shortname'] );
	}

	return $bases_array;
}

function get_course_max_date() {
	global $CONFP, $db;
	
	$tp_max_date_query = $db->query('select max(pc.tp_planned_course_date) as max_date 
                                       from tp_planned_courses pc 
									  where pc.tp_planned_course_completed = 1');
	$tp_max_date_item = $tp_max_date_query->fetchRow();
	$max_date = $tp_max_date_item['max_date'];

	return $max_date;
}

function get_course_min_date() {
	global $CONFP, $db;
	
	$tp_min_date_query = $db->query('select min(pc.tp_planned_course_date) as min_date 
                                       from tp_planned_courses pc 
									  where pc.tp_planned_course_completed = 1');
	$tp_min_date_item = $tp_min_date_query->fetchRow();
	$min_date = $tp_min_date_item['min_date'];

	return $min_date;
}

function tep_draw_pull_down_menu($name, $values, $default = '', $parameters = '') {
	$field = '<select name="' . $name . '"';

	if ($parameters != '') $field .= ' ' . $parameters;

	$field .= '>';

	if (empty($default) && isset($GLOBALS[$name])) $default = stripslashes($GLOBALS[$name]);

	for ($i=0, $n=sizeof($values); $i<$n; $i++) {
		$field .= '<option value="' . $values[$i]['id'] . '"';
		if ($default == $values[$i]['id']) {
	        $field .= ' SELECTED';
		}

    	$field .= '>' . $values[$i]['text'] . '</option>';
	}
	
	$field .= '</select>';

	return $field;
}

function get_max_passed_date_of_course($_tp_planned_course_id) {
	global $CONFP, $db;
	
	$tp_max_date_query = $db->query('select max(passed_date) as max_date 
                                       from tp_planned_courses_to_sabres  
									  where tp_planned_course_id = ' . (int)$_tp_planned_course_id);
	$tp_max_date_item = $tp_max_date_query->fetchRow();
	$max_date = $tp_max_date_item['max_date'];

	return $max_date;
}

function get_min_passed_date_of_course($_tp_planned_course_id) {
	global $CONFP, $db;
	
	$tp_min_date_query = $db->query('select min(passed_date) as min_date 
                                       from tp_planned_courses_to_sabres  
									  where tp_planned_course_id = ' . (int)$_tp_planned_course_id);
	$tp_min_date_item = $tp_min_date_query->fetchRow();
	$min_date = $tp_min_date_item['min_date'];

	return $min_date;
}

function getDeletedUsersList() {
	global $CONFP, $db;

	$tmp_query = $db->query('select distinct pc2s.no_staff, concat(pc2s.firstname, " ", pc2s.surname) as user_name 
                               from tp_planned_courses_to_sabres pc2s
							  where pc2s.no_staff not in (select no_staff from users)
								order by user_name asc');
	
	$result_array = array();
	while ( $tmp_item = $tmp_query->fetchRow() ) {
		$result_array[] = array(
			'no_staff' => $tmp_item['no_staff'],
			'user_name' => ( empty($tmp_item['user_name']) ? 'not detected' : $tmp_item['user_name'] )
		);
	}
	
	return $result_array;
}

function checkSabreExistsForUserInCourse($_tp_planned_course_id, $_sabre_id, $_no_staff) {
	global $db;
	
	$res = $db->query('SELECT *  
						 FROM tp_planned_courses_to_sabres 
                        WHERE tp_planned_course_id = "' . (int)$_tp_planned_course_id . '"
						  AND sabre_id = "' . (int)$_sabre_id . '"
						  AND no_staff = "' . addslashes($_no_staff) . '"');
	if ( $res->numRows() > 0 ) {
		return true;
	} else {
		return false;
	}
}
?>
