<?php

$CONFP = array();

$CONFP['REV'] = 1;

$CONFP['PROJECT_NAME'] = 'Training Planner';

$CONFP['REL_PATH_XPLORER'] = '../../';

$CONFP['HTTP_PATH'] = '/external/tp/';

$CONFP['TMP_FOLDER_PATH'] = '/var/www/vhosts/tcxplorer.com/httpsdocs/tmp/';

$CONFP['TP_SITE_URL'] =  'http' . ( $_SERVER['HTTPS'] =='on' ? 's' : '' ) . '://' . $_SERVER['HTTP_HOST'] . $CONFP['HTTP_PATH'];

# -------------------------

$CONFP['TABLE_CONFIG'] = 'tp_config';
$CONFP['TABLE_DIC'] = 'tp_dic';

$CONFP['TABLE_CREW_QUALIF'] = 'tp_crew_qualif';
$CONFP['TABLE_TP_COURSES'] = 'tp_courses';
$CONFP['TABLE_TP_SABRE_QUALIF'] = 'tp_sabre_qualif';
$CONFP['TABLE_TP_CREWS_TO_SABRES'] = 'tp_crews_to_sabres';
$CONFP['TABLE_TP_SABRES_TO_COURSES'] = 'tp_sabres_to_courses';
$CONFP['TABLE_TP_PLANNED_COURSES'] = 'tp_planned_courses';
$CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] = 'tp_planned_courses_to_sabres';

$CONFP['TABLE_USERS_XP'] = 'users';
$CONFP['TABLE_USERS_BASES'] = 'users_bases';
$CONFP['TABLE_USERS_FLEETS'] = 'users_fleets';
$CONFP['TABLE_USERS_RANKS'] = 'users_ranks';
$CONFP['TABLE_BASES'] = 'bases';
$CONFP['TABLE_FLEETS'] = 'fleets';
$CONFP['TABLE_RANKS'] = 'ranks';

# -------------------------

$CONFP['EMAIL_ADMIN'] = 'anstream@atwix.com';
$CONFP['EMAIL_FROM_DEFAULT'] = 'anstream@atwix.com';

$CONFP['EMAIL_DEVELOPER'] = 'firster@atwix.com';
$CONFP['PATHF_ERR_LOG_DEV'] = ROOT_PROJ . 'logs/error.log';

$CONFP['DATE_FORMAT'] = 'd/m/y';
$CONFP['DATE_FORMAT_LONG'] = 'F d, Y';
$CONFP['DATE_FORMAT_INPUT'] = 'dmy';

$CONFP['XPLORER_BID_ADMIN_GROUP_ID'] = 0;

$CONFP['SERVICE_SEC_KEY'] = '6HqTJnD\xfmb;>q]zw}D';
$CONFP['CLS_SERVICE_SEC_KEY'] = 'T(BD^(3=`O]{A7qSe+jemp@[3[#d<6pWD;ZT+3"e';

$CONFP['TRAINER_MARKS'] = array
(
	'pass' => 1,
	'pass-retake' => 2,
	'fail' => 3,
	'complete' => 4,
	'not-complete' => 5,
	'not-attended' => 6
);

$CONFP['SABRE_RANKS'] = array();
$CONFP['SABRE_RANKS'][0] = 'Both';
$CONFP['SABRE_RANKS'][1] = 'Pilot';
$CONFP['SABRE_RANKS'][2] = 'Crew';

//$CONFP['RANKS_PILOTS'] = array('Captain', 'Reserve Captain', 'First Officer', 'TRE/TRI');
//$CONFP['RANKS_CREWS'] = array('CM', 'CM Shorthaul Only', 'Dual', 'Dual Shorthaul Only', 'C3', 'C3 Shorthaul Only');
$CONFP['RANKS_PILOTS'] = array(1, 2, 13, 16);
$CONFP['RANKS_CREWS'] = array(3, 4, 5, 6, 7, 8);

$CONFP['COUNT_COLUMNS'] = 15;
$CONFP['TEXT_CONSTANTS'] = array
(
	'IMG_TITLE_USERS_MODULE' => 'Module is applicable to this user',
	'IMG_TITLE_USERS_ADDITIONAL_MODULE' => 'Module is applicable to this user as additional',
	'IMG_TITLE_NOT_USERS_MODULE' => 'Module not applicable to this crew member',
	'TABLE_HEADER_REQUIREMENTS' => 'REQUIREMENTS',
	'TABLE_HEADER_DATE' => 'DATE'
);

$CONFP['PDF_FOOTER_TEXT'] = '<hr size="1">' . 
							'<br>' . 
							'All training has been completed in accordance with the relevant parts of EASA IR & EU-Ops requirements as detailed in the Thomas Cook Airlines Ltd Gen Ops (Part A), Safety & Survival (Part B1) & Training (Part D1/2) Manuals';
?>
