<?php

/*
 * alias of PHP htmlspecialchars() using site encoding (if it declared in constant)
 */
function h($s)
{
	if ( version_compare(PHP_VERSION, '4.3', '>=') && defined('ENCODING_MAIN') )
	{
		return htmlspecialchars($s, ENT_QUOTES, ENCODING_MAIN);
	}
	return htmlspecialchars($s, ENT_QUOTES);
}

function nh($s)
{
	return nl2br(h($s));
}

/*
 * Escape function for correctly put string in JS variant 1
 */
function j($s)
{
	return str_replace(
		array("\\",   "'",  '"',   "\r\n",    "\n"),
		array("\\\\", "\'", '\"',  "\\n\\\r", "\\n\\\n"),
		$s
	);
}

/*
 * Escape function for correctly put string in JS variant 2
 */
function jh($s)
{
	return str_replace(
		array("\\",   "'",  '"',      "\r\n",    "\n"),
		array("\\\\", "\'", '&quot;', "\\n\\\r", "\\n\\\n"),
		$s
	);
}

/*
 * alias of PHP mysql_real_escape_string()
 */
function mres($s)
{
	if ( function_exists('mysql_real_escape_string') )
	{
		return mysql_real_escape_string($s);
	}
	return mysql_escape_string($s);
}

/*
 * Escapes special charachters in MySQL LIKE structure [_%]
 * Can also escape MySQL special charachters
 */
function mresLike($s, $escape = true)
{
	$sReturn = $s;

	$b = '\\'; # escape-symbol

	# "backslash" escaping
	$sReturn = str_replace('\\', $b.'\\', $sReturn);

	# wildcards escaping
	$sReturn = str_replace(
		array (   '_',    '%'),
		array ($b.'_', $b.'%'),
		$sReturn
	);

	# usual escaping
	if ( $escape ) $sReturn = mres($sReturn);

	return $sReturn;
}

/*
 * Smart stripslashes() if magic_quotes activated
 */
function ss($arg)
{
	if ( get_magic_quotes_gpc() || ini_get('magic_quotes_sybase') )
	{
		if ( is_array($arg) )
		{
			foreach ($arg as $k => $v) $arg[$k] = ss($v);
			return $arg;
		}
		else return stripslashes($arg);
	}
	return $arg;
}

/*
 * Returns number with specified precision and trimmed ending zeros
 *
 * @param mixed $f        source value
 * @param integer $prec   precision
 *
 * @return string
 *
 * Examples:
 *   floatTrimmed(15.27980) returns 15.2798
 *   floatTrimmed(15.27980, 3) returns 15.28
 *   floatTrimmed(15.0) returns 15
 */
function floatTrimmed($f, $prec = false)
{
	$f = floatval($f);
	if ( is_numeric($prec) )
	{
		$f = number_format($f, intval($prec), '.', '');
		$f = rtrim(rtrim($f, '0'), '.,');
	}
	return $f;
}

/*
 * Returns forced converted to integer array values
 *
 * @param mixed $id         source value, can be array, array of array
 * @param boolean $allow0   if FALSE - returns non-0 values only
 *
 * @return linear array
 */
function intArray($id = 0, $allow0 = false, $abs = false)
{
	$arr = array();
	if ( is_array($id) ) foreach ($id as $v) $arr = array_merge($arr, intArray($v, $allow0));
	elseif ( ($tmp = intval($id)) || $allow0 ) $arr[] = $abs ? abs($tmp) : $tmp;
	return $arr;
}

/*
 * Returns forced converted to strings array values
 *
 * @param mixed $id              source value, can be array, array of array
 * @param boolean $allow_empty   if FALSE - returns non-empty values only
 *
 * @return linear array
 */
function stringArray($s = 0, $allow_empty = false)
{
	$arr = array();
	if ( is_array($s) ) foreach ($s as $v) $arr = array_merge($arr, stringArray($v));
	elseif ( ($tmp = strval($s)) || $allow_empty ) $arr[] = $tmp;
	return $arr;
}

/*
 * Returns empty array if argument is not an array
 *
 * @param mixed $a   source value
 *
 * @return array
 */
function forcedArray($a)
{
	return is_array($a) ? $a : array();
}

/*
 * Remove array keys prefix
 *
 * @param array $arr    source array
 * @params all next - will be used as prefixes that needs to be removed (could be array(s))
 *
 * @return array
 */
function arrayKeysRemovePfx($arr)
{
	$r = array();
	$pfxs = func_get_args();
	array_shift($pfxs);
	$pfxs = stringArray($pfxs);
	foreach ($arr as $k => $v)
	{
		$key = $k;
		foreach ($pfxs as $pfx)
		{
			$key = preg_replace('~^' . preg_quote($pfx) . '~', '', $k);
			if ( $key != $k ) break; # first replace ends loop
		}
		$r[$key] = $v;
	}
	return $r;
}

/*
 * Converts array items to specific data types
 *
 * @param array $a             source-array
 * @param array $keys          key-datatype pairs array
 * @param array $default_type  default type for conversion if empty one given for some key
 * @param boolean $forced      creates key in result-array if it even is not exists in source-array
 *
 * @return array
 */
function typedArray($a, $keys, $default_type = 'trim', $forced = false)
{
	$arr = array();
	if ( is_array($a) && is_array($keys) )
	foreach ($keys as $k => $type)
	{
		if ( ! $forced && ! isset($a[$k]) ) continue;
		if ( ! $type ) $type = $default_type;

		$val = @ $a[$k];
		switch ( $type )
		{
			case 'int': $val = intval($val); break;
			case 'absint': $val = abs(intval($val)); break;
			case 'float': $val = floatval($val); break;
			case 'absfloat': $val = abs(floatval($val)); break;
			case 'bool': $val = (bool) $val; break;
			case 'boolint': $val = $val ? 1 : 0; break;
			case 'str': $val = strval($val); break;
			case 'trim': $val = trim(strval($val)); break;
		}
		$arr[$k] = $val;
	}
	return $arr;
}

/*
 * Returns TRUE if date is empty or equals to '0000-00-00' and FALSE otherwise
 *
 * @param string $date   source value
 *
 * @return boolean
 */
function dateEmpty($date)
{
	$date = trim($date);
	return empty($date) || '0000-00-00' == $date;
}

/*
 * Returns float representation of timestamp plus microseconds
 */
function fmicrotime()
{
	list($msec,$sec) = explode(' ', microtime());
	return $sec + $msec;
}

/*
 * Using JSON encoding 3rd party lib for PHP versions without json_encode() function
 */
if ( ! function_exists('json_encode') )
{
	function json_encode($m)
	{
		require_once dirname(__FILE__) . '/json.php';
		$json = new Services_JSON();

		return $json->encode($m);
	}
}

/*
 * Using JSON decoding 3rd party lib for PHP versions without json_decode() function
 */
if ( ! function_exists('json_decode') )
{
	function json_decode($m)
	{
		require_once dirname(__FILE__) . '/json.php';
		$json = new Services_JSON();

		return $json->decode($m);
	}
}

/*
 *
 */
function endingS($d)
{
	$d = intval($d);
	return $d > 1 || $d < 1 ? 's' : '';
}

/*
 * Removes placeholder content from a var
 */
function placeholderClean(& $var, $phldr)
{
	if ( ! $phldr ) return;
	if ( $var == $phldr ) $var = '';
}

/*
 * Cleans intended for user success messages
 */
function successClear()
{
	global $aOK;
	$aOK = array();
}

/*
 * Adds message to intended for user success messages list
 */
function successToPrint($s = '')
{
	global $aOK;
	if ( $s = trim($s) ) $aOK[] = $s;
}

/*
 * Cleans intended for user info messages
 */
function infoMsgsClear()
{
	global $aInfoMessages;
	$aInfoMessages = array();
}

/*
 * Adds message to intended for user info messages list
 */
function infoToPrint($s = '')
{
	global $aInfoMessages;
	if ( $s = trim($s) ) $aInfoMessages[] = $s;
}

/*
 * Cleans intended for user error messages
 */
function errorsClear()
{
	global $aErrors;
	$aErrors = array();
}

/*
 * Adds message to intended for user error messages list
 */
function errorToPrint($s = '')
{
	global $aErrors;
	if ( $s = trim($s) ) $aErrors[] = $s;
}

/*
 * Logging message to the developer's log file
 */
function errorLog($log_message)
{
	if ( !($log_message = trim($log_message)) ) return ;

	if ( !($f = @fopen(CONFP('PATHF_ERR_LOG_DEV'), 'a')) )
	{
		errorToDeveloper('Can not open developer log file '. CONFP('PATHF_ERR_LOG_DEV') .' for writing.');
	}
	else
	{
		flock($f, LOCK_EX);
		fwrite($f, $log_message."\n");
		flock($f, LOCK_UN);
		fclose($f);
		chmod(CONFP('PATHF_ERR_LOG_DEV'), 0777);
	}
}

/*
 * Sends message to developer's email
 */
function errorToDeveloper($body, $err = null)
{
	if ( !($body = trim($body)) ) return ;

	if ( is_array($body) || PEAR::isError($body) ) $body = print_r($body, true);
	if ( is_array($err) || PEAR::isError($err) ) $body .= "\n\n". print_r($err, true);

	/*sendmail(array
	(
		'to' => CONFP('EMAIL_DEVELOPER'),
		'subject' => PROJECT_NAME . ': site work error',
		'body' => $body,
	));
	*/

	mail(CONFP('EMAIL_DEVELOPER'), PROJECT_NAME . ': site work error', $body);
}

/*
 * Triggers developer error
 */
function triggerError($errstr, $errno = '', $errfile = '', $errline = '')
{
	if ( true === $errno ) $errno = E_USER_ERROR;
	if ( ! $errno ) $errno = E_USER_WARNING;
	
	if ( $errno & @E_STRICT ) # interoperability and forward compatibility of code
	{
		return ;
	}

	if ( ! $errfile || ! $errline )
	{
		$debug = debug_backtrace();
		$errfile = $debug[0]['file'];
		$errline = $debug[0]['line'];
	}

	if ( $errno & (E_USER_ERROR | E_USER_WARNING | E_USER_NOTICE) )
	{
		$log_message = date('Y-m-d H:i:s') .' '. (E_USER_ERROR == $errno ? 'ERROR: ' : '') . $errstr .' in '. $errfile .' on line '. $errline;

		errorLog($log_message);

		if ( E_USER_ERROR == $errno )
		{
			errorToDeveloper($log_message);
		}

		return ;
	}
}

/*
 * Returns current script alias (path)
 * Examples: asm_view, admin/reports, trainer/stat
*/
function scriptAlias()
{
	$alias = preg_replace('~^' . preg_quote(ROOT_PROJ) . '~', '', $_SERVER['SCRIPT_FILENAME'], 1);
	$alias = preg_replace('~\.php$~', '', $alias, 1);

	return $alias;
}

/*
 * Adds info to the HTML <HEAD> element
 */
function headAppend($s)
{
	global $TPL;
	@ $TPL['headAdditional'] .= $s . "\n";
}

/*
 * Includes JS file to the HTML <HEAD> element
 *
 * @param string $sSrc     JS file path
 * @param boolean $bOnce   if TRUE function works like PHP include_once
 */
function includeJS($sSrc = '', $bOnce = true)
{
	static $included = array();

	if ( !$sSrc )
	{
		$tmp = 'js/' . scriptAlias() . '.js';
		if ( is_file(ROOT_PROJ . $tmp)  ) $sSrc = $tmp;
	}

	if ( !$sSrc ) return;

	if ( !$bOnce || !isset($included[$sSrc]) )
	{
		$included[$sSrc] = true;
		headAppend('<script type="text/javascript" src="'. h($sSrc) .'"></script>');
	}
}

/*
 * Includes CSS file to the HTML <HEAD> element
 *
 * @param string $sSrc     CSS file path
 * @param boolean $bOnce   if TRUE function works like PHP include_once
 */
function includeCSS($sHref, $bOnce = true)
{
	static $included = array();

	if ( !$sHref )
	{
		$tmp = 'css/' . scriptAlias() . '.js';
		if ( is_file(ROOT_PROJ . $tmp)  ) $sHref = $tmp;
	}

	if ( !$sHref ) return;

	if ( !$bOnce || !isset($included[$sHref]) )
	{
		$included[$sHref] = true;
		headAppend('<link type="text/css" rel="stylesheet" media="screen,projection,print" href="'. h($sHref) .'">');
	}
}

/*
 * Assigns script variable for using in template
 *
 * @param string $var_name   variable's name (with this name it will be available in template)
 * @param boolean $val       variable's value
 */
function pageAssignVar($var_name, $val = null)
{
	global $TPL;
	$TPL[$var_name] = $val;
}

/*
 * Assigns script variables from array for using in template
 *
 * @param array $arr   keys-values array
 */
function pageAssignVarsArray($arr)
{
	foreach ($arr as $k => $v) pageAssignVar($k, $v);
}

/*
 * Automatically assigns script variables from GLOBALS for using in template
 *
 * @params string unlimited variables names
 */
function pageAutoAssignVars()
{
	global $TPL;

	$args = func_get_args();
	$args = stringArray($args);

	foreach ($args as $var_name)
		if ( is_string($var_name) )
		{
			if ( ! isset($GLOBALS[$var_name]) ) $GLOBALS[$var_name] = NULL;
			$TPL[$var_name] = & $GLOBALS[$var_name];
		}
}

/*
 * Processing and displays template
 *
 * @param string $tpl_name   template name (default equals to script alias)
 *
 * Includes template from templates_folder/{$tpl_name}.tpl
 */
function pageDisplay($tpl_name = '', $display = true)
{
	global $CONFP, $TPL;

	if ( ! $tpl_name ) $tpl_name = scriptAlias();
	$tpl_fn = PATH_TEMPLATES . $tpl_name . '.tpl';

	if ( ! is_file($tpl_fn) )
	{
		$debug = debug_backtrace();
		triggerError('Template ' . $tpl_fn .' (called in ' . $debug[0]['file'] . ':' . $debug[0]['line'] . ') is not exists.');
		echo 'Page template is not exists.';
		exit;
	}

	extract($TPL, EXTR_REFS); # unwraps assigned to template variables

	ob_start();
	require $tpl_fn;
	$output = ob_get_clean();
	if ( $display )
	{
		require_once PATH_TEMPLATES . 'inc/_header.tpl';
		echo $output;
		require_once PATH_TEMPLATES . 'inc/_footer.tpl';
	}
	return $output;
}

/*
 * Displays ctitical messages end ends application without showing template
 *
 * @param string $content   message content
 * @param string $error     if specified - displays popup error message
 */
function errorCritical($content, $error = '')
{
	errorsClear();
	pageAssignVar('content', $content);
	if ( $error ) errorToPrint($error);
	pageDisplay('inc/_empty');
	exit;
}

/*
 * Returns configuration value
 *
 * @param string $name        configuration name
 * @param string $data_type   data type to convert returning value [str, int, float, bool]
 *
 * @return mixed
 */
function CONFP($name, $data_type = '')
{
	global $CONFP;

	$name = trim($name);
	$val = isset($CONFP[$name]) ? $CONFP[$name] : null;
	switch ( $data_type )
	{
		case 'str': $val = strval($val);
		case 'int': $val = intval($val); break;
		case 'absint': $val = abs(intval($val)); break;
		case 'float': $val = floatval($val); break;
		case 'arr': $val = forcedArray($val); break;
		case 'bool': $val = (bool) $val; break;
	}
	return $val;
}

/*
 * Loads all configurations stored in database
 */
function confDbLoad()
{
	global $CONFP, $db;

	if ( isset($CONFP['DBCONF']) ) return;

	$CONFP['DBCONF'] = array();
	if ( ! PEAR::isError($tmp = $db->queryAll('SELECT conf_name, conf_value FROM ' . CONFP('TABLE_CONFIG') . '', null, MDB2_FETCHMODE_ASSOC, true)) )
	{
		$CONFP['DBCONF'] = $tmp;
	}
}

/*
 * Returns configuration value stored in database
 *
 * @param string $name        configuration name
 * @param string $data_type   data type to convert returning value [int, float, bool, obj]
 *
 * @return mixed (default string)
 */
function confDb($name, $data_type = '')
{
	global $CONFP;

	confDbLoad();

	$name = trim($name);
	$val = isset($CONFP['DBCONF'][$name]) ? $CONFP['DBCONF'][$name] : null;
	switch ( $data_type )
	{
		case 'int': $val = intval($val); break;
		case 'absint': $val = abs(intval($val)); break;
		case 'float': $val = floatval($val); break;
		case 'bool': $val = (bool) $val; break;
		case 'obj': $val = unserialize(strval($val)); break;
		default: $val = strval($val);
	}
	return $val;
}

/*
 * Save configuration value to database
 *
 * @param string $name        configuration name
 * @param string $val         configuration value
 * @param string $data_type   data type to convert saving value [int, float, bool, obj]
 */
function confDbSave($name, $val, $data_type = '')
{
	global $CONFP, $db;

	$name = trim($name);
	switch ( $data_type )
	{
		case 'bool':
		case 'int': $val = intval($val); break;
		case 'absint': $val = abs(intval($val)); break;
		case 'float': $val = floatval($val); break;
		case 'obj': $val = serialize($val); break;
		default: $val = strval($val);
	}
	$db->query('REPLACE ' . CONFP('TABLE_CONFIG') . ' SET conf_name = "' . mres($name) . '", conf_value = "' . mres($val) . '"');
	$CONFP['DBCONF'][$name] = $val;

	return $val;
}

/*
 * Returns configuration dictionary (values array) from config file
 *
 * @param string $list_name   configuration name
 * @param string $true_only   returns only items with numeric keys
 *
 * @return array
 *
 * Examples:
 *   dicList('YN') returns array(1 => 'Y', 2 => 'N')
 *   dicList('T2_GRADES_CUSTOM') returns
        array
        (
            1 => 'Engine',
            2 => 'Pilot/Static System',
            3 => 'Electrical System',
            ...
            15 => 'Slat & Flap System',
            17 => 'Fire Drills',
            ...
        )
 */
function dicList($list_name, $true_only = false)
{
	global $CONFP;

	$arr = array();
	
	if ( isset($CONFP[$list_name]) ) $arr = $CONFP[$list_name];

	if ( $true_only ) foreach ($arr as $k => $v) if ( 0 === strpos($k, '~') ) unset($arr[$k]);

	return $arr;
}

/*
 * Returns configuration dictionary item value by its ID from config file
 *
 * @param string $list_name   configuration name
 * @param string $val         dictionary ID
 * @param string $old         if TRUE - also searching in {$list_name}_OLD dictionary (for deleted items)
 *
 * @return mixed
 *
 * Examples:
 *   dicListItem('T2_TYPES', 2) returns 'Annual Line Check'
 */
function dicListItem($list_name, $val, $old = true)
{
	global $CONFP;

	if ( 0 !== strpos($val, '~') )
	{
		if ( isset($CONFP[$list_name]) && isset($CONFP[$list_name][$val]) ) return $CONFP[$list_name][$val];
		if ( isset($CONFP[$list_name . '_OLD']) && isset($CONFP[$list_name . '_OLD'][$val]) ) return $CONFP[$list_name . '_OLD'][$val];
	}

	return NULL;
}

/*
 * Returns first item value from config file
 *
 * @param string $list_name   configuration name
 *
 * @return mixed
 */
function dicListFirst($list_name)
{
	global $CONFP;

	$arr = array();
	if ( isset($CONFP[$list_name]) ) $arr = $CONFP[$list_name];
	foreach ($arr as $k => $v) if ( 0 === strpos($k, '~') ) unset($arr[$k]);

	return array_shift($arr);
}

/*
 * Save configuration dictionary items to DB (for using in queries)
 *
 * @param string $list_name   configuration name
 *
 * @return NULL
 *
 */
function dicDbSave($list_name)
{
	global $db;

	$args = func_get_args();
	$args = stringArray($args);

	foreach ($args as $list_name)
	foreach (dicList($list_name) as $k => $v)
	{
		if ( is_array($v) ) $v = serialize($v);
		$db->query('REPLACE ' . CONFP('TABLE_DIC') . ' SET
			dic_name = "' . mres($list_name) . '",
			dic_id = "' . mres($k) . '",
			dic_val = "' . mres($v) . '"');
	}

	return NULL;
}

?>