<?php

$_aErrorHandlersStack = array();

/*
 * Database errors triggering mode when nothing happens with database errors
 *
 */
function dbErrorsReturn()
{
	dbSetErrorHandling('return');
}
/*
 * Database errors triggering mode when errors are stored to log file
 */
function dbErrorsTrigger()
{
	dbSetErrorHandling('trigger');
}
/*
 * Database errors triggering mode when errors are sent to developer (also stored to log file)
 *
 */
function dbErrorsMail()
{
	dbSetErrorHandling('mail');
}

/*
 * Sets previous database errors triggering mode
 *
 */
function dbErrorsPrev()
{
	global $_aErrorHandlersStack;
	$action = array_pop($_aErrorHandlersStack);
	dbSetErrorHandling($action);
}

dbErrorsTrigger(); # default triggering mode

function dbErrorHandler($err, $errno = E_USER_NOTICE)
{
	global $db;

	if ( ! PEAR::isError($err) ) return ;

	$endtrack = array();
	foreach ($err->backtrace as $trace)
	{
		if ( 0 == strcasecmp(@$trace['class'], 'MDB2_Driver_Common') ) $endtrack = $trace;
	}

	triggerError($err->userinfo, $errno, @$endtrack['file'], @$endtrack['line']);
}

/*
 * Called when database errors triggering mode sets to saving
 *
 */
function dbErrorHandlerTrigger($err)
{
	dbErrorHandler($err);
}

/*
 * Called when database errors triggering mode sets to mailing
 *
 */
function dbErrorHandlerMail($err)
{
	dbErrorHandler($err, E_USER_ERROR);
}

/*
 * Sets database errors triggering mode
 *
 */
function dbSetErrorHandling($action)
{
	global $db, $_aErrorHandlersStack;

	if ( ! in_array($action, array('mail','return','trigger')) ) return;

	switch ( $action )
	{
		case 'mail':
			$db->setErrorHandling(PEAR_ERROR_CALLBACK, 'dbErrorHandlerMail');
			break;

		case 'return':
			$db->setErrorHandling(PEAR_ERROR_RETURN, null);
			break;

		case 'trigger':
			$db->setErrorHandling(PEAR_ERROR_CALLBACK, 'dbErrorHandlerTrigger');
			break;
	}

	array_push($_aErrorHandlersStack, $action);
}

/*
 * Returns ready "key = value, key = value" MySQL query SET part
 *
 * @param array $a   source array
 *
 * @return string (all values are properly-escaped)
 */
function dbSetFields($a)
{
	global $db;

	$r = '';
	if ( is_array($a) )
	foreach ($a as $k => $v) $r .= $k . ' = ' . $db->quote($v) . ', ';
	if ( $r ) $r = substr($r, 0, -2);
	return $r;
}

?>