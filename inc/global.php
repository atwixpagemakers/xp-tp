<?php

error_reporting(E_ALL);
ini_set('display_errors', 0);

define('DEV_MODE', isset($_COOKIE['dev_mode']));
define('ROOT_PROJ', realpath(dirname(__FILE__) . '/../') . '/');

if ( DEV_MODE )
{
	ini_set('display_errors', 1);
}

require_once ROOT_PROJ . 'inc/config.php';
require_once ROOT_PROJ . $CONFP['REL_PATH_XPLORER'] . 'inc/global.php';
require_once ROOT_PROJ . 'inc/config_xplorer.php';

if ( empty($NO_LOGIN_NEED) && ! PROJ_INCLUDED ) isLogged();

require_once ROOT_PROJ . 'inc/functions.php';
require_once ROOT_PROJ . 'inc/database.php';
require_once ROOT_PROJ . 'inc/project.php';

# ===================================================

define('TIME', time());

define('PROT_STRING', 'http'. (getenv('HTTPS')=='on'?'s':'') .'://');
define('HTTP_ROOT', PROT_STRING . $_SERVER['HTTP_HOST']);

define('PATH_TEMPLATES', ROOT_PROJ . 'templates' . '/');

$errors_caught = array();
$aErrors = array();
$aOK = array();
$aInfoMessages = array();
$TPL = array();

set_error_handler('errorHandlerProj');

pageAssignVar('basePath', HTTP_ROOT . $CONFP['HTTP_PATH']);
pageAssignVar('pageTitle', $CONFP['PROJECT_NAME']);
pageAssignVar('headAdditional', '');
pageAutoAssignVars('G', 'P', 'C', 'S', 'E', 'aOK', 'aErrors', 'aInfoMessages');

dbErrorsMail();
#dbErrorsTrigger();

crewQualifGet();

?>