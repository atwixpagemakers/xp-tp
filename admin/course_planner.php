<?php

require_once '../inc/global.php';

adminAccessCheck();

pageTitle('Course Planner');

$action = $_GET['action'];

$flag_clear_remove_array = true;

switch ( $action )  {
	case 'new_course_upload':
		// check date and check file
		//var_dump(course_date_valid('280112'));
//		if ( trim($_POST['planned_course_date']) != '' ) {

		$flag_error = false;
		
		if ( course_date_valid($_POST['planned_course_date']) == true ) {
			$_SESSION['planned_course_date'] = trim($_POST['planned_course_date']);
			$_SESSION['planned_course_notes'] = trim($_POST['planned_course_notes']);
			$_SESSION['planned_course_trainer'] = trim($_POST['planned_course_trainer']);
		} else {
			//errorToPrint('The date of course was not defined!');
			errorToPrint('Please check date.');
			$flag_error = true;
		}

		$_planned_course_single_user = trim($_POST['planned_course_single_user']);
		$csv_data = array();
		$st_no_staff = '';
		$_SESSION['CSV_UPLOADED_USERS'] = array();

		if ( $_FILES['planned_course_csv']['name']!='' && !$flag_error ) {
			$upfile = $CONFP['TMP_FOLDER_PATH'] . $_FILES['planned_course_csv']['name'];

			if ( move_uploaded_file($_FILES['planned_course_csv']['tmp_name'], $upfile) ) {
				$fd = fopen($upfile, "r");    

				while ( !feof($fd) ) {
 	    			$csv_line = trim( fgets( $fd, 1024 ) );
					
					if ( !empty( $csv_line ) ) {
						$buf = explode(',', $csv_line); 
						
						if ( isset($buf[0]) ) $buf[0] = trim($buf[0]);
						if ( isset($buf[1]) ) $buf[1] = trim($buf[1]);

						if ( !preg_match('(^[a-zA-Z0-9]{1,}$)', $buf[1]) || count($buf) > 2 ) {
							errorToPrint('Check CSV file. Wrong format.');
							
							$_SESSION['CSV_UPLOADED_USERS'] = array();
							$flag_error = true;
							break;
						} 

						$csv_data[] = $buf;
											
						$st_no_staff .= '"' . $buf[0] . '",'; // user's staff number

						$_SESSION['CSV_UPLOADED_USERS'][] = $buf[0];
					}
				}
				fclose ($fd);	
				unlink($upfile);

			} else {
				errorToPrint('File was not uploaded sucessfully!');
			}
		} elseif ( !empty($_planned_course_single_user) ) {
			$buf = explode(',', $_planned_course_single_user); 
		
			if ( isset($buf[0]) ) $buf[0] = trim($buf[0]);
			if ( isset($buf[1]) ) $buf[1] = trim($buf[1]);

			if ( !preg_match('(^[a-zA-Z0-9]{1,}$)', $buf[1]) || count($buf) > 2 ) {
				errorToPrint('Check single user data. Wrong format.');
						
				$_SESSION['CSV_UPLOADED_USERS'] = array();
				$flag_error = true;
			} else {
				$csv_data[] = $buf;
											
				$st_no_staff .= '"' . $buf[0] . '",'; // user's staff number

				$_SESSION['CSV_UPLOADED_USERS'][] = $buf[0];
			}
		} else {
			$flag_error = true;
			
			errorToPrint('No data to input!');
		}
		
		$st_no_staff = trim($st_no_staff);
		if ( !empty($st_no_staff) ) {
			$st_no_staff = substr($st_no_staff, 0, -1);
		} else {
			$flag_error = true;
			
			errorToPrint('No data to input!');
		}
		
		if ( !$flag_error ) {
					

					// SORT order users array
					/*
					$res = $db->query('SELECT DISTINCT no_staff, CONCAT(surname, ", ", firstname) as user_name, surname, firstname, rank     
						 		   FROM users 
                                   WHERE no_staff in (' . $st_no_staff . ')
					 			   ORDER BY user_name asc');
					*/
					
					$res = $db->query('SELECT DISTINCT u.no_staff, CONCAT(u.surname, ", ", u.firstname) as user_name, u.surname, u.firstname, r.id_rank as rank        
						 		   FROM users u, users_ranks ur, ranks r  
                                   WHERE u.no_staff in (' . $st_no_staff . ')
								     AND ur.id_rank = r.id_rank
								     AND ur.id_user = u.id_user');

					$_SESSION['UPLOADED_USERS'] = array();				
					while ( $row = $res->fetchRow() ) {
						foreach ( $csv_data as $key => $csv_item ) {
							if ( $row['no_staff'] == $csv_item[0] ) {
								$_SESSION['UPLOADED_USERS'][$csv_item[0]] = $csv_item;
								$_SESSION['UPLOADED_USERS'][$csv_item[0]]['firstname'] = $row['firstname'];
								$_SESSION['UPLOADED_USERS'][$csv_item[0]]['surname'] = $row['surname'];
								$_SESSION['UPLOADED_USERS'][$csv_item[0]]['rank'] = $row['rank'];
//								$_SESSION['UPLOADED_USERS'][$csv_item[0]]['deleted'] = 0;
								break;
							}
						}	
					}

					header('Location: ' . $_SERVER['PHP_SELF'] . '?success=uploaded');
		}
		
	break;

	case 'cancel_course_upload':
		$_SESSION['UPLOADED_USERS'] = array();
		$_SESSION['CSV_UPLOADED_USERS'] = array();
		$_SESSION['planned_course_date'] = '';
		$_SESSION['planned_course_notes'] = '';
		$_SESSION['planned_course_trainer'] = '';
		
		header('Location: ' . $_SERVER['PHP_SELF']);
	break;
	
	case 'course_upload_save':
		$_tp_courses_id = (int)$_POST['tp_courses_id'];
		$st_planned_course_date = trim($_POST['planned_course_date']);
		$st_planned_course_notes = trim($_POST['planned_course_notes']);
		$st_planned_course_trainer = trim($_POST['planned_course_trainer']);

		if ( course_date_valid($st_planned_course_date) == false ) {
			header('Location: ' . $_SERVER['PHP_SELF'] . '?error=check_date&tp_courses_id=' . $_tp_courses_id);
		} else {
			$flag_nothing_to_save = false;
			
			if ( count( $_POST['sabre_status'][$_tp_courses_id] ) ) {
				foreach ( $_POST['sabre_status'][$_tp_courses_id] as $_no_staff => $available_sabres ) {
					if ( count($available_sabres) ) {
						foreach ( $available_sabres as $_sabre_id => $_sabre_added_status ) {
							if ( $_sabre_added_status > 0 ) {
								$flag_nothing_to_save = true;
								
								break 2;
							}
						}
					}
				}
			}			
			
			if ( count( $_POST['ddm_additional'] ) ) {
				foreach ( $_POST['ddm_additional'] as $ddm_id => $additional_sabre_id ) {
					if ( $additional_sabre_id > 0 ) {
						foreach ( $_POST['ddm_sabre_status'][$_tp_courses_id] as $_no_staff => $available_sabres ) {
							$additional_sabre_status = $_POST['ddm_sabre_status'][$_tp_courses_id][$_no_staff][$ddm_id];
							if ( $additional_sabre_status > 0 ) {
								$flag_nothing_to_save = true;

								break 2;
							}
						}
					}
				}
			}			
			
			if ( $flag_nothing_to_save == false ) {
				errorToPrint('Sorry, nothing to save.');
			} else {
				$_tp_planned_course_id = 0;

				if ( $_tp_courses_id > 0 ) {
					$_tp_planned_course_date = mktime( 0, 0, 0, substr($st_planned_course_date, 2, 2), substr($st_planned_course_date, 0, 2), substr($st_planned_course_date, 4, 2) );
					// 1. create planned course
					$sql = 'insert into ' . CONFP('TABLE_TP_PLANNED_COURSES') . ' (
						tp_courses_id,
						tp_planned_course_date,
						tp_planned_course_notes,
						tp_planned_course_trainer,
						tp_planned_course_added) values (
						"' . $_tp_courses_id . '", 
						' . $_tp_planned_course_date . ', 
						"' . addslashes($st_planned_course_notes) . '", 
						"' . addslashes($st_planned_course_trainer) . '", 
						' . TIME . ')';
					$db->query($sql);			
					$_tp_planned_course_id = $db->lastInsertID();
				}
			
				if ( $_tp_planned_course_id > 0 ) {
				// 2. create tp_planned_courses_to_sabres
					if ( count( $_POST['sabre_status'][$_tp_courses_id] ) ) {
						foreach ( $_POST['sabre_status'][$_tp_courses_id] as $_no_staff => $available_sabres ) {
							if ( count($available_sabres) ) {
								foreach ( $available_sabres as $_sabre_id => $_sabre_added_status ) {
									if ( $_sabre_added_status > 0 ) {
										$username_info = userNameByStaffNo($_no_staff);
										
										if ( ! checkSabreExistsForUserInCourse($_tp_planned_course_id, $_sabre_id, $_no_staff) ) {
											$sql = 'insert into ' . CONFP('TABLE_TP_PLANNED_COURSES_TO_SABRES') . ' (
												tp_planned_course_id,
												sabre_id,
												no_staff,
												firstname, 
												surname, 
												sabre_added_status,
												date_added) values (
												"' . $_tp_planned_course_id . '", 
												"' . $_sabre_id . '", 
												"' . addslashes($_no_staff) . '", 
												"' . $username_info['firstname'] . '", 
												"' . $username_info['surname'] . '", 
												"' . $_sabre_added_status . '", 
												' . TIME . ')';

											$db->query($sql);
										}
									}
								}
							}
						}
					}
			
					if ( count( $_POST['ddm_additional'] ) ) {
						foreach ( $_POST['ddm_additional'] as $ddm_id => $additional_sabre_id ) {
							if ( $additional_sabre_id > 0 ) {
								foreach ( $_POST['ddm_sabre_status'][$_tp_courses_id] as $_no_staff => $available_sabres ) {
									$additional_sabre_status = $_POST['ddm_sabre_status'][$_tp_courses_id][$_no_staff][$ddm_id];
									if ( $additional_sabre_status > 0 ) {
										$username_info = userNameByStaffNo($_no_staff);
										
										if ( ! checkSabreExistsForUserInCourse($_tp_planned_course_id, $additional_sabre_id, $_no_staff) ) {
											$sql = 'insert into ' . CONFP('TABLE_TP_PLANNED_COURSES_TO_SABRES') . ' (
												tp_planned_course_id,
												sabre_id,
												no_staff,
												firstname, 
												surname, 
												sabre_added_status,
												date_added) values (
												"' . $_tp_planned_course_id . '", 
												"' . $additional_sabre_id . '", 
												"' . addslashes($_no_staff) . '", 
												"' . $username_info['firstname'] . '", 
												"' . $username_info['surname'] . '", 
												"' . $additional_sabre_status . '", 
												' . TIME . ')';

											$db->query($sql);
										}
									}
								}
							}
						}
					}	
					$_SESSION['UPLOADED_USERS'] = array();
					$_SESSION['CSV_UPLOADED_USERS'] = array();
					$_SESSION['planned_course_date'] = '';
					$_SESSION['planned_course_notes'] = '';
					$_SESSION['planned_course_trainer'] = '';

					header('Location: ' . $_SERVER['PHP_SELF'] . '?success=saved_course');
				} else {
					errorToPrint('Please select a course!');
				}
			}
		}
	break;
	
	case 'course_edit_save':
	  $_tp_planned_course_id = (int)$_POST['tp_planned_course_id'];
	  $current_tp_planned_course_id = $_tp_planned_course_id;
		
	  $st_planned_course_date = $_POST['planned_course_date'];
	  $st_planned_course_notes = trim($_POST['planned_course_notes']);
	  $st_planned_course_trainer = trim($_POST['planned_course_trainer']);

	  if ( course_date_valid($st_planned_course_date) == false ) {
			header('Location: ' . $_SERVER['PHP_SELF'] . '?error=check_date&action=edit_planned_course&tp_planned_course_id=' . $current_tp_planned_course_id);
	  } else {
		$_tp_planned_course_date = mktime( 0, 0, 0, substr($st_planned_course_date, 2, 2), substr($st_planned_course_date, 0, 2), substr($st_planned_course_date, 4, 2) );
		
		// 1. Check if course date was not changed
		$res = $db->query('SELECT * 
	                     FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' 
	                   WHERE tp_planned_course_id = "' . $_tp_planned_course_id . '" 
					     and tp_planned_course_date = "' . $_tp_planned_course_date . '"');

		if ( $res->numRows() == 0 ) {
			$db->query('UPDATE ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' 
						SET tp_planned_course_date = "' . $_tp_planned_course_date . '" 
	                    WHERE tp_planned_course_id = "' . $_tp_planned_course_id . '"');
		} 

		$db->query('UPDATE ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' 
						SET tp_planned_course_notes = "' . addslashes($st_planned_course_notes) . '",
							tp_planned_course_trainer = "' . addslashes( ucwords($st_planned_course_trainer) ) . '",
						    tp_planned_course_changed = "' . TIME . '",
							changed_by = "' . userStaffNo() . '" 
	                    WHERE tp_planned_course_id = "' . $_tp_planned_course_id . '"');
					     
		// 2. update STANDARD existing course's modules and added new
		if ( count( $_POST['sabre_status'] ) ) {
			foreach ( $_POST['sabre_status'] as $_tp_planned_courses_to_sabres_id => $_sabre_added_status ) {
				if ( strpos($_tp_planned_courses_to_sabres_id, '_')===false ) { // field sabre_status[tp_planned_course_id]
					if ( (int)$_sabre_added_status == 0 ) {
						$db->query('DELETE FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' WHERE tp_planned_courses_to_sabres_id = "' . $_tp_planned_courses_to_sabres_id . '"');
					} else {
						$db->query('UPDATE ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' SET sabre_added_status = "' . $_sabre_added_status . '", date_modified = "' . TIME . '" WHERE tp_planned_courses_to_sabres_id = "' . $_tp_planned_courses_to_sabres_id . '"');
					}
				} else { // field sabre_status[tp_planned_course_id + staff_no + sabre_id]
					if ( (int)$_sabre_added_status > 0 ) {
						list ($tp_planned_course_id, $no_staff, $sabre_id) = split('_', $_tp_planned_courses_to_sabres_id);
						
						$username_info = userNameByStaffNo($no_staff);
						
						if ( ! checkSabreExistsForUserInCourse($tp_planned_course_id, $sabre_id, $no_staff) ) {
							$sql = 'insert into ' . CONFP('TABLE_TP_PLANNED_COURSES_TO_SABRES') . ' (
											tp_planned_course_id,
											sabre_id,
											no_staff,
											firstname, 
											surname, 
											sabre_added_status,
											date_added) values (
											"' . $tp_planned_course_id . '", 
											"' . $sabre_id . '", 
											"' . addslashes($no_staff) . '", 
											"' . $username_info['firstname'] . '", 
											"' . $username_info['surname'] . '", 
											"' . $_sabre_added_status . '", 
											' . TIME . ')';
							$db->query($sql);
						}
					}	
				}
			}
		}

		// 2.1. update ADDITIONAL existing course's modules and added new
		if ( count( $_POST['ddm_additional'] ) ) {
				foreach ( $_POST['ddm_additional'] as $ddm_id => $additional_sabre_id ) {
					if ( $additional_sabre_id > 0 ) { // add NEW, edit EXISTING
						foreach ( $_POST['ddm_sabre_status'][$_tp_planned_course_id] as $_no_staff => $available_sabres ) {
							$additional_sabre_status = $_POST['ddm_sabre_status'][$_tp_planned_course_id][$_no_staff][$ddm_id];

							$res = $db->query('select sabre_added_status from ' . CONFP('TABLE_TP_PLANNED_COURSES_TO_SABRES') . ' where tp_planned_course_id = "' . $_tp_planned_course_id . '" and no_staff="' . addslashes($_no_staff) . '" and sabre_id="' . $additional_sabre_id . '"');
							
							$sql = '';
							if ( $res->numRows() > 0 ) {
								$row = $res->fetchRow();
		
								if ( $row['sabre_added_status'] != $additional_sabre_status ) {
									if ( $additional_sabre_status==0 ) {
										$sql = 'delete from ' . CONFP('TABLE_TP_PLANNED_COURSES_TO_SABRES') . ' 
										              where tp_planned_course_id = "' . $_tp_planned_course_id . '" 
													    and no_staff="' . addslashes($_no_staff) . '" 
														and sabre_id="' . $additional_sabre_id . '"';
									}
								}
							} else {
								if ( $additional_sabre_status > 0 ) {
									$username_info = userNameByStaffNo($_no_staff);
									
									if ( ! checkSabreExistsForUserInCourse($_tp_planned_course_id, $additional_sabre_id, $_no_staff) ) {
										$sql = 'insert into ' . CONFP('TABLE_TP_PLANNED_COURSES_TO_SABRES') . ' (
											tp_planned_course_id,
											sabre_id,
											no_staff,
											firstname, 
											surname, 
											sabre_added_status,
											date_added) values (
											"' . $_tp_planned_course_id . '", 
											"' . $additional_sabre_id . '", 
											"' . addslashes($_no_staff) . '", 
											"' . $username_info['firstname'] . '", 
											"' . $username_info['surname'] . '", 
											"' . $additional_sabre_status . '", 
											' . TIME . ')';
									}
								}
							}

							if ( !empty($sql) ) {
								$db->query($sql);
							}
							
							
						}
					} elseif ( $_POST['ddm_additional_prev'][$ddm_id] > 0 && $additional_sabre_id == 0 ) { // REMOVE
						if (count($_POST['ddm_sabre_status'][$_tp_planned_course_id])){
							foreach ( $_POST['ddm_sabre_status'][$_tp_planned_course_id] as $_no_staff => $available_sabres ) {
								$sql = 'delete from ' . CONFP('TABLE_TP_PLANNED_COURSES_TO_SABRES') . ' 
										              where tp_planned_course_id = "' . $_tp_planned_course_id . '" 
													    and no_staff="' . addslashes($_no_staff) . '" 
														and sabre_id="' . $_POST['ddm_additional_prev'][$ddm_id] . '"';
								//echo $sql . '<br>';
								$db->query($sql);
							}
						}
					}
				}
		}			

		// 3. delete users from the COURSE
		foreach ( $_POST['delete_staff_no'] as $_no_staff => $value ) {
			if ( $value > 0 ) {
				$db->query('DELETE FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' 
    					        WHERE tp_planned_course_id = "' . $_tp_planned_course_id . '"
								  AND no_staff = "' . $_no_staff . '"');
			}
		}

//		$_SESSION['DELETED_EDIT_USERS'] = array();
			
		// if count USERS in the course = 0 then DELETE the COURSE
		$res = $db->query('SELECT * FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' WHERE tp_planned_course_id = "' . $_tp_planned_course_id . '"');
		if ( $res->numRows() == 0 ) {
			$res = $db->query('select tp_courses_id from ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' WHERE tp_planned_course_id = ' . $_tp_planned_course_id);	
			$row = $res->fetchRow();
			$real_course_id = $row['tp_courses_id'];

			$db->query('delete from ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' WHERE tp_planned_course_id = ' . $_tp_planned_course_id);	
			
			$res = $db->query('select * from ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' WHERE tp_courses_id = ' . $real_course_id);	
			if ( $res->numRows() > 0 ) {
				header('Location: ' . $_SERVER['PHP_SELF'] . '?success=saved_course&action=edit_planned_course&tp_courses_id=' . $real_course_id);
			} else {
				header('Location: /external/tp/admin/planned_course.php');			
			}
		} else {
			header('Location: ' . $_SERVER['PHP_SELF'] . '?success=saved_course&action=edit_planned_course&tp_planned_course_id=' . $current_tp_planned_course_id);
		}
	  }		
	break;
	
	case 'add_new_user_to_existing_course':
	  if ( trim($_POST['new_staff_no'])!='' ) {
		$flag_error_new_user = false;
		$buf = explode(',', $_POST['new_staff_no']);
		
		$buf[0] = trim($buf[0]); // staff_no
		$buf[1] = trim($buf[1]); // crew code
		
		$res = $db->query('SELECT DISTINCT u.no_staff, CONCAT(u.surname, ", ", u.firstname) as user_name, u.surname, u.firstname, r.id_rank as rank        
						 		   FROM users u, users_ranks ur, ranks r  
                                   WHERE no_staff = "' . $buf[0] . '"
								     AND ur.id_rank = r.id_rank
								     AND ur.id_user = u.id_user');

        
		if ( $res->numRows() ) {
			$row = $res->fetchRow();
        	$_rank = $row['rank'];
					
			$check_user_query = $db->query('SELECT * FROM tp_planned_courses_to_sabres WHERE no_staff = "' . $buf[0] . '" and tp_planned_course_id = ' . (int)$_GET['tp_planned_course_id']);
			if ( $check_user_query->numRows() ) {
				$detected_user = userNameByStaffNo($buf[0]);

				errorToPrint($detected_user['surname'] . ', ' . $detected_user['firstname'] . ' already added!');
			
				$flag_error_new_user = true;
			}
			
			if ( empty($buf[1]) ) {
				errorToPrint('User qualification is empty!');
			
				$flag_error_new_user = true;
			} 
		} else {
			errorToPrint('User not detected!');
			
			$flag_error_new_user = true;
		}

		if ( $flag_error_new_user == false ) {
			$get_sabres_from_course_query = $db->query('SELECT distinct sq.sabre_id, sq.sabre_rank
						FROM ' . $CONFP['TABLE_TP_SABRE_QUALIF'] . ' sq, ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' pc, ' . $CONFP['TABLE_TP_SABRES_TO_COURSES'] . ' s2c
						WHERE s2c.sabre_id = sq.sabre_id
						  and s2c.tp_courses_id = pc.tp_courses_id
						  and pc.tp_planned_course_id = ' . (int)$_GET['tp_planned_course_id'] . ' 
					 order by sq.sabre_id asc');

			$flag_user_inserted = false;
			while ( $sabre_item = $get_sabres_from_course_query->fetchRow() ) {
				$flag_use_logic = false;

				if ( $sabre_item['sabre_rank'] == 0 ) {
					$flag_use_logic = true;
				} elseif ( ( $sabre_item['sabre_rank'] == 1 ) && ( in_array($_rank, $CONFP['RANKS_PILOTS']) ) ) {
					$flag_use_logic = true;
				} elseif ( ( $sabre_item['sabre_rank'] == 2 ) && ( in_array($_rank, $CONFP['RANKS_CREWS'])  ) ) {
					$flag_use_logic = true;
				}

				if ( $flag_use_logic && crewCodeAssignedToSabre($buf[1], $sabre_item['sabre_id']) ) {
					$username_info = userNameByStaffNo($buf[0]);
					
					if ( ! checkSabreExistsForUserInCourse((int)$_GET['tp_planned_course_id'], $sabre_item['sabre_id'], $buf[0]) ) {
						$db->query('insert into tp_planned_courses_to_sabres(
									tp_planned_course_id, 
									sabre_id,
									no_staff, 
									firstname, 
									surname, 
									sabre_added_status,
									date_added
									) values(
									"' . (int)$_GET['tp_planned_course_id'] . '", 
									' . $sabre_item['sabre_id'] . ',
									"' . $buf[0] . '", 
									"' . $username_info['firstname'] . '", 
									"' . $username_info['surname'] . '", 
									1,
									' . TIME . ')');

						$flag_user_inserted = true;
					}
				} 
				
				if ( $flag_user_inserted == false ) {
					$username_info = userNameByStaffNo($buf[0]);
					
					$db->query('insert into tp_planned_courses_to_sabres(tp_planned_course_id, no_staff, firstname, surname, date_added) values("' . (int)$_GET['tp_planned_course_id'] . '", "' . $buf[0] . '", "' . $username_info['firstname'] . '",	"' . $username_info['surname'] . '", ' . TIME . ')');
				}
			}		
		}
	  }
	  
	  $action = 'edit_planned_course';
	  $flag_clear_remove_array = false;

	case 'edit_planned_course':
		$planned_courses = getAllPlannedCourses();
		$planned_courses_dates = array();
		
		if ( $flag_clear_remove_array == true ) {
			$_SESSION['DELETED_EDIT_USERS'] = array();
		}
		
		foreach ( $planned_courses as $_tp_courses_id => $courses_data ) {
			$planned_courses_dates[$_tp_courses_id] = getPlannedCourseDates($_tp_courses_id);                    
		}	

		if ( isset($_GET['tp_planned_course_id']) && (int)$_GET['tp_planned_course_id'] > 0 ) { 
			$current_tp_planned_course_id = (int)$_GET['tp_planned_course_id'];		

			$current_tp_courses_id = 0;
			foreach ( $planned_courses_dates as $_tp_courses_id => $courses_data ) {
				foreach ( $courses_data as $_tp_planned_course_id => $planned_course_data ) {
					if ( $_tp_planned_course_id == $current_tp_planned_course_id ) {
						$current_tp_courses_id = $_tp_courses_id;

						break;
					}
				}
				if ( $current_tp_courses_id > 0 ) break;
			}
		} elseif ( isset($_GET['tp_courses_id']) ) {
			$current_tp_courses_id = (int)$_GET['tp_courses_id'];

			reset($planned_courses_dates[$current_tp_courses_id]);
			$tmp_buffer = current($planned_courses_dates[$current_tp_courses_id]);
			$current_tp_planned_course_id = $tmp_buffer['tp_planned_course_id'];
		}

		// BOF get user's data
		$courses_users_table = get_courses_users_table($current_tp_planned_course_id);
		// EOF get user's data
	break;
	
	case 'add_new_user':
	 if ( trim($_POST['new_staff_no'])!='' ) {
		$flag_error_new_user = false;
		$buf = explode(',', $_POST['new_staff_no']);
		
		$buf[0] = trim($buf[0]);
		$buf[1] = trim($buf[1]);
		/*		
		$res = $db->query('SELECT DISTINCT no_staff, CONCAT(surname, ", ", firstname) as user_name, surname, firstname, rank     
						 		   FROM users 
                                   WHERE no_staff = "' . $buf[0] . '"');
		*/
		
		$res = $db->query('SELECT DISTINCT u.no_staff, CONCAT(u.surname, ", ", u.firstname) as user_name, u.surname, u.firstname, r.id_rank as rank          
						 		   FROM users u, users_ranks ur, ranks r  
                                   WHERE no_staff = "' . $buf[0] . '"
								     AND ur.id_rank = r.id_rank
								     AND ur.id_user = u.id_user');

		if ( $res->numRows() ) {
			$row = $res->fetchRow();
			
			foreach ( $_SESSION['UPLOADED_USERS'] as $_no_staff => $value ) {
				if ( $_no_staff == $buf[0] ) {
					errorToPrint($row['user_name'] . ' already added!');
			
					$flag_error_new_user = true;
					
					break;
				}
			}
			
			if ( empty($buf[1]) ) {
				errorToPrint('User qualification is empty!');
			
				$flag_error_new_user = true;
			} 
		} else {
			errorToPrint('User not detected!');
			
			$flag_error_new_user = true;
		}

		if ( $flag_error_new_user == false ) {
			$_SESSION['UPLOADED_USERS'][$buf[0]] = array($buf[0], $buf[1]);	
			$_SESSION['UPLOADED_USERS'][$buf[0]]['firstname'] = $row['firstname'];
			$_SESSION['UPLOADED_USERS'][$buf[0]]['surname'] = $row['surname'];
			$_SESSION['UPLOADED_USERS'][$buf[0]]['rank'] = $row['rank'];
//			$_SESSION['UPLOADED_USERS'][$csv_item[0]]['deleted'] = 0;
		}
	 }
	break;
}

if ( $action != 'edit_planned_course' ) {
	// 1. create courses array
	$current_tp_courses_id = (int)$_GET['tp_courses_id'];
	
	$courses_users_table = array();
	$course_sabres_array = array();
	
	if ( $current_tp_courses_id == 0 ) {
		for ( $i=0; $i < $CONFP['COUNT_COLUMNS']; $i++) {
			$course_sabres_array[] = array(
				'sabre_id' => 0,
				'sabre_code' => '&nbsp;'
			);
		}

		if ( count($_SESSION['UPLOADED_USERS']) > 0 ) {
			foreach ( $_SESSION['UPLOADED_USERS'] as $key => $user_item ) {
				$courses_users_table[$user_item[0]] = array();
			}		
		}
	} else {
		$course_sabres_array = getSabresByCourse( $current_tp_courses_id );
/*
Array
(
    [0] => Array
        (
            [sabre_id] => 2
            [sabre_code] => F31
            [sabre_subject] => A321 Refresher
            [sabre_rank] => 0
        )
)

Array
(
    [0] => 168302
    [1] =>  33
    [firstname] => Paul
    [surname] => Daniels
    [rank] => Captain
)

Array
(
    [0] => 0300206
    [1] =>  30
    [firstname] => Steven
    [surname] => Mellor
    [rank] => First Officer
)

*/

		if ( count($_SESSION['UPLOADED_USERS']) > 0 ) {
			foreach ( $_SESSION['UPLOADED_USERS'] as $key => $user_item ) {

				foreach ( $course_sabres_array as $key => $sabre_item ) {
					$flag_use_logic = false;

					if ( $sabre_item['sabre_rank'] == 0 ) {
						$flag_use_logic = true;
					} elseif ( ( $sabre_item['sabre_rank'] == 1 ) && ( in_array($user_item['rank'], $CONFP['RANKS_PILOTS']) ) ) {
						$flag_use_logic = true;
					} elseif ( ( $sabre_item['sabre_rank'] == 2 ) && ( in_array($user_item['rank'], $CONFP['RANKS_CREWS'])  ) ) {
						$flag_use_logic = true;
					}

					$courses_users_table[$user_item[0]][$sabre_item['sabre_id']] = 0;
				
					if ( $flag_use_logic && crewCodeAssignedToSabre(trim($user_item[1]), $sabre_item['sabre_id']) ) {
						// check if USER pass this SABRE in the COURSE
						$courses_users_table[$user_item[0]][$sabre_item['sabre_id']] = 1;
					}
				}
			}		
		}

	}
	
}

switch ( @$_GET['success'] ) {
	case 'uploaded': 
		/*
		10 users correctly uploaded.
		167882 unknown
		12334 unknown
		*/
		$unknown_users = '';
		$unknown_crews = '';
		$correctly_uploaded_users = 0;
		
		if ( count($_SESSION['CSV_UPLOADED_USERS']) > 0 ) {
			foreach ( $_SESSION['CSV_UPLOADED_USERS'] as $key => $no_staff ) {
				if ( is_array($_SESSION['UPLOADED_USERS'][$no_staff]) ) {
					$correctly_uploaded_users++;

					// BOF check CREW code section
					$tmp_res = $db->query('SELECT * FROM tp_crew_qualif WHERE cq_code ="' . $_SESSION['UPLOADED_USERS'][$no_staff][1] . '"');
					if ( $tmp_res->numRows() == 0 ) {
						$unknown_crews .= '<br>' . $no_staff . ': unknown crew code';
					}
					// EOF check CREW code section
				} else {
					$unknown_users .= $no_staff . ' unknown' . '<br>';
				}
			}		
		}
		
		$use_auto_close = false;
		
		infoToPrint( $correctly_uploaded_users . ' users correctly uploaded.' . '<br>' . $unknown_users . $unknown_crews); 
	break;
	case 'saved_course': 
		successToPrint('Course was successfully saved'); 
	break;
}

switch ( @$_GET['error'] ) {
	case 'check_date': 
		errorToPrint('Please check date.');
	break;
}
includeJS();

pageAutoAssignVars('action', 'courses_users_table', 'course_sabres_array', 'planned_courses', 'planned_courses_dates', 'current_tp_courses_id', 'current_tp_planned_course_id');

pageDisplay();

?>
