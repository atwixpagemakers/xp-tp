<?php

require_once '../inc/global.php';

adminAccessCheck();

pageTitle('Email Setup');

$correct_admin_email = array();

switch ( $_GET['action'] )  {
	case 'confirm_email_update':
		$st_admin_email = trim($_POST['admin_email']);
		$admin_email_array = explode(',', $st_admin_email);	
		
		if ( count($admin_email_array) ) {
			foreach ( $admin_email_array as $key => $admin_email )	{
				$admin_email = trim($admin_email);
				if ( tep_validate_email($admin_email) ) {
					$correct_admin_email[] = $admin_email;
				}
			}
		}
		
		if ( count($correct_admin_email) ) {
			$db->query('update tp_config set conf_value="' . implode(',', $correct_admin_email) . '" where conf_name="TP_ADMIN_EMAILS"');

			successToPrint( 'Email' . ( count($correct_admin_email)==1 ? ' was' : 's were' )  . '  successfully saved');			
		} else {
			$db->query('update tp_config set conf_value="" where conf_name="TP_ADMIN_EMAILS"');

			errorToPrint('Check Admin Email field!');
		}
	break;
}

$tp_admin_emails_query = $db->query('select conf_value from tp_config where conf_name="TP_ADMIN_EMAILS"');
$tp_admin_emails_item = $tp_admin_emails_query->fetchRow();
$admin_email = $tp_admin_emails_item['conf_value'];
//includeJS();

pageAutoAssignVars('courses_data', 'admin_email');

pageDisplay();
?>
