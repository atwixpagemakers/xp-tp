<?php
require_once '../inc/global.php';

adminAccessCheck();

pageTitle('Planned Courses');


$planned_courses = getAllPlannedCourses(true);
$planned_courses_dates = array();
foreach ( $planned_courses as $_tp_courses_id => $courses_data ) {
	$planned_courses_dates[$_tp_courses_id] = getPlannedCourseDates($_tp_courses_id, true);                    
}

if ( (!isset($_GET['tp_planned_course_id'])) && (!isset($_GET['tp_courses_id'])) ) {
	// get first course
	reset($planned_courses);
	$tmp_buffer = current($planned_courses);
	$current_tp_courses_id = $tmp_buffer['tp_courses_id'];
	
	reset($planned_courses_dates[$current_tp_courses_id]);
	$tmp_buffer = current($planned_courses_dates[$current_tp_courses_id]);
	$current_tp_planned_course_id = $tmp_buffer['tp_planned_course_id'];
} elseif ( isset($_GET['tp_planned_course_id']) ) {
	$current_tp_planned_course_id = (int)$_GET['tp_planned_course_id'];
	
	$current_tp_courses_id = 0;
	foreach ( $planned_courses_dates as $_tp_courses_id => $courses_data ) {
		foreach ( $courses_data as $_tp_planned_course_id => $planned_course_data ) {
			if ( $_tp_planned_course_id == $current_tp_planned_course_id ) {
				$current_tp_courses_id = $_tp_courses_id;
				break;
			}
		}
		if ( $current_tp_courses_id > 0 ) break;
	}
} elseif ( isset($_GET['tp_courses_id']) ) {
	$current_tp_courses_id = (int)$_GET['tp_courses_id'];

	reset($planned_courses_dates[$current_tp_courses_id]);
	$tmp_buffer = current($planned_courses_dates[$current_tp_courses_id]);
	$current_tp_planned_course_id = $tmp_buffer['tp_planned_course_id'];
}

// BOF get user's data
$courses_users_table = get_courses_users_table($current_tp_planned_course_id);
// EOF get user's data

// BOF get SABRES in the current COURSE
$courses_sabres_table = array();
$courses_sabres_table = getSabresByCourse($current_tp_courses_id);
$another_courses_sabres_table = getAnotherSabresFromPlannedCourse($current_tp_planned_course_id);

foreach ( $another_courses_sabres_table as $key => $sabre_item ) {
	$courses_sabres_table[] = array_merge($sabre_item, array('color' => 'red') );
}
// EOF get SABRES in the current COURSE

switch ( $_GET['action'] )  {
	case 'print_course':
		define('_MPDF_PATH', ROOT_PROJ . '/inc/MPDF/');
		define('ROOT_PAS', realpath(ROOT_PROJ . '../pas') . '/');
	
		require_once _MPDF_PATH . 'mpdf.php';
		require_once ROOT_PAS . 'inc/class.ns.http.php';
	break;

	case 'delete_planned_course':
		$db->query('delete from ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' WHERE tp_planned_course_id = ' . (int)$_GET['tp_planned_course_id']);
		$db->query('delete from ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' WHERE tp_planned_course_id = ' . (int)$_GET['tp_planned_course_id']);

		header('Location: ' . $_SERVER['PHP_SELF'] . '?success=course_deleted'); exit;
	break;
}


switch ( @$_GET['success'] ) {
	case 'uploaded': successToPrint('Successfully uploaded'); break;
	case 'saved_course': successToPrint('Course was successfully saved'); break;
	case 'course_deleted': successToPrint('Course was successfully deleted!'); break;
}

includeJS();

pageAutoAssignVars('courses_users_table', 'planned_courses', 'planned_courses_dates', 'current_tp_courses_id', 'current_tp_planned_course_id', 'mpdf', 'courses_sabres_table');

if ( $_GET['action'] == 'print_course' ) {
	pageDisplay( 'admin/reports/planned_course' );
} else {
	pageDisplay();
}

?>
