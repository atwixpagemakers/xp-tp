<?php

require_once '../inc/global.php';

adminAccessCheck();

pageTitle('Sabre Qualification');

$aSQ = sabreQualifGet();

$iEdit = intval(@ $_GET['edit']);

if ( ! isset($aSQ[$iEdit]) ) $iEdit = 0;

if ( isset($_POST['delete']) ) {
	// BOF check for assigned COURSES
	$tmp_assinged_courses = '';
	$flag_assinged_courses_exists = false;
	$total_courses = 0;
	foreach ( $_POST['delete'] as $_sabre_id ) {
		$count_courses = sabreAssignedToCourses($_sabre_id);
		$total_courses += $count_courses;
		$tmp_assinged_courses .= '<small>* Sabre <font color=red>' . $aSQ[$_sabre_id]['sabre_code'] . '</font> assigned with <font color=red>' . $count_courses . ' course' . ( $count_courses > 1 ? 's' : '' ) . '</font>.</small>' . '<br>';
		
		if ( $count_courses > 0 ) {
			$flag_assinged_courses_exists = true;	
		}
		
	}
	// EOF check for assigned SABRES
	
	if ( $flag_assinged_courses_exists ) {
		errorToPrint( ( $total_courses > 1 ? 'There are some courses' : 'There is course' ) . ' associated with selected sabre' . ( count($_POST['delete']) > 1 ? 's' : '' ) . '.<br>Please deassign ' . ( $total_courses > 1 ? 'them' : 'it' ) . ' before deleting: <br>' . $tmp_assinged_courses);
	} else {
		foreach ( $_POST['delete'] as $_sabre_id ) {
			$db->query('DELETE FROM ' . $CONFP['TABLE_TP_SABRE_QUALIF'] . ' WHERE sabre_id = ' . $_sabre_id);
			$db->query('delete from tp_crews_to_sabres where sabre_id = ' . $_sabre_id);
			$db->query('delete from tp_sabres_to_courses where sabre_id = ' . $_sabre_id);
			
			// BOF delete sabres from planned courses
			$active_courses_query = $db->query('select pc2s.tp_planned_courses_to_sabres_id 
			                                      from tp_planned_courses_to_sabres pc2s, tp_planned_courses pc
												  where pc2s.tp_planned_course_id = pc.tp_planned_course_id 
												  and pc.tp_planned_course_completed = 0
												  and pc2s.sabre_id = ' . $_sabre_id);	
			while ( $active_courses_item = $active_courses_query->fetchRow() ) {
				$db->query('delete from tp_planned_courses_to_sabres where tp_planned_courses_to_sabres_id = ' . (int)$active_courses_item['tp_planned_courses_to_sabres_id'] );
			}
			// EOF delete sabres from planned courses
		}
	
		header('Location: ' . $_SERVER['PHP_SELF'] . '?success=deleted'); exit;
	}
}


$sData = array();
if ( $iEdit ) $sData = $aSQ[$iEdit];

if ( ! is_array($sData) ) $sData = array();



if ( ! empty($_POST['submitted']) ) {
	$sData = typedArray($_POST, array
	(
		'sabre_code' => '',
		'sabre_subject' => '',
	),
	'trim');
	
	$sData['sabre_id'] = $iEdit;
	$sData['sabre_exp'] = (int)$_POST['sabre_exp'];
	$sData['sabre_pf'] = (int)$_POST['sabre_pf'];
	$sData['sabre_eom'] = (int)$_POST['sabre_eom'];
	$sData['sabre_grace'] = (int)$_POST['sabre_grace'];
	$sData['sabre_rank'] = (int)$_POST['sabre_rank'];

	$sData['sabre_code'] = mb_strtoupper($sData['sabre_code']);

	# Errors		
	if ( ! $sData['sabre_code'] || ! $sData['sabre_subject'] ) {
		errorToPrint('Please complete all fields');
	} elseif ( sabreCodeExists($sData['sabre_code']) && $iEdit != sabreCodeExists($sData['sabre_code']) ) {
		errorToPrint('This code already used');
	}

	if ( ! $aErrors ) {
		if ( ! $iEdit ) { // INSERT
			$sql = 'insert into ' . CONFP('TABLE_TP_SABRE_QUALIF') . ' (
						sabre_code,
						sabre_subject,
						sabre_exp,
						sabre_pf,
						sabre_eom,
						sabre_grace,
						date_added,
						sabre_rank) values (
						"' . addslashes($sData['sabre_code']) . '", 
						"' . addslashes($sData['sabre_subject']) . '", 
						"' . (int)$_POST['sabre_exp'] . '",
						"' . (int)$_POST['sabre_pf'] . '",
						"' . (int)$_POST['sabre_eom'] . '",	
						"' . (int)$_POST['sabre_grace'] . '",
						' . TIME . ',
						"' . (int)$_POST['sabre_rank'] . '")';
			
			if ( PEAR::isError($db->query($sql)) ) {
				errorToPrint('Database error on saving info');
			} else {
				$_sabre_id = $db->lastInsertID();
				if ( isset($_POST['cq_id']) ) {
					foreach ( $_POST['cq_id'] as $cq_id => $value ) {
						$db->query('insert into ' . CONFP('TABLE_TP_CREWS_TO_SABRES') . '(cq_id, sabre_id) values (' . $cq_id . ', ' . $_sabre_id . ')');
					}
				}
	
				header('Location: ' . $_SERVER['PHP_SELF'] . '?success=saved'); exit;				
			}
		} else { // UPDATE
			$sql = 'update ' . CONFP('TABLE_TP_SABRE_QUALIF') . ' 
					set sabre_code = "' . addslashes($sData['sabre_code']) . '",
						sabre_subject = "' . addslashes($sData['sabre_subject']) . '",
						sabre_exp = "' . (int)$_POST['sabre_exp'] . '",
						sabre_pf = "' . (int)$_POST['sabre_pf'] . '",
						sabre_eom = "' . (int)$_POST['sabre_eom'] . '",	
						sabre_grace = "' . (int)$_POST['sabre_grace'] . '",
						date_modified = ' . TIME . ',
						sabre_rank = "' . (int)$_POST['sabre_rank'] . '"
					where sabre_id = ' . $iEdit;
			
			if ( PEAR::isError($db->query($sql)) ) {
				errorToPrint('Database error on saving info');
			} else {
				$db->query('delete from ' . CONFP('TABLE_TP_CREWS_TO_SABRES') . ' where sabre_id = ' . $iEdit);
				
				$_sabre_id = $iEdit;
				if ( isset($_POST['cq_id']) ) {
					foreach ( $_POST['cq_id'] as $cq_id => $value ) {
						$db->query('insert into ' . CONFP('TABLE_TP_CREWS_TO_SABRES') . '(cq_id, sabre_id) values (' . $cq_id . ', ' . $_sabre_id . ')');
					}
				}
	
				header('Location: ' . $_SERVER['PHP_SELF'] . '?success=saved'); exit;				
			}
		}
	}
}

switch ( @$_GET['success'] )
{
	case 'saved': successToPrint('Successfully saved'); break;
	case 'deleted': successToPrint('Successfully deleted'); break;
}

includeJS();

pageAutoAssignVars('iEdit', 'aSQ', 'sData');

pageDisplay();

?>
