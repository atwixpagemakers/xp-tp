<?php
ini_set('memory_limit', '256M');

require_once '../inc/global.php';

adminAccessCheck();

pageTitle('Reports');

if ( isset($_GET['report_type']) && in_array($_GET['report_type'], array('report_exp', 'report_mod', 'report_user', 'report_course', 'report_fail')) ) {
	$selected_report_type = $_GET['report_type'];
} else {
	$selected_report_type = '';
}

if ( isset($_GET['sort_field']) && !empty($_GET['sort_field']) ) {
	$selected_sort_field = $_GET['sort_field'];
} else {
	$selected_sort_field = '';
}


$flag_error = false;
$reports_data = array();

if ( isset($_GET['include_deleted_users']) ) {
	$_include_deleted_users = (int)$_GET['include_deleted_users'];
} else {
	$_include_deleted_users = 0;
}

switch ( $_GET['action'] )  {
	case 'confirm_report':
		// *****************************************************************************
		//                            BOF check errors
		// *****************************************************************************
		$error_message = "";

		if ( in_array($selected_report_type, array('report_exp', 'report_mod', 'report_fail')) ) {
			$month_from = 0;
			$month_to = 0;
			$year_from = 0;		
			$year_to = 0;
			$id_base = 0;
						
			if ( isset($_GET['month_from']) ) $month_from = (int)$_GET['month_from'];
			if ( isset($_GET['month_to']) ) $month_to = (int)$_GET['month_to'];
			if ( isset($_GET['year_from']) ) $year_from = (int)$_GET['year_from'];
			if ( isset($_GET['year_to']) ) $year_to = (int)$_GET['year_to'];
			if ( isset($_GET['id_base']) ) $id_base = (int)$_GET['id_base'];

			if ( $month_from == 0 ) {
				$error_message .= "* 'From Month' field is empty!" . "\n";

				$flag_error = true;
			}

			if ( $year_from == 0 ) {
				$error_message .= "* 'From Year' field is empty!" . "\n";

				$flag_error = true;
			}

			if ( $month_to == 0 ) {
				$error_message .= "* 'To Month' field is empty!" . "\n";

				$flag_error = true;
			}

			if ( $year_to == 0 ) {
				$error_message .= "* 'To Year' field is empty!" . "\n";

				$flag_error = true;
			}

			if ( $year_to > 0 && $month_to > 0 && $month_from > 0 && $year_from > 0 ) {
				if ( $year_from > $year_to ) {
					$error_message .= "* 'From Year' field is more than 'To Year' field!" . "\n";

					$flag_error = true;
				}

				if ( $year_to == $year_from ) {
					if ( $month_from > $month_to ) {
						$error_message .= "* 'From Month' field is more than 'To Month' field!" . "\n";

						$flag_error = true;
					}
				}
			}
		}

		if ( $selected_report_type == 'report_user' ) {
			$selected_no_staff = trim($_GET['no_staff']);
			
			if ( $selected_no_staff == '' ) {
				$error_message .= "User field is empty!" . "\n";

				$flag_error = true;
			}
		}


		if ( $selected_report_type == 'report_course' ) {
			$month_from = 0;
			$month_to = 0;
			$year_from = 0;		
			$year_to = 0;
						
			if ( isset($_GET['month_from']) ) $month_from = (int)$_GET['month_from'];
			if ( isset($_GET['month_to']) ) $month_to = (int)$_GET['month_to'];
			if ( isset($_GET['year_from']) ) $year_from = (int)$_GET['year_from'];
			if ( isset($_GET['year_to']) ) $year_to = (int)$_GET['year_to'];

			if ( $month_from == 0 ) {
				$error_message .= "* 'From Month' field is empty!" . "\n";

				$flag_error = true;
			}

			if ( $year_from == 0 ) {
				$error_message .= "* 'From Year' field is empty!" . "\n";

				$flag_error = true;
			}

			if ( $month_to == 0 ) {
				$error_message .= "* 'To Month' field is empty!" . "\n";

				$flag_error = true;
			}

			if ( $year_to == 0 ) {
				$error_message .= "* 'To Year' field is empty!" . "\n";

				$flag_error = true;
			}

			if ( $year_to > 0 && $month_to > 0 && $month_from > 0 && $year_from > 0 ) {
				if ( $year_from > $year_to ) {
					$error_message .= "* 'From Year' field is more than 'To Year' field!" . "\n";

					$flag_error = true;
				}

				if ( $year_to == $year_from ) {
					if ( $month_from > $month_to ) {
						$error_message .= "* 'From Month' field is more than 'To Month' field!" . "\n";

						$flag_error = true;
					}
				}
			}
		}
		
		if ( $flag_error ) {
			errorToPrint($error_message);
		}
		// *****************************************************************************
		//                            EOF check errors
		// *****************************************************************************

		if ( $flag_error == false ) {
		  switch ( $selected_report_type )  {
			// *****************************************************************
			//                 BOF EXPIRY report
			// *****************************************************************
			case 'report_exp':
				$date_from = mktime(0, 0, 0, $month_from, 1, $year_from);
				$date_to = mktime(23, 59, 59, $month_to, cal_days_in_month(CAL_GREGORIAN, $month_to, $year_to), $year_to);

				if ( $selected_sort_field != '' ) {
					switch ( $selected_sort_field ) {
						case 'user_name':
							$sql_order_by = 'user_name ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' ) . ', b.base_shortname asc, pc2s.sabre_code asc';
						break;

						case 'base_shortname':
							$sql_order_by = 'b.base_shortname ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'sabre_code':
							$sql_order_by = 'pc2s.sabre_code ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'sabre_subject':
							$sql_order_by = 'pc2s.sabre_subject ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'date':
							$sql_order_by = 'pc.tp_planned_course_complete_date ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'exp_date':
							$sql_order_by = 'pc2s.exp_date ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						default: 
							$sql_order_by = 'pc2s.exp_date asc, b.base_shortname asc, pc2s.sabre_code asc';
							
							$selected_sort_field = 'exp_date';
					}
				} else {
					$sql_order_by = 'pc2s.exp_date asc, b.base_shortname asc, pc2s.sabre_code asc';
					
					$selected_sort_field = 'exp_date';	
				}
			
				if ( $id_base == 0 ) {
					$report_sql = 'select distinct u.firstname, u.id_user, u.surname, CONCAT(u.surname, ", ", u.firstname) as user_name,
								  pc.tp_planned_course_date,
								  pc.tp_planned_course_complete_date,
								  max(pc.tp_planned_course_id) as tp_planned_course_id,
								  pc2s.no_staff, pc2s.tp_planned_courses_to_sabres_id, pc2s.sabre_code, pc2s.sabre_subject, pc2s.sabre_id,
								  
								  /* pc2s.exp_date, */
								  pc2s.tp_planned_course_id as sql_tp_planned_course_id, 
								  pc2s.no_staff as sql_no_staff, 
								  pc2s.sabre_id as sql_sabre_id,
								  pc2s.passed_date,
								  if( pc2s.exp_date >0, 
								      pc2s.exp_date, 
											  (
												SELECT pc2s2.exp_date
												  FROM tp_planned_courses_to_sabres pc2s2, tp_planned_courses pc2
												 WHERE pc2s2.tp_planned_course_id <> sql_tp_planned_course_id
												   AND pc2s2.tp_planned_course_id = pc2.tp_planned_course_id
												   AND pc2.tp_planned_course_completed =1
												   AND pc2s2.no_staff = sql_no_staff
												   AND pc2s2.sabre_id = sql_sabre_id
												   AND pc2s2.exp_date > 0
												  /* and ( pc2s.exp_date >= ' . $date_from . ' and ' . $date_to . ' >= pc2s.exp_date)*/
											  ORDER BY pc.tp_planned_course_id DESC
												 LIMIT 1
											  ) 
								  ) AS exp_date,
								  pc2s.manually_added_by_trainer,
								  b.base_shortname
							 from tp_planned_courses_to_sabres pc2s, tp_planned_courses pc, users u
							 left join users_bases ub on u.id_user = ub.id_user
							 left join bases b on ub.id_base = b.id_base 
							where u.no_staff = pc2s.no_staff
							  and b.is_active > 0 
							  and pc.tp_planned_course_completed = 1
							  and ( pc2s.exp_date >= ' . $date_from . ' and ' . $date_to . ' >= pc2s.exp_date)
							  and pc.tp_planned_course_id = pc2s.tp_planned_course_id
						 GROUP BY user_name, pc2s.sabre_code
						 order by ' . $sql_order_by;
				} else {
					$report_sql = 'select distinct u.firstname, u.id_user, u.surname, CONCAT(u.surname, ", ", u.firstname) as user_name,
								  pc.tp_planned_course_date,
								  pc.tp_planned_course_complete_date,
								  max(pc.tp_planned_course_id) as tp_planned_course_id,
								  pc2s.no_staff, pc2s.tp_planned_courses_to_sabres_id, pc2s.sabre_code, pc2s.sabre_subject, pc2s.sabre_id,
								  /* pc2s.exp_date, */
								  pc2s.tp_planned_course_id as sql_tp_planned_course_id, 
								  pc2s.no_staff as sql_no_staff, 
								  pc2s.sabre_id as sql_sabre_id,
								  pc2s.passed_date,

								  if( pc2s.exp_date >0, 
								      pc2s.exp_date, 
											  (
												SELECT pc2s2.exp_date
												  FROM tp_planned_courses_to_sabres pc2s2, tp_planned_courses pc2
												 WHERE pc2s2.tp_planned_course_id <> sql_tp_planned_course_id
												   AND pc2s2.tp_planned_course_id = pc2.tp_planned_course_id
												   AND pc2.tp_planned_course_completed =1
												   AND pc2s2.no_staff = sql_no_staff
												   AND pc2s2.sabre_id = sql_sabre_id
												   AND pc2s2.exp_date > 0
												   /*and ( pc2s.exp_date >= ' . $date_from . ' and ' . $date_to . ' >= pc2s.exp_date)*/
											  ORDER BY pc.tp_planned_course_id DESC
												 LIMIT 1
											  ) 
								  ) AS exp_date,
								  pc2s.manually_added_by_trainer,								  
								  b.base_shortname
							 from tp_planned_courses_to_sabres pc2s, tp_planned_courses pc, users u, users_bases ub, bases b
							where u.no_staff = pc2s.no_staff
							  and u.id_user = ub.id_user
							  and ub.id_base = b.id_base
							  and b.id_base = ' . $id_base . '
							  and b.is_active > 0 
							  and pc.tp_planned_course_completed = 1
							  and ( pc2s.exp_date >= ' . $date_from . ' and ' . $date_to . ' >= pc2s.exp_date)
							  and pc.tp_planned_course_id = pc2s.tp_planned_course_id
						 GROUP BY user_name, pc2s.sabre_code
						 order by ' . $sql_order_by;
				}
				//echo $report_sql;
				$tp_report_query = $db->query($report_sql);
		
				while ( $tp_report_item = $tp_report_query->fetchRow() ) {
					if ( $tp_report_item['exp_date'] == 0 ) {
						$out_exp_date = 'Training Incomplete';				
					} elseif ( $tp_report_item['exp_date'] == $tp_report_item['passed_date'] ) {
						$out_exp_date = 'No Expiry';
					} else {
						$out_exp_date = date('d F, Y', $tp_report_item['exp_date']);
					}
			
					$reports_data[] = array(
						'firstname' => $tp_report_item['firstname'],
						'surname' => $tp_report_item['surname'],
						'manually_added_by_trainer' => $tp_report_item['manually_added_by_trainer'],
						'user_name' => $tp_report_item['user_name'],
						'sabre_code' => $tp_report_item['sabre_code'],
						'sabre_subject' => $tp_report_item['sabre_subject'],
						//'tp_planned_course_date' => date('d F, Y', $tp_report_item['tp_planned_course_date']),
						'date' => date('d F, Y', $tp_report_item['tp_planned_course_complete_date']),
						'exp_date' => $out_exp_date,
						'base_shortname' => $tp_report_item['base_shortname']
					);
				}			
			break;
			// *****************************************************************
			//                 EOF EXPIRY report
			// *****************************************************************

			// *****************************************************************
			//                 BOF MODULE report
			// *****************************************************************
			case 'report_mod':
				$date_from = mktime(0, 0, 0, $month_from, 1, $year_from);
				$date_to = mktime(23, 59, 59, $month_to, cal_days_in_month(CAL_GREGORIAN, $month_to, $year_to), $year_to);
			
				if ( $selected_sort_field != '' ) {
					switch ( $selected_sort_field ) {
						case 'sabre_code':
							$sql_order_by = 'pc2s.sabre_code ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'sabre_subject':
							$sql_order_by = 'pc2s.sabre_subject ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'base_shortname':
							$sql_order_by = 'b.base_shortname ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'total_count_passed':
							$sql_order_by = 'total_count_passed ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'total_count_modules':
							$sql_order_by = 'total_count_modules ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'percentage':
							$sql_order_by = 'percentage ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						default: 
							$sql_order_by = 'percentage asc';

							$selected_sort_field = 'percentage';
					}
				} else {
					$sql_order_by = 'percentage asc';
					
					$selected_sort_field = 'percentage';
				}

				if ( $id_base == 0 ) {
					$report_sql = 'SELECT pc2s.sabre_id, 
					                      sum( if( pc2s.sabre_passed_status IN ( 1, 2, 4 ) , 1, 0 ) ) AS total_count_passed, 
										  sum( if( pc2s.sabre_passed_status IN ( 1, 2, 3, 4, 5 ) , 1, 0 ) ) AS total_count_modules,
										  round( (sum( if( pc2s.sabre_passed_status IN ( 1, 2, 4 ) , 1, 0 ) ) / sum( if( pc2s.sabre_passed_status IN ( 1, 2, 3, 4, 5 ) , 1, 0 ) ) ) *100, 2 ) AS percentage,
										  pc2s.sabre_code, pc2s.sabre_subject,
										  b.base_shortname
								     FROM tp_planned_courses_to_sabres pc2s, tp_planned_courses pc, users u 
							    LEFT JOIN users_bases ub on u.id_user = ub.id_user
							    LEFT JOIN bases b on ub.id_base = b.id_base 
								    WHERE u.no_staff = pc2s.no_staff
									  AND b.is_active > 0 
									  AND pc.tp_planned_course_completed = 1
									  AND ( pc.tp_planned_course_date >= ' . $date_from . ' and pc.tp_planned_course_date <= ' . $date_to . ' )
									  AND pc.tp_planned_course_id = pc2s.tp_planned_course_id
								 GROUP BY pc2s.sabre_id
								 ORDER BY ' . $sql_order_by;
				} else {
					$report_sql = 'SELECT pc2s.sabre_id, 
					                      sum( if( pc2s.sabre_passed_status IN ( 1, 2, 4 ) , 1, 0 ) ) AS total_count_passed, 
										  sum( if( pc2s.sabre_passed_status IN ( 1, 2, 3, 4, 5 ) , 1, 0 ) ) AS total_count_modules,
										  round( (sum( if( pc2s.sabre_passed_status IN ( 1, 2, 4 ) , 1, 0 ) ) / sum( if( pc2s.sabre_passed_status IN ( 1, 2, 3, 4, 5 ) , 1, 0 ) ) ) *100, 2 ) AS percentage,
										  pc2s.sabre_code, pc2s.sabre_subject,
										  b.base_shortname
								     FROM tp_planned_courses_to_sabres pc2s, tp_planned_courses pc, users u 
							    LEFT JOIN users_bases ub on u.id_user = ub.id_user
							    LEFT JOIN bases b on ub.id_base = b.id_base 
								    WHERE u.no_staff = pc2s.no_staff
									  AND b.is_active > 0 
		  							  AND b.id_base = ' . $id_base . '									  
									  AND pc.tp_planned_course_completed = 1
									  AND ( pc.tp_planned_course_date >= ' . $date_from . ' and pc.tp_planned_course_date <= ' . $date_to . ' )
									  AND pc.tp_planned_course_id = pc2s.tp_planned_course_id
								 GROUP BY pc2s.sabre_id
								 ORDER BY ' . $sql_order_by;
				}
				
				// echo $report_sql;
				$tp_report_query = $db->query($report_sql);
		
				while ( $tp_report_item = $tp_report_query->fetchRow() ) {
					if ( $id_base == 0 ) {
						$out_base_shortname = 'ALL';						
					} else {
						$out_base_shortname = $tp_report_item['base_shortname'];
					}
					
					$reports_data[] = array(
						'sabre_code' => $tp_report_item['sabre_code'],
						'sabre_subject' => $tp_report_item['sabre_subject'],
						'percentage' => $tp_report_item['percentage'] . '%',
						'base_shortname' => $out_base_shortname,
						'total_count_modules' => $tp_report_item['total_count_modules'],
						'total_count_passed' => $tp_report_item['total_count_passed']
					);
				}			
			break;
			// *****************************************************************
			//                 EOF MODULE report
			// *****************************************************************


			// *****************************************************************
			//                 BOF USER report
			// *****************************************************************
			case 'report_user':
				if ( $selected_sort_field != '' ) {
					switch ( $selected_sort_field ) {
						case 'sabre_code':
							$sql_order_by = 'pc2s.sabre_code ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'sabre_subject':
							$sql_order_by = 'pc2s.sabre_subject ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'passed_by_trainer':
							$sql_order_by = 'passed_by_trainer ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;
						
						case 'passed_date':
							//$sql_order_by = 'pc.tp_planned_course_complete_date ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
							$sql_order_by = 'pc2s.passed_date ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'exp_date':
							$sql_order_by = 'exp_date ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'sabre_passed_status':
							$sql_order_by = 'pc2s.sabre_passed_status ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						default: 
							$sql_order_by = 'exp_date asc';
						
							$selected_sort_field = 'exp_date';	
					}
				} else {
					$sql_order_by = 'exp_date asc';
					
					$selected_sort_field = 'exp_date';	
				}
				
				
				if ( isset($_GET['tp_planned_course_id']) && (int)$_GET['tp_planned_course_id'] > 0 ) {
					$current_tp_planned_course_id = (int)$_GET['tp_planned_course_id'];
				} else {
					$current_tp_planned_course_id = 0;
				}
				
				$tp_report_user_users = array();

				// 0. how many users
				if ( isset($_GET['get_user_info']) ) {
					$how_many_users_query = $db->query('select distinct concat(no_staff, " ", firstname, " ", surname ) AS search_field,
				                                           no_staff, firstname, surname 
				                                      from tp_planned_courses_to_sabres
													 where no_staff = "' . $selected_no_staff . '"');
				} else {
					$how_many_users_query = $db->query('select distinct concat(no_staff, " ", firstname, " ", surname ) AS search_field, 
														   concat(no_staff, " ", surname, " ", firstname ) AS search_field2,
				                                           no_staff, firstname, surname 
				                                      from tp_planned_courses_to_sabres
													having search_field LIKE "%' . $selected_no_staff . '%" or search_field2 LIKE "%' . $selected_no_staff . '%"');
				}
				
				if ( $how_many_users_query->numRows() == 0 ) {
					errorToPrint('User not found!');
				} elseif ( $how_many_users_query->numRows() == 1 ) {
					$user_item = $how_many_users_query->fetchRow();
					$selected_no_staff = $user_item['no_staff'];
					
					// 1. detect user courses
					$detect_user_courses_query = $db->query('SELECT DISTINCT pc.tp_planned_course_id, 
				                                                         pc.tp_planned_course_date, 
																		 pc.tp_planned_course_notes, 
																		 pc.tp_planned_course_trainer, 
																		 pc.tp_planned_course_complete_date,
				                                                         pc.tp_courses_name
																FROM tp_planned_courses pc, tp_planned_courses_to_sabres pc2s 
																WHERE pc.tp_planned_course_completed =1
																  AND pc2s.tp_planned_course_id = pc.tp_planned_course_id
													              AND pc2s.no_staff = "' . $selected_no_staff . '"
															 ORDER BY pc.tp_planned_course_date asc, pc.tp_courses_name asc');
					
					if ( $_GET['sort_field'] == 'tp_planned_course_complete_date' ) {
						$selected_sort_field = 'tp_planned_course_complete_date';	
					}
					
					if ( $detect_user_courses_query->numRows() == 0 ) {
						errorToPrint('No results!');
					}

					$_courses_array = array();
					while ( $detect_user_courses_item = $detect_user_courses_query->fetchRow() ) {
						$_courses_array[] = $detect_user_courses_item['tp_planned_course_id'];
						
						$reports_data[$detect_user_courses_item['tp_planned_course_id']] = array(
							'tp_planned_course_id' => $detect_user_courses_item['tp_planned_course_id'],
							'tp_planned_course_notes' => $detect_user_courses_item['tp_planned_course_notes'],
							'tp_courses_name' => $detect_user_courses_item['tp_courses_name']
						);
					}

					$reports_data[0] = array(
						'tp_planned_course_id' => implode(',', $_courses_array),
						'tp_courses_name' => 'All courses'
					);


					//while ( $detect_user_courses_item = $detect_user_courses_query->fetchRow() ) {
					foreach ( $reports_data as $key => $detect_user_courses_item ) {
						if ( $current_tp_planned_course_id == 0 && $detect_user_courses_query->numRows() == 1 ) {
							$current_tp_planned_course_id = 0;//$detect_user_courses_item['tp_planned_course_id'];
						}

						// 2. get info about each course
						$report_sql = 'SELECT pc2s.sabre_id as sql_sabre_id, 
											  pc2s.tp_planned_course_id as sql_tp_planned_course_id, 
											  pc2s.no_staff as sql_no_staff, 
						                      pc2s.sabre_passed_status, 
											  pc2s.sabre_none_passed_reason, 
											  CONCAT(u.firstname, " ", u.surname) as passed_by_trainer,
											  /*
											  pc2s.exp_date, 
											  */
										      pc2s.sabre_code, pc2s.sabre_subject,
										      pc2s.passed_date,
										      
										      pc.tp_planned_course_complete_date,

										  if( pc2s.exp_date >0, 
										      pc2s.exp_date, 
											  (
												SELECT pc2s2.exp_date
												  FROM tp_planned_courses_to_sabres pc2s2, tp_planned_courses pc2
												 WHERE ' . ( (int)$detect_user_courses_item['tp_planned_course_id'] > 0 ? ' pc2s.tp_planned_course_id < ' . (int)$detect_user_courses_item['tp_planned_course_id'] . ' AND ' : '' ) . '
												       pc2s2.tp_planned_course_id = pc2.tp_planned_course_id
												   AND pc2.tp_planned_course_completed =1
												   AND pc2s2.no_staff = sql_no_staff
												   AND pc2s2.sabre_id = sql_sabre_id
												   AND pc2s2.exp_date >0
											  ORDER BY pc2s2.exp_date DESC
												 LIMIT 1
											  ) 
										  ) AS exp_date

								     FROM tp_planned_courses pc, tp_planned_courses_to_sabres pc2s 
								LEFT JOIN users u on u.no_staff = pc2s.passed_by 
								    WHERE pc2s.no_staff = "' . $selected_no_staff . '"
								      AND pc2s.tp_planned_course_id = pc.tp_planned_course_id
									  ' . ( $key > 0 || empty($detect_user_courses_item['tp_planned_course_id']) ? ' AND pc2s.tp_planned_course_id = ' . (int)$detect_user_courses_item['tp_planned_course_id'] : ' AND pc2s.tp_planned_course_id IN (' . $detect_user_courses_item['tp_planned_course_id'] . ')' ) . '
								 ORDER BY ' . $sql_order_by;

						$tp_report_query = $db->query($report_sql);
						if ( $tp_report_query->numRows() > 0 ) {
						  while ( $tp_report_item = $tp_report_query->fetchRow() ) {
							$out_sabre_none_passed_reason = '';
						
							switch ( $tp_report_item['sabre_passed_status'] ) {
								case '1':
									$out_sabre_passed_status = 'Pass';
								break;
								case '2':
									$out_sabre_passed_status = 'Pass-Retake';
								break;
								case '3':
									$out_sabre_passed_status = 'Fail';
	
									if ( trim($tp_report_item['sabre_none_passed_reason'])=='' ) {
										$out_sabre_none_passed_reason = 'Reason is not entered!';
									} else {
										$out_sabre_none_passed_reason = trim($tp_report_item['sabre_none_passed_reason']);
									}
								break;
								case '4':
									$out_sabre_passed_status = 'Complete';
								break;
								case '5':
									$out_sabre_passed_status = 'Not-Complete';
								
									if ( trim($tp_report_item['sabre_none_passed_reason'])=='' ) {
										$out_sabre_none_passed_reason = 'Reason is not entered!';
									} else {
										$out_sabre_none_passed_reason = trim($tp_report_item['sabre_none_passed_reason']);
									}
								break;
								case '6':
									$out_sabre_passed_status = 'Not-Attended';
								break;
							}

							if ( $tp_report_item['exp_date'] == 0 ) {
								$out_exp_date = 'Training Incomplete';				
							} elseif ( $tp_report_item['exp_date'] == $tp_report_item['passed_date'] ) {
								$out_exp_date = 'No Expiry';
							} else {
								$out_exp_date = date('d F, Y', $tp_report_item['exp_date']);
							}				
						
							$reports_data[$key]['course_info'][] = array(
								'exp_date' => $out_exp_date,
								'sabre_code' => $tp_report_item['sabre_code'],
								'sabre_subject' => $tp_report_item['sabre_subject'],
								'sabre_none_passed_reason' => $out_sabre_none_passed_reason,
								'sabre_passed_status' => $out_sabre_passed_status,
								'sabre_passed_by_trainer' => ( $tp_report_item['passed_by_trainer']=='' ? 'Trainer Deleted' : $tp_report_item['passed_by_trainer'] ),
								//'date' => date('d F, Y', $tp_report_item['tp_planned_course_complete_date'])
								'date' => date('d F, Y', $tp_report_item['passed_date'])
							);

						  }
						}
					}
				} else {
					while ( $user_item = $how_many_users_query->fetchRow() ) {
						$tp_report_user_users[] = array(
							'no_staff' => $user_item['no_staff'],
							'firstname' => $user_item['firstname'],
							'surname' => $user_item['surname'] 
						);
					}
				}
			break;
			// *****************************************************************
			//                 EOF USER report
			// *****************************************************************

			// *****************************************************************
			//                 BOF FAIL report
			// *****************************************************************
			case 'report_fail':
				$date_from = mktime(0, 0, 0, $month_from, 1, $year_from);
				$date_to = mktime(23, 59, 59, $month_to, cal_days_in_month(CAL_GREGORIAN, $month_to, $year_to), $year_to);
			
				if ( $selected_sort_field != '' ) {
					switch ( $selected_sort_field ) {
						case 'sabre_code':
							$sql_order_by = 'pc2s.sabre_code ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'sabre_subject':
							$sql_order_by = 'pc2s.sabre_subject ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'user_name':
							$sql_order_by = 'user_name ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'passed_by_trainer':
							$sql_order_by = 'passed_by_trainer ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'exp_date':
							$sql_order_by = 'exp_date ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'sabre_passed_status':
							$sql_order_by = 'pc2s.sabre_passed_status ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						default: 
							$sql_order_by = 'exp_date asc';
						
							$selected_sort_field = 'exp_date';	
					}
				} else {
					$sql_order_by = 'exp_date asc';
					
					$selected_sort_field = 'exp_date';	
				}
				
				$report_sql = 'SELECT DISTINCT pc2s.sabre_id as sql_sabre_id, 
											  pc2s.tp_planned_course_id as sql_tp_planned_course_id, 
											  pc2s.no_staff as sql_no_staff, 

						                      pc2s.sabre_passed_status, 
											  pc2s.sabre_none_passed_reason, 
											  pc2s.manually_added_by_trainer, 

											  CONCAT(u.firstname, " ", u.surname) as passed_by_trainer,
											  CONCAT(pc2s.firstname, " ", pc2s.surname) as user_name,											  

											  /*pc2s.exp_date, */
										      pc2s.sabre_code, 
											  pc2s.sabre_subject,
											  
											  pc.tp_planned_course_complete_date,
											  pc.tp_planned_course_date,
											  pc.tp_courses_name,
											  pc.tp_planned_course_notes,
											  
										  if( pc2s.exp_date >0, 
										      pc2s.exp_date, 
											  (
												SELECT pc2s2.exp_date
												  FROM tp_planned_courses_to_sabres pc2s2, tp_planned_courses pc2
												 WHERE pc2s2.tp_planned_course_id <> sql_tp_planned_course_id
												   AND pc2s2.tp_planned_course_id = pc2.tp_planned_course_id
												   AND pc2.tp_planned_course_completed =1
												   AND pc2s2.no_staff = sql_no_staff
												   AND pc2s2.sabre_id = sql_sabre_id
												   AND pc2s2.exp_date >0
											  ORDER BY pc2s2.exp_date DESC
												 LIMIT 1
											  ) 
										  ) AS exp_date';
				
				if ( $_include_deleted_users == 1 ) {
					$report_sql .= '
					                 FROM tp_planned_courses pc, tp_planned_courses_to_sabres pc2s 
								LEFT JOIN users u on u.no_staff = pc2s.passed_by 
								    WHERE pc.tp_planned_course_completed = 1
								      AND pc2s.no_staff NOT IN ( SELECT no_staff FROM users)
									  AND pc.tp_planned_course_id = pc2s.tp_planned_course_id
									  AND pc2s.sabre_passed_status ' . ( (int)$_GET['id_fail']==0 ? 'in (0,2,3,5,6)' : ' = ' . (int)$_GET['id_fail'] ) . ' 
									  AND ( pc.tp_planned_course_complete_date >= ' . $date_from . ' and pc.tp_planned_course_complete_date <= ' . $date_to . ' )';
				} else {
					$report_sql .= '
								     FROM tp_planned_courses pc, ' . ( $id_base > 0 ? 'users_bases ub, bases b,' : '' ) . ' tp_planned_courses_to_sabres pc2s 
								LEFT JOIN users u on u.no_staff = pc2s.passed_by 
								    WHERE pc.tp_planned_course_completed = 1
								      AND pc.tp_planned_course_id = pc2s.tp_planned_course_id
									  AND pc2s.sabre_passed_status ' . ( (int)$_GET['id_fail']==0 ? 'in (0,2,3,5,6)' : ' = ' . (int)$_GET['id_fail'] ) . ' 
									  AND ( pc.tp_planned_course_complete_date >= ' . $date_from . ' and pc.tp_planned_course_complete_date <= ' . $date_to . ' )
									  ' . ( $id_base > 0 ? ' AND u.id_user = ub.id_user AND ub.id_base = b.id_base AND b.is_active > 0 AND b.id_base = ' . $id_base : '' ) . '';
				}
				
				$report_sql .= ' ORDER BY pc2s.tp_planned_course_id asc, ' . $sql_order_by;

					$tp_report_query = $db->query($report_sql);
					while ( $tp_report_item = $tp_report_query->fetchRow() ) {
							$out_sabre_none_passed_reason = '';
						
							if ( $tp_report_item['exp_date'] == 0 ) {
								$out_exp_date = 'Training Incomplete';				
							} else {
								$out_exp_date = date('d F, Y', $tp_report_item['exp_date']);
							}				

							switch ( $tp_report_item['sabre_passed_status'] ) {
								case '2':
									$out_sabre_passed_status = 'Pass-Retake';
								break;
								case '3':
									$out_sabre_passed_status = 'Fail';
	
									if ( trim($tp_report_item['sabre_none_passed_reason'])=='' ) {
										$out_sabre_none_passed_reason = 'Reason is not entered!';
									} else {
										$out_sabre_none_passed_reason = trim($tp_report_item['sabre_none_passed_reason']);
									}
								break;
								case '5':
									$out_sabre_passed_status = 'Not-Complete';
								
									if ( trim($tp_report_item['sabre_none_passed_reason'])=='' ) {
										$out_sabre_none_passed_reason = 'Reason is not entered!';
									} else {
										$out_sabre_none_passed_reason = trim($tp_report_item['sabre_none_passed_reason']);
									}
								break;
								case '6':
									$out_sabre_passed_status = 'Not-Attended';
								break;
							}
						
							$reports_data[$tp_report_item['sql_tp_planned_course_id']]['course_name'] = $tp_report_item['tp_courses_name'];
							$reports_data[$tp_report_item['sql_tp_planned_course_id']]['course_notes'] = $tp_report_item['tp_planned_course_notes'];
							$reports_data[$tp_report_item['sql_tp_planned_course_id']]['course_date'] = date('d F, Y', $tp_report_item['tp_planned_course_date']);
							$reports_data[$tp_report_item['sql_tp_planned_course_id']]['complete_date'] = date('d F, Y', $tp_report_item['tp_planned_course_complete_date']);
							$reports_data[$tp_report_item['sql_tp_planned_course_id']]['course_info'][] = array(
								'user_name' => $tp_report_item['user_name'],
								'manually_added_by_trainer' => $tp_report_item['manually_added_by_trainer'],
								'exp_date' => $out_exp_date,
								'sabre_code' => $tp_report_item['sabre_code'],
								'sabre_subject' => $tp_report_item['sabre_subject'],
								'sabre_none_passed_reason' => $out_sabre_none_passed_reason,
								'sabre_passed_status' => $out_sabre_passed_status,
								'sabre_passed_by_trainer' => ( $tp_report_item['passed_by_trainer']=='' ? 'Trainer Deleted' : $tp_report_item['passed_by_trainer'] )
							);
					}
			break;
			// *****************************************************************
			//                 EOF FAIL report
			// *****************************************************************



			// *****************************************************************
			//                 BOF COURSE report
			// *****************************************************************
		  	case 'report_course':
				$date_from = mktime(0, 0, 0, $month_from, 1, $year_from);
				$date_to = mktime(23, 59, 59, $month_to, cal_days_in_month(CAL_GREGORIAN, $month_to, $year_to), $year_to);
		  	
				if ( $selected_sort_field != '' ) {
					switch ( $selected_sort_field ) {
						case 'tp_planned_course_date':
							$sql_order_by = 'pc.tp_planned_course_date ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'tp_planned_course_notes':
							$sql_order_by = 'pc.tp_planned_course_notes ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'tp_planned_course_trainer':
							//$sql_order_by = 'pc.tp_planned_course_trainer ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
							$sql_order_by = 'course_trainer ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'total_count_modules':
							$sql_order_by = 'total_count_modules ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'total_count_passed':
							$sql_order_by = 'total_count_passed ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						case 'percentage':
							$sql_order_by = 'percentage ' . ( $_GET['order_type']=='a' ? 'asc' : 'desc' );
						break;

						default: 
							$sql_order_by = 'pc.tp_planned_course_date desc';
						
							$selected_sort_field = 'tp_planned_course_date';	
					}
				} else {
					$sql_order_by = 'pc.tp_planned_course_date desc';
					
					$selected_sort_field = 'tp_planned_course_date';	
				}

				if ( isset($_GET['tp_courses_id']) && (int)$_GET['tp_courses_id'] > 0 ) {
					$current_tp_courses_id = (int)$_GET['tp_courses_id'];
				} else {
					$current_tp_courses_id = 0;
				}

				$report_sql = 'select distinct pc.tp_planned_course_id, pc.tp_planned_course_date, pc.tp_planned_course_notes, pc.tp_planned_course_trainer,
											   CONCAT(u.firstname, " ", u.surname) as course_trainer,
				                      sum( if( pc2s.sabre_passed_status IN ( 1, 2, 4 ) , 1, 0 ) ) AS total_count_passed, 
									  sum( if( pc2s.sabre_passed_status IN ( 1, 2, 3, 4, 5 ) , 1, 0 ) ) AS total_count_modules,
									  round( (sum( if( pc2s.sabre_passed_status IN ( 1, 2, 4 ) , 1, 0 ) ) / sum( if( pc2s.sabre_passed_status IN ( 1, 2, 3, 4, 5 ) , 1, 0 ) ) ) *100, 2 ) AS percentage
							 from tp_planned_courses_to_sabres pc2s, tp_planned_courses pc
					    LEFT JOIN users u on u.no_staff = pc.tp_planned_course_completed_by
							where pc.tp_planned_course_completed = 1';

				if ( $_include_deleted_users == 1 ) {
					$report_sql .= '
					          AND pc2s.no_staff NOT IN ( SELECT no_staff FROM users)';
				} 
				
				$report_sql .= '
							  and pc.tp_planned_course_id = pc2s.tp_planned_course_id
							  and pc.tp_courses_id = "' . $current_tp_courses_id . '"
							  and pc.tp_planned_course_date >= ' . $date_from . ' and pc.tp_planned_course_date <= ' . $date_to . '
							  GROUP BY pc.tp_planned_course_id
						 order by ' . $sql_order_by;

				$tp_report_query = $db->query($report_sql);


				$C['pdf_status'] = $tp_report_query->numRows() + 5;
				
				$pdf_passed_data_array = array();
				$pdf_none_passed_data_array = array();
				while ( $tp_report_item = $tp_report_query->fetchRow() ) {

					// BOF get passed modules
					if ( isset($_GET['save_to_pdf']) ) {
						$available_statuses = array(1,2,4,3,5,6);
						foreach ( $available_statuses as $value ) {
							$tmp_course_modules_query = $db->query('select distinct pc2s.sabre_id,
																   pc2s.sabre_code,
																   pc2s.sabre_subject
							                                  from tp_planned_courses_to_sabres pc2s
							                                 where pc2s.sabre_passed_status = ' . $value . '
															   and pc2s.tp_planned_course_id = ' . $tp_report_item['tp_planned_course_id'] . 
														' order by pc2s.sabre_code, pc2s.sabre_subject asc');
							while ( $tmp_course_modules_item = $tmp_course_modules_query->fetchRow() ) {
								$pdf_passed_data_array[$tp_report_item['tp_planned_course_id']][$value]['modules'][$tmp_course_modules_item['sabre_id']]['module_info'] = $tmp_course_modules_item;

								$tmp_course_users_query = $db->query('select distinct pc2s.no_staff,
																   CONCAT(pc2s.firstname, " ", pc2s.surname) as username
							                                  from tp_planned_courses_to_sabres pc2s
							                                 where pc2s.sabre_passed_status  = ' . $value . '
							                                   and pc2s.sabre_id = ' . (int)$tmp_course_modules_item['sabre_id'] . '
															   and pc2s.tp_planned_course_id = ' . $tp_report_item['tp_planned_course_id'] . ' 
														  order by username asc');
								while ( $tmp_course_users_item = $tmp_course_users_query->fetchRow() ) {
									$pdf_passed_data_array[$tp_report_item['tp_planned_course_id']][$value]['modules'][$tmp_course_modules_item['sabre_id']]['users'][] = $tmp_course_users_item;
								}
							}				
						}
					}
					// EOF get passed modules

					$course_users_query = $db->query('select distinct pc2s.firstname, pc2s.surname, pc2s.manually_added_by_trainer  
					                                   from tp_planned_courses_to_sabres pc2s
													  where pc2s.tp_planned_course_id = ' . $tp_report_item['tp_planned_course_id'] . ' 
												   order by pc2s.surname asc, pc2s.firstname asc');
					$out_users = array();
					while ( $course_users_item = $course_users_query->fetchRow() ) {
						$out_users[] = $course_users_item['surname'] . ' ' . $course_users_item['firstname'] . ( $course_users_item['manually_added_by_trainer']==1 ? ' (M)' : '' );
					}
					
					$course_modules_query = $db->query('select distinct pc2s.sabre_code, pc2s.sabre_subject
					                                   from tp_planned_courses_to_sabres pc2s
													  where pc2s.tp_planned_course_id = ' . $tp_report_item['tp_planned_course_id'] . ' 
												   order by pc2s.sabre_code asc');
					$out_modules = array();
					while ( $course_modules_item = $course_modules_query->fetchRow() ) {
						$out_modules[] = $course_modules_item['sabre_code'] . ': ' . $course_modules_item['sabre_subject'];
					}
					
					
					$reports_data[] = array(
								'tp_planned_course_id' => $tp_report_item['tp_planned_course_id'],
								'tp_planned_course_date' => date('d F, Y', $tp_report_item['tp_planned_course_date']),
								'tp_planned_course_notes' => $tp_report_item['tp_planned_course_notes'],
								//'tp_planned_course_trainer' => $tp_report_item['tp_planned_course_trainer'],
								'tp_planned_course_trainer' => ( $tp_report_item['course_trainer']=='' ? 'Trainer Deleted' : $tp_report_item['course_trainer'] ),
								'total_count_passed' => $tp_report_item['total_count_passed'],
								'total_count_modules' => $tp_report_item['total_count_modules'],
								'attendees' => $out_users,
								'modules' => $out_modules,
								'percentage' => $tp_report_item['percentage'] . '%'
							);
				}
			break;
			// *****************************************************************
			//                 EOF COURSE report
			// *****************************************************************
		  }
		}
	break;
}

includeJS();

if ( in_array($selected_report_type, array('report_exp', 'report_mod', 'report_fail')) ) {
	$months_array = get_report_ddm_months();
	
	if ( $selected_report_type=='report_exp' ) {
		$min_date = get_report_min_date();
		$max_date = get_report_max_date();
	} else {
		$min_date = get_course_min_date();
		$max_date = get_course_max_date();
	}
	
	$min_year = date('Y', $min_date);
	$max_year = date('Y', $max_date);

	$years_array = array();
	$years_array[] = array( 'id' => 0, 'text' => 'Select Year...' );

	for ($i = 0, $N = ($max_year - $min_year); $i <= $N; $i++ ) {
		$tmp_year = $min_year + $i;
		$years_array[] = array( 'id' => $tmp_year, 'text' => $tmp_year );
	}

	$bases_array = get_report_ddm_bases();

	if ( $selected_report_type=='report_fail' ) {
		$fail_data_array = array();
		$fail_data_array[] = array('id' => 0, 'text' => 'All');
		$fail_data_array[] = array('id' => 3, 'text' => 'Fail');
		$fail_data_array[] = array('id' => 5, 'text' => 'Not-Complete');
		$fail_data_array[] = array('id' => 6, 'text' => 'Not-Attended');
		$fail_data_array[] = array('id' => 2, 'text' => 'Pass on Retake');			
	}

	pageAutoAssignVars(
		'selected_report_type',
		'selected_sort_field', 
		'months_array',
		'years_array',
		'bases_array',
		'month_to',
		'month_from',
		'year_to',
		'year_from',
		'id_base',
		
 		( $selected_report_type=='report_fail' ? 'fail_data_array' : '' ),
 	
		'reports_data',
		'flag_error'
	);
}

if ( $selected_report_type == 'report_user' ) {
	pageAutoAssignVars(
		'selected_report_type',
		'selected_sort_field',
		'selected_no_staff',
		'current_tp_planned_course_id',
	
		'reports_data',
		'tp_report_user_users',
		'flag_error'
	);
}

if ( $selected_report_type == 'report_course' ) {
	$months_array = get_report_ddm_months();
	$min_date = get_course_min_date();
	$max_date = get_course_max_date();
	
	$min_year = date('Y', $min_date);
	$max_year = date('Y', $max_date);

	$years_array = array();
	$years_array[] = array( 'id' => 0, 'text' => 'Select Year...' );

	for ($i = 0, $N = ($max_year - $min_year); $i <= $N; $i++ ) {
		$tmp_year = $min_year + $i;
		$years_array[] = array( 'id' => $tmp_year, 'text' => $tmp_year );
	}

	$completed_courses_query = $db->query('SELECT DISTINCT pc.tp_courses_id, pc.tp_courses_name 
						 FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' pc
                        WHERE pc.tp_planned_course_completed = 1
                     ORDER BY pc.tp_courses_name asc');
				
	$completed_courses_array = array();
	$completed_courses_array[] = array( 'id' => 0, 'text' => 'Select Course...');
	while ( $completed_courses_item = $completed_courses_query->fetchRow() ) {
		$completed_courses_array[] = array( 'id' => $completed_courses_item['tp_courses_id'], 'text' => $completed_courses_item['tp_courses_name'] );
	}

	pageAutoAssignVars(
		'selected_report_type',
		'selected_sort_field',
		'current_tp_courses_id',
		'month_to',
		'month_from',
		'year_to',
		'year_from',
		'pdf_passed_data_array',

		'reports_data',
		'months_array',
		'years_array',
		'completed_courses_array',
		'flag_error'
	);
}

if ( isset($_POST['pdf']) && !empty($selected_report_type) ) {
	pageDisplay( 'admin/reports/' . $selected_report_type );
} else {
	pageDisplay();
}
?>
