<?php

require_once '../inc/global.php';

adminAccessCheck();

pageTitle('Crew Qualification');

$aCQ = crewQualifGet();

$iEdit = intval(@ $_GET['edit']);

if ( ! isset($aCQ[$iEdit]) ) $iEdit = 0;

if ( isset($_POST['delete']) ) {
	// BOF check for assigned SABRES
	$tmp_assinged_sabres = '';
	$flag_assinged_sabres_exists = false;
	$total_sabres = 0;
	foreach ( $_POST['delete'] as $_cq_id ) {
		$count_sabres = crewAssignedToSabres($_cq_id);
		$total_sabres += $count_sabres;
		
		$tmp_assinged_sabres .= '<small>* Crew <font color=red>' . $aCQ[$_cq_id]['cq_code'] . '</font> assigned to <font color=red>' . $count_sabres . ' ' . ( $count_sabres > 1 ? 'Sabres codes' : 'Sabre code' ) . '</font>.</small>' . '<br>';
		
		if ( $count_sabres > 0 ) {
			$flag_assinged_sabres_exists = true;	
		}
		
	}
	// EOF check for assigned SABRES
	
	if ( $flag_assinged_sabres_exists ) {
		errorToPrint( ( $total_sabres > 1 ? 'There are some Sabres codes' : 'There is a Sabre code' ) . ' associated with ' . ( count($_POST['delete']) > 1 ? 'these' : 'this' ) . '.<br>Please reassign ' . ( $total_sabres > 1 ? 'them' : 'it' ) . ' before deleting: <br>' . $tmp_assinged_sabres);
	} else {
		foreach ( $_POST['delete'] as $_cq_id ) {
			$db->query('DELETE FROM ' . $CONFP['TABLE_CREW_QUALIF'] . ' WHERE cq_id = ' . $_cq_id);
		}
	
		header('Location: ' . $_SERVER['PHP_SELF'] . '?success=deleted'); exit;
	}
}

$aData = array();
if ( $iEdit ) $aData = $aCQ[$iEdit];

if ( ! is_array($aData) ) $aData = array();

if ( ! empty($_POST['submitted']) )
{
	$aData = typedArray($_POST, array
	(
		'cq_code' => '',
		'cq_desc' => '',
	),
	'trim');

	$aData['cq_code'] = mb_strtoupper($aData['cq_code']);

	# Errors		

	if ( ! $aData['cq_code'] || ! $aData['cq_desc'] )
	{
		errorToPrint('Please complete all fields');
	}
	elseif ( ($tmp = dicListItem('CREW_QUALIF_CODE_2_ID', $aData['cq_code'])) && $iEdit != $tmp )
	{
		errorToPrint('This code already used');
	}

	if ( ! $aErrors )
	{
		$aDb = typedArray($aData, array
		(
			'cq_code' => '',
			'cq_desc' => '',
		), '');

		if ( ! $iEdit ) $aDb['cq_tsp_added'] = TIME;
		$aDb['cq_tsp_modified'] = TIME;

		if ( $iEdit ) {
			$sql = 'update ' . CONFP('TABLE_CREW_QUALIF') . ' set cq_code="' . addslashes($aDb['cq_code']) . '", cq_desc="' . addslashes($aDb['cq_desc']) . '", cq_tsp_modified = ' . $aDb['cq_tsp_modified'] . ' where cq_id=' . $iEdit;
		} else {
			$sql = 'insert into ' . CONFP('TABLE_CREW_QUALIF') . ' (cq_code, cq_desc, cq_tsp_added) values ("' . addslashes($aDb['cq_code']) . '", "' . addslashes($aDb['cq_desc']) . '", ' . $aDb['cq_tsp_added'] . ')';
		}

		if ( PEAR::isError($db->query($sql)) )
		{
			errorToPrint('Database error on saving info');
		}
		else 
		{
			header('Location: ' . $_SERVER['PHP_SELF'] . '?success=saved'); exit;
		}
	}
}

switch ( @$_GET['success'] )
{
	case 'saved': successToPrint('Successfully saved'); break;
	case 'deleted': successToPrint('Successfully deleted'); break;
}

includeJS();

pageAutoAssignVars('iEdit', 'aCQ', 'aData');

pageDisplay();

?>
