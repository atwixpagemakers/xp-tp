<?php

require_once '../inc/global.php';

adminAccessCheck();

pageTitle('Course Builder');

$coursesData = coursesGet();

$iEdit = intval(@ $_GET['edit']);

if ( ! isset($coursesData[$iEdit]) ) $iEdit = 0;

if ( isset($_POST['delete']) ) {
//	errorToPrint('Delete feature is not available for now.');

	$flag_assinged_courses_exists = false;
	$tmp_error_string = '';
	// 1. check if COURSE is planned?
	foreach ( $_POST['delete'] as $_tp_courses_id ) {
		$res = $db->query('SELECT tp_pc.tp_planned_course_id, tp_c.tp_courses_name 
						   FROM tp_planned_courses tp_pc
						   LEFT JOIN tp_courses tp_c on tp_pc.tp_courses_id = tp_c.tp_courses_id
                           WHERE tp_c.tp_courses_id = "' . $_tp_courses_id . '"');
        $tmp_count = $res->numRows();
		if ( $tmp_count > 0 ) {
			$row = $res->fetchRow();
			
			$tmp_error_string .= '<small>* Course <font color=red>' . $row['tp_courses_name'] . '</font> assigned with <font color=red>' . $tmp_count . ' planned course' . ( $tmp_count > 1 ? 's' : '' ) . '</font>.</small>' . '<br>';

			$flag_assinged_courses_exists = true;	
		}
	}

	if ( $flag_assinged_courses_exists ) {
		errorToPrint( $tmp_error_string );
	} else {
		foreach ( $_POST['delete'] as $_tp_courses_id ) {
			$db->query('DELETE FROM ' . $CONFP['TABLE_TP_COURSES'] . ' WHERE tp_courses_id = ' . $_tp_courses_id);
			$db->query('DELETE FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' WHERE tp_planned_course_id IN (SELECT tp_planned_course_id FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' WHERE tp_courses_id = ' . $_tp_courses_id . ')');

			$db->query('DELETE FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' WHERE tp_courses_id = ' . $_tp_courses_id);			
		}
	
		header('Location: ' . $_SERVER['PHP_SELF'] . '?success=deleted'); exit;
	}
}


$curr_course_data = array();
if ( $iEdit ) $curr_course_data = $coursesData[$iEdit];

if ( ! is_array($curr_course_data) ) $curr_course_data = array();

if ( ! empty($_POST['submitted']) ) {
	$curr_course_data = typedArray($_POST, array
	(
		'tp_courses_name' => ''
	),
	'trim');

	# Errors		
	$tmp_tp_courses_id = getCourseIDByName($curr_course_data['tp_courses_name']);
	
	if ( ! $curr_course_data['tp_courses_name'] ) {
		errorToPrint('Please enter a course name.');
	} elseif ( ( $iEdit != $tmp_tp_courses_id ) && ( $tmp_tp_courses_id > 0 ) ) {
		errorToPrint('This Course Name already exists.');
	}

	if ( ! $aErrors ) {
		if ( ! $iEdit ) { // INSERT
			$sql = 'insert into ' . CONFP('TABLE_TP_COURSES') . ' (
						tp_courses_name,
						date_added) values (
						"' . addslashes($curr_course_data['tp_courses_name']) . '", 
						' . TIME . ')';
			
			if ( PEAR::isError($db->query($sql)) ) {
				errorToPrint('Database error on saving info');
			} else {
				$_tp_courses_id = $db->lastInsertID();
				if ( isset($_POST['sabre_id']) ) {
					foreach ( $_POST['sabre_id'] as $_sabre_id => $value ) {
						$db->query('insert into ' . CONFP('TABLE_TP_SABRES_TO_COURSES') . '(sabre_id, tp_courses_id) values (' . $_sabre_id . ', ' . $_tp_courses_id . ')');
					}
				}
	
				header('Location: ' . $_SERVER['PHP_SELF'] . '?success=saved'); exit;				
			}
		} else { // UPDATE
			$sql = 'update ' . CONFP('TABLE_TP_COURSES') . ' 
					set	tp_courses_name = "' . addslashes($curr_course_data['tp_courses_name']) . '",
						date_modified = ' . TIME . '
					where tp_courses_id = ' . $iEdit;
			
			if ( PEAR::isError($db->query($sql)) ) {
				errorToPrint('Database error on saving info');
			} else {
				$db->query('delete from ' . CONFP('TABLE_TP_SABRES_TO_COURSES') . ' where tp_courses_id = ' . $iEdit);
				
				$_tp_courses_id = $iEdit;
				if ( isset($_POST['sabre_id']) ) {
					foreach ( $_POST['sabre_id'] as $sabre_id => $value ) {
						$db->query('insert into ' . CONFP('TABLE_TP_SABRES_TO_COURSES') . '( sabre_id, tp_courses_id ) values (' . $sabre_id . ', ' . $_tp_courses_id . ')');
					}
				}
	
				header('Location: ' . $_SERVER['PHP_SELF'] . '?success=saved'); exit;				
			}
		}
	}
}

switch ( @$_GET['success'] )
{
	case 'saved': successToPrint('Successfully saved'); break;
	case 'deleted': successToPrint('Successfully deleted'); break;
}

includeJS();

$courseData = $curr_course_data;

pageAutoAssignVars('iEdit', 'coursesData', 'courseData');

pageDisplay();

?>
