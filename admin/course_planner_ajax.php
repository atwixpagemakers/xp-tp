<?php
require_once '../inc/global.php';

adminAccessCheck();

/*
echo '<pre>';
print_r($_SESSION['UPLOADED_USERS']);
echo '</pre>';
*/

if ( $_POST['action']=='remove_user_from_session' ) {
	if ( count($_SESSION['UPLOADED_USERS']) > 0 ) {
		foreach ( $_SESSION['UPLOADED_USERS'] as $key => $user_item ) {
			if ( $key == $_POST['staff_no'] ) {
				unset($_SESSION['UPLOADED_USERS'][$key]);
				 
				$return_json_array['user_staff_no'] = $key;
				break;
			}
		}		
	}
}

if ( $_POST['action']=='remove_user_from_corse' ) {
	if ( !in_array($_POST['staff_no'], $_SESSION['DELETED_EDIT_USERS']) ) {
		$_SESSION['DELETED_EDIT_USERS'][] = $_POST['staff_no'];
	} 
		
	/*
	$db->query('DELETE FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' 
	            WHERE tp_planned_course_id = "' . $_POST['tp_planned_course_id'] . '"
				  AND no_staff = "' . $_POST['staff_no'] . '"');
	*/
	$return_json_array['user_staff_no'] = $_POST['staff_no'];
}

if ( $_POST['action']=='add_user_to_corse' ) {
	if ( in_array($_POST['staff_no'], $_SESSION['DELETED_EDIT_USERS']) ) {
		for ($i = 0, $N = count($_SESSION['DELETED_EDIT_USERS']); $i < $N; $i++) {
			if ( $_POST['staff_no']==$_SESSION['DELETED_EDIT_USERS'][$i] ) {
				unset($_SESSION['DELETED_EDIT_USERS'][$i]);
				
				break;
			}
		}
		
	} 
		
	$return_json_array['user_staff_no'] = $_POST['staff_no'];
}


echo json_encode($return_json_array);
?>
