<?php

require_once '../inc/global.php';

adminAccessCheck();

pageTitle('Deleted Users Data');

$duData = getDeletedUsersList();

if ( isset($_POST['delete']) ) {
	foreach ( $_POST['delete'] as $_no_staff ) {
		// proverka na pystoty kursa!!!!
		$courses_query = $db->query('select distinct tp_planned_course_id 
		                               FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' 
									  WHERE no_staff = "' . $_no_staff . '"');
		while ( $courses_item = $courses_query->fetchRow() ) {
			$check_other_users_query = $db->query('select distinct no_staff
		                                             FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' 
									                WHERE no_staff <> "' . $_no_staff . '"
													  and tp_planned_course_id = ' . (int)$courses_item['tp_planned_course_id']);
			if ( $check_other_users_query->numRows() > 0 ) {
				$db->query('DELETE FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' 
				                  WHERE no_staff = "' . $_no_staff . '" 
								    and tp_planned_course_id = ' . (int)$courses_item['tp_planned_course_id']);
			} else {
				$db->query('DELETE FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES'] . ' WHERE tp_planned_course_id = ' . (int)$courses_item['tp_planned_course_id']);				
				$db->query('DELETE FROM ' . $CONFP['TABLE_TP_PLANNED_COURSES_TO_SABRES'] . ' WHERE tp_planned_course_id = ' . (int)$courses_item['tp_planned_course_id']);
			}
		}
	}
	
	header('Location: ' . $_SERVER['PHP_SELF'] . '?success=deleted'); exit;
}

switch ( @$_GET['success'] )
{
	case 'deleted': successToPrint('Successfully deleted'); break;
}

includeJS();

pageAutoAssignVars('duData');

pageDisplay();
?>
